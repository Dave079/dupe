﻿
CREATE TABLE "users" (
  "id" INT NOT NULL,
  "name" VARCHAR(30) NOT NULL,
  "surname" VARCHAR(30) NOT NULL,
  "lastlogin" timestamp  NULL,
  "password" VARCHAR(20) NOT NULL,
  "type" VARCHAR(10) NOT NULL,
  PRIMARY KEY ("id"));

CREATE TABLE "customer" (
  "id" INT NOT NULL,
  "name" VARCHAR(100) NOT NULL,
  "address" VARCHAR(200) NOT NULL,
  "phonenumber" VARCHAR(20) NULL,
  "email" VARCHAR(254) NULL,
  "ico" VARCHAR(20) NULL,
  "note" VARCHAR(200) NULL,
  PRIMARY KEY ("id"));

CREATE TABLE "product" (
  "id" INT NOT NULL,
  "ean" INT NULL,
  "name" VARCHAR(45) NOT NULL,
  "stock" INT NOT NULL DEFAULT 0,
  "price" INT NOT NULL,
  "note" VARCHAR(200) NULL,
  PRIMARY KEY ("id"));

CREATE TABLE "payment" (
  "id" INT NOT NULL,
  "accountnumber" VARCHAR(45) NOT NULL,
  "variablesymbol" INT NULL,
  "amount" INT NOT NULL,
  PRIMARY KEY ("id"));

CREATE TABLE "purchaseorder" (
  "id" INT NOT NULL,
  "datetime" timestamp  NOT NULL,
  "state" VARCHAR(15) NOT NULL,
  "price" INT NOT NULL,
  "notefromcustomer" VARCHAR(200) NULL,
  "notefromuser" VARCHAR(200) NULL,
  "payment_id" INT NULL,
  "user_id" INT NOT NULL,
  "customer_id" INT NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_invoice_user1"
    FOREIGN KEY ("user_id")
    REFERENCES "users" ("id"),
  CONSTRAINT "fk_invoice_customer1"
    FOREIGN KEY ("customer_id")
    REFERENCES "customer" ("id"),
  CONSTRAINT "fk_invoice_payment1"
    FOREIGN KEY ("payment_id")
    REFERENCES "payment" ("id"));    

CREATE TABLE "productsonpurchaseorder" (
  "id" INT NOT NULL,
  "quantity" INT NOT NULL,
  "discount" INT NOT NULL DEFAULT 0,
  "price" INT NOT NULL,
  "note" VARCHAR(200) NULL,
  "purchaseorder_id" INT NOT NULL,
  "product_id" INT NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_product_has_invoice_invoice1"
    FOREIGN KEY ("purchaseorder_id")
    REFERENCES "purchaseorder" ("id"),
  CONSTRAINT "fk_product_has_invoice_product1"
    FOREIGN KEY ("product_id")
    REFERENCES "product" ("id"));
 
 delete from customer where id>2000000
