﻿using DuPE.Domain.Databases;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;

namespace DuPE.API.Databases
{
    public class GeneralRepositoryGetter
    {
        private Dictionary<string, IDatabaseRepositoryCreater> databases;

        public GeneralRepositoryGetter(Dictionary<string, IDatabaseRepositoryCreater> databases)
        {
            this.databases = databases;
        }

        public IGeneralRepository Get(string name, Dictionary<string, object> arguments)
        {
            if (databases != null && databases.ContainsKey(name))
                return databases[name].GetGeneralRepository(arguments);
            else
                throw new Exception("Unsupported database");
        }

        public IGeneralRepository Get(string name, string connectionString, string schema)
        {
            if (databases != null && databases.ContainsKey(name))
                return databases[name].GetGeneralRepository(connectionString, schema);
            else
                throw new Exception("Unsupported database");
        }
    }
}
