﻿using DuPE.API.Databases;
using DuPE.Domain.Collections;
using DuPE.Domain.Masking;
using DuPE.Domain.Projects;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Reports;
using DuPE.Domain.Tables;
using System.Collections.Generic;

namespace DuPE.API
{
    public class DuPEApi
    {
        public ICollectionRepository CollectionRepository { get; private set; }
        public Project ActualProject { get { return actualProject; } private set { actualProject = value; SetGeneralRepository(); } }

        private Project actualProject;

        private IProjectSaverAndLoader projectSaverAndLoader;
        private IGeneralRepository sourceGeneralRepository;
        private IGeneralRepository targetGeneralRepository;
        private IProjectCheckerForMasking projectCheckerWithReportForUser;
        private IProjectCheckerForUpdate projectCheckerForDifferences;
        private IProjectUpdater projectUpdater;
        private GeneralRepositoryGetter generalRepositoryGetter;

        public DuPEApi(
            IProjectSaverAndLoader projectSaverAndLoader,
            ICollectionRepository collectionRepository,
            IProjectCheckerForMasking projectCheckerWithReportForUser,
            IProjectCheckerForUpdate projectCheckerForDifferences,
            IProjectUpdater projectUpdater,
            GeneralRepositoryGetter generalRepositoryGetter
            )
        {
            CollectionRepository = collectionRepository;

            this.generalRepositoryGetter = generalRepositoryGetter;
            this.projectSaverAndLoader = projectSaverAndLoader;

            this.projectCheckerWithReportForUser = projectCheckerWithReportForUser;
            this.projectCheckerForDifferences = projectCheckerForDifferences;
            this.projectUpdater = projectUpdater;
        }

        public void LoadProject(string path)
        {
            ActualProject = projectSaverAndLoader.Load(path);
        }

        public void SaveProject()
        {
            projectSaverAndLoader.Save(ActualProject);
        }

        public void CreateProject(string databaseName, string projectName, Dictionary<string, object> sourceConnectionParameters, Dictionary<string, object> targetConnectionParameters)
        {
            sourceGeneralRepository = generalRepositoryGetter.Get(databaseName, sourceConnectionParameters);

            if (targetConnectionParameters == null)
            {
                targetGeneralRepository = sourceGeneralRepository;
                targetConnectionParameters = sourceConnectionParameters;
            }
            else
                targetGeneralRepository = generalRepositoryGetter.Get(databaseName, targetConnectionParameters);

            actualProject = new Project()
            {
                SourceConnetionString = sourceGeneralRepository.GetConnectionString(),
                SourceSchema = sourceGeneralRepository.GetSchema(),
                TargetConnetionString = targetGeneralRepository.GetConnectionString(),
                TargetSchema = targetGeneralRepository.GetSchema(),
                Database = databaseName,
                Name = projectName
            };
            actualProject.Tables = sourceGeneralRepository.GetAll();
        }

        public bool IsProjectReadyForMasking(out Report report, TypeOfControl checkType)
        {
            return projectCheckerWithReportForUser.IsProjectReadyForMasking(ActualProject, out report, TypeOfControl.CompleteCheck, targetGeneralRepository);
        }

        public bool IsProjectSameAsTargetDatabase(out IList<ProjectDifference> differences)
        {
            return projectCheckerForDifferences.IsProjectSameAsTargetDatabase(ActualProject, out differences, targetGeneralRepository);
        }

        //public bool IsProjectSameAsTargetDatabase(out IList<ProjectDifference> differences)
        //{
        //    return projectCheckerForDifferences.IsProjectSameAsTargetDatabase(ActualProject, out differences, sourceGeneralRepository);
        //}

        public int StartMasking(MaskingType type)
        {
            var masker = new MaskingProcesor(targetGeneralRepository, CollectionRepository);
            return masker.StartMasking(ActualProject, type);
        }

        private void SetGeneralRepository()
        {
            sourceGeneralRepository = generalRepositoryGetter.Get(ActualProject.Database, ActualProject.SourceConnetionString, ActualProject.SourceSchema);
            targetGeneralRepository = generalRepositoryGetter.Get(ActualProject.Database, ActualProject.TargetConnetionString, ActualProject.TargetSchema);
        }

        public void Update(IEnumerable<ProjectDifference> differences)
        {
            projectUpdater.Update(differences, actualProject);
        }
    }
}
