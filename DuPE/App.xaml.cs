﻿using DuPE.Tools;
using log4net;
using System.Windows;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace DuPE
{

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string MutexName = "#?#||DuPE||#?#";
        private ILog log = LogManager.GetLogger("App");

        protected override void OnStartup(StartupEventArgs e)
        {
            AppInfo.Instance.Initialize(System.Reflection.Assembly.GetExecutingAssembly(), "");
            log.Info(string.Format("Starting App {0} v{1} from location {2} on {3} {4} bit {5}",
               AppInfo.Instance.NameWithCustomer,
               AppInfo.Instance.Version,
               AppInfo.Instance.Location,
               AppInfo.Instance.OperationSystem,
               AppInfo.Instance.OS64x32bit,
               AppInfo.Instance.ServicePack
               ));


            if (CheckThisAppAlreadyRunning())
                App.Current.Shutdown();

            SettingsPreparer.LoadAndPrepare();

            base.OnStartup(e);
        }


        private static bool CheckThisAppAlreadyRunning()
        {
            if (RunningProcessChecker.isThisAppAlreadyRunning(MutexName))
            {
                var process = RunningProcessChecker.FindRunningProcessWithSameName();
                if (process != null)
                    ControlWindows.SetForegroundWindow(process.MainWindowHandle);
                else
                    System.Windows.MessageBox.Show("Application is already running.", "", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK, MessageBoxOptions.ServiceNotification);

                return true;
            }
            else
                return false;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            SettingsPreparer.Save();
            log.Info(string.Format("App closed"));
            base.OnExit(e);
        }

    }
}
