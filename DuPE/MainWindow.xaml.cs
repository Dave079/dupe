﻿using DuPE.Actions;
using DuPE.API;
using DuPE.DependencyInjection;
using DuPE.Domain.AutomaticRulesSetterLogic;
using DuPE.Domain.Columns;
using DuPE.Domain.Logging;
using DuPE.Domain.Masking;
using DuPE.Domain.Projects;
using DuPE.GUI;
using DuPE.GUI.Collections;
using DuPE.GUI.NewProject;
using DuPE.GUI.Tables;
using DuPE.GUI.Tools;
using DuPE.Logging;
using DuPE.Properties;
using DuPE.Resources.Strings;
using FirstFloor.ModernUI.Windows.Controls;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DuPE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public static readonly RoutedCommand StartMaskingAllRule = new RoutedCommand();
        public static readonly RoutedCommand StartMaskingUsedRule = new RoutedCommand();
        public static readonly RoutedCommand StartMaskingUnusedRule = new RoutedCommand();

        public static readonly RoutedCommand AddRuleToSelectItemsCommand = new RoutedCommand();
        public static readonly RoutedCommand AutomaticAddRuleToAllCommand = new RoutedCommand();
        public static readonly RoutedCommand DeleteRuleFromSelectedItemsCommand = new RoutedCommand();
        public static readonly RoutedCommand DeleteAllRuleCommand = new RoutedCommand();

        public static readonly RoutedCommand CheckProject = new RoutedCommand();
        public static readonly RoutedCommand UpdateByTarget = new RoutedCommand();
        public static readonly RoutedCommand UpdateBySource = new RoutedCommand();

        private readonly string APP_TITTLE;
        private ILogger logger = new Log4NetLogger(LogManager.GetLogger("App"));
        private Project project;
        private DuPEApi API;

        public MainWindow()
        {
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            InitializeComponent();

            API = NinjectService.Instance.Get<DuPEApi>();
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var versionShort = string.Format("{0}.{1}", assembly.GetName().Version.Major.ToString(), assembly.GetName().Version.Minor.ToString());
            APP_TITTLE = string.Format("DuPE (Data ProtEctor) {0}", versionShort);
            this.Title = APP_TITTLE;

            MainStartUserControl.SetMethodOpenProject(OpenProject);
            MainStartUserControl.SetMethodCreateProject(CreateNewProject);
        }

        private void SetTitle()
        {
            this.Title = string.Format(" {0} ({2})- {1}", string.IsNullOrWhiteSpace(project.Name) ? "NA" : project.Name, APP_TITTLE, project.LastSave == null ? MainResource.NoSave : string.Format(MainResource.LastSave, project.LastSave));
        }

        private void LoadProject()
        {
            SetTitle();
            TreeViewForProject.Items.Clear();

            var tablesUserCOntrol = new TablesUserControl(project.Tables);
            tablesUserCOntrol.OnUserAction += TablesUserCOntrol_OnUserAction;
            MainView.Content = tablesUserCOntrol;

            var schema = new TreeViewItem();
            schema.Header = MainResource.Schema;

            schema.Selected += Schema_Selected;

            TreeViewForProject.Items.Add(schema);

            foreach (var table in project.Tables)
            {
                var tableItemForTree = new TreeViewItem() { DataContext = table };
                tableItemForTree.Header = table.TableName;

                tableItemForTree.Selected += TableItemForTree_Selected;
                tableItemForTree.Style = Resources["tableStyle"] as Style;

                schema.Items.Add(tableItemForTree);

                foreach (var col in table.Columns)
                {
                    var itemCol = new TreeViewItem() { Header = col.ColumnName, DataContext = col };
                    itemCol.Style = Resources["columnStyle"] as Style;
                    tableItemForTree.Items.Add(itemCol);
                }
            }
        }

        private void LoadRules()
        {
            if (TreeViewForProject.Items.Count == 2)
                TreeViewForProject.Items.RemoveAt(1);

            var rules = new TreeViewItem
            {
                Header = MainResource.Rules
            };

            TreeViewForProject.Items.Add(rules);

            foreach (var table in project.Tables)
            {
                var tableItemForTree = new TreeViewItem() { DataContext = table };
                tableItemForTree.Header = table.TableName;

                bool existRule = false;
                foreach (var col in table.Columns)
                {
                    if (col.RuleForMasking != null)
                    {
                        var itemCol = new TreeViewItem() { Header = col.RuleForMasking.GetDescription(), DataContext = col.RuleForMasking };
                        tableItemForTree.Items.Add(itemCol);
                        existRule = true;
                    }
                }

                if (existRule)
                    rules.Items.Add(tableItemForTree);
            }
        }

        void Schema_Selected(object sender, RoutedEventArgs e)
        {
            var tablesUserCOntrol = new TablesUserControl(project.Tables);
            tablesUserCOntrol.OnUserAction += TablesUserCOntrol_OnUserAction;
            MainView.Content = tablesUserCOntrol;
            e.Handled = true;
        }

        void TableItemForTree_Selected(object sender, RoutedEventArgs e)
        {
            var tableDetail = new TableDetailUserControl() { DataContext = ((TreeViewItem)sender).DataContext };
            tableDetail.OnUserAction += TablesUserCOntrol_OnUserAction;
            MainView.Content = tableDetail;
            e.Handled = true;
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            logger.Fatal("Application crash", e.Exception);
            MessageToUser.Show(MainResource.Error, MainResource.ApplicationError + Environment.NewLine + MainResource.ApplicationErrorSecondLine);// + Environment.NewLine+e.Exception);
            e.Handled = true;
            Application.Current.Shutdown();
        }

        private void NewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            CreateNewProject();
        }

        private void CreateNewProject()
        {
            var newProject = ProjectCreateHelper.CreateNewProject(logger);
            if (newProject != null)
            {
                MainStartUserControl.Visibility = Visibility.Collapsed;
                TreeViewForProject.Visibility = Visibility.Visible;
                project = newProject;
                Refresh();
            }
        }

        public void OpenExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            OpenProject(null);
        }


        private void OpenProject(string path)
        {
            var newProject = ProjectFileHelper.Load(API, logger, path);
            if (newProject != null)
            {
                MainStartUserControl.Visibility = Visibility.Collapsed;
                TreeViewForProject.Visibility = Visibility.Visible;
                project = newProject;
                Refresh();
            }
        }

        private void SaveExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (ProjectFileHelper.Save(API, logger, project))
                SetTitle();
        }

        private void SaveAsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (ProjectFileHelper.Save(API, logger, project, true))
                SetTitle();
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void HelpExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            About a = new About();
            a.Owner = App.Current.MainWindow;
            a.ShowDialog();
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CanExecuteSave(object sender, CanExecuteRoutedEventArgs e)
        {
            if (project != null)
                e.CanExecute = true;
        }

        private void CanExecuteSaveAs(object sender, CanExecuteRoutedEventArgs e)
        {
            if (project != null)
                e.CanExecute = true;
        }

        private void TablesUserCOntrol_OnUserAction(object sender, ActionWithRuleEventArgs e)
        {
            switch (e.Action)
            {
                case ActionWithRuleType.AddOrEdit:
                    RuleAdder.AddRule(e.Columns.FirstOrDefault(), Refresh, this);
                    break;
                case ActionWithRuleType.Delete:
                    RuleDeleter.DeleteRules(e.Columns, Refresh);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void StartMaskingExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Masker.StartMasking(API, MaskingType.AllRule, Refresh, this);
        }

        private void StartMaskingUsedRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Masker.StartMasking(API, MaskingType.OnlyUsedRule, Refresh, this);
        }

        private void StartMaskingUnusedRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Masker.StartMasking(API, MaskingType.OnlyUnusedRule, Refresh, this);
        }

        private void CanExecuteStartMasking(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Masker.CanExecuteMasking(MaskingType.AllRule, project);
        }

        private void CanExecuteStartMaskingUsedRule(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Masker.CanExecuteMasking(MaskingType.OnlyUsedRule, project);
        }

        private void CanExecuteStartMaskingUnusedRule(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Masker.CanExecuteMasking(MaskingType.OnlyUnusedRule, project);
        }

        private void MenuImportCollection_Click(object sender, RoutedEventArgs e)
        {
            var usercontrol = new ImportCollectionUserControl();
            DialogWindow dialog = new DialogWindow(usercontrol);
            dialog.Owner = App.Current.MainWindow;
            dialog.ShowDialog();
        }

        private void MenuCollections_Click(object sender, RoutedEventArgs e)
        {
            var usercontrol = NinjectService.Instance.Get<CollectionsViewUserControl>();
            BaseWindow dialog = new BaseWindow(usercontrol, CollectionsResource.CollectionSummary);
            dialog.Owner = App.Current.MainWindow;
            dialog.ShowDialog();
        }

        private void Refresh()
        {
            LoadProject();
            LoadRules();
        }

        private void CanExecuteIfProjectLoaded(object sender, CanExecuteRoutedEventArgs e)
        {
            if (project != null)
            {
                e.CanExecute = true;
                e.Handled = true;
            }
        }

        private void AddRuleToSelectItemsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedColumns = ItemsGetter.GetSelectedColumns(MainView.Content);

            if (selectedColumns == null)
                return;

            if (selectedColumns.WithoutPrimaryKey.Count > 0)
                MessageToUser.Show(MessageForUserResource.Warrning, MessageForUserResource.RuleCanNotBeSetForSelectedColumns + Environment.NewLine
                  + MessageForUserResource.RuleCanNotBeSetForSelectedColumnsdExplanation);

            if (selectedColumns.WithPrimaryKey.Count == 0)
                MessageToUser.Show(MessageForUserResource.Warrning, MessageForUserResource.NoColumnsHaveBeenSelectedForWhichRuleCanBeSet + Environment.NewLine
                  + MessageForUserResource.NoColumnsHaveBeenSelectedForWhichRuleCanBeSetExplanation);
            else
                RuleAdder.BulkRuleAdd(selectedColumns.WithPrimaryKey, Refresh, this);
        }

        private void AutomaticAddRuleToAllExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            AutomaticRuleSetter ruleSetter = new AutomaticRuleSetter();
            ruleSetter.AddRuleForColumnWithoutRule(project.Tables);
            Refresh();
        }

        private void DeleteRuleFromSelectedItemsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var selectedcolumns = ItemsGetter.GetSelectedColumns(MainView.Content);

            if (selectedcolumns == null)
                return;

            if (selectedcolumns.WithPrimaryKey.Count == 0)
                MessageToUser.Show(MessageForUserResource.Warrning, MessageForUserResource.NoColumnsHaveBeenSelectedForWhichRuleCanBeSet + Environment.NewLine
                  + MessageForUserResource.NoColumnsHaveBeenSelectedForRuleDelete);
            else
            {
                List<Column> columns = new List<Column>();
                columns.AddRange(selectedcolumns.WithPrimaryKey);
                columns.AddRange(selectedcolumns.WithoutPrimaryKey);
                RuleDeleter.DeleteRules(columns, Refresh);
            }
        }

        private void DeleteAllRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            RuleDeleter.DeleteAllRules(project, Refresh);
        }

        private void CheckProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ProjectCheck.CheckForMasking();
        }

        private void UpdateByTarget_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ProjectCheck.CheckForUpdate(true))
                Refresh();
        }

        private void UpdateBySource_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ProjectCheck.CheckForUpdate(false))
                Refresh();
        }
    }
}
