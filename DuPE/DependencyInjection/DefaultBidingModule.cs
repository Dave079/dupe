﻿using DuPE.API;
using DuPE.API.Databases;
using DuPE.DAL.Collections.Parsers;
using DuPE.DAL.Collections.Repository;
using DuPE.DAL.Projects;
using DuPE.DAL.Reports;
using DuPE.Domain.Collections;
using DuPE.Domain.Databases;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects;
using DuPE.Domain.Reports;
using DuPE.GUI.DBConnectionCreaters;
using DuPE.Logging;
using DuPE.PostgreSQL;
using DuPE.SqlServer;
using log4net;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace DuPE.DependencyInjection
{
    public class DefaultBidingModule : NinjectModule
    {
        public override void Load()
        {
            var logger = new Log4NetLogger(LogManager.GetLogger("App"));

            var databaseRepositories = new Dictionary<string, Domain.Databases.IDatabaseRepositoryCreater>();
            var contentDictionary = new Dictionary<string, Tuple<UserControl, UserControl>>();

            AddSQLServerComponents(databaseRepositories, contentDictionary, logger);
            AddPostgreSQLComponents(databaseRepositories, contentDictionary, logger);

            var generalRepositoryGetter = new GeneralRepositoryGetter(databaseRepositories);

            var defaultProjectCheckerWithReportForUser = new DefaultProjectCheckerWithReportForUser(logger);
            var defaultProjectCheckerForDiferences = new DefaultProjectCheckerForDiferences(logger);
            var defaultProjectUpdater = new DefaultProjectUpdater();
            var collectionReposiory = new CollectionRepository();

            var API = new DuPEApi(
                 new ProjectSaverAndLoader(),
                 collectionReposiory,
                 defaultProjectCheckerWithReportForUser,
                 defaultProjectCheckerForDiferences,
                 defaultProjectUpdater,
                 generalRepositoryGetter
                 );

            Bind<ILogger>().ToConstant(logger);
            Bind<ICollectionRepository>().ToConstant(collectionReposiory);
            Bind<ICollectionParser>().To<CollectionTxtFileParser>();
            Bind<Dictionary<string, Tuple<UserControl, UserControl>>>().ToConstant(contentDictionary);
            Bind<DuPEApi>().ToConstant(API);
            Bind<IReportExporter>().To<ReportTxtExporter>();
        }

        private void AddPostgreSQLComponents(Dictionary<string, IDatabaseRepositoryCreater> databaseRepositories, Dictionary<string, Tuple<UserControl, UserControl>> contentDictionary, ILogger logger)
        {
            PostgreSQLDatabaseRepositoryCreater PostgreSQL = new PostgreSQLDatabaseRepositoryCreater(logger);

            databaseRepositories.Add(PostgreSQL.Name, PostgreSQL);
            contentDictionary.Add(
               PostgreSQL.Name,
                    new Tuple<UserControl, UserControl>(
                        new PostgrSQLConnectionStringCreaterUserControl(true),
                        new PostgrSQLConnectionStringCreaterUserControl(false)
                        )
               );
        }

        private void AddSQLServerComponents(Dictionary<string, IDatabaseRepositoryCreater> databaseRepositories, Dictionary<string, Tuple<UserControl, UserControl>> contentDictionary, ILogger logger)
        {
            SqlDatabaseRepositoryCreater SQLServer = new SqlDatabaseRepositoryCreater(logger);

            databaseRepositories.Add(SQLServer.Name, SQLServer);
            contentDictionary.Add(
                    SQLServer.Name,
                    new Tuple<UserControl, UserControl>(
                        new SqlServerConnectionStringCreaterUserControl(true),
                        new SqlServerConnectionStringCreaterUserControl(false)
                        )
                );
        }
    }
}
