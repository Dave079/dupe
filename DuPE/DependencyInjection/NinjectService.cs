﻿using Ninject;
using Ninject.Modules;
using System.Collections.Generic;

namespace DuPE.DependencyInjection
{
    public class NinjectService
    {
        private static NinjectService instance;
        private IKernel kernel;

        public static NinjectService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NinjectService();
                }
                return instance;
            }
        }

        private NinjectService()
        {
            SetKernel();
        }

        public void ReloadKernel()
        {
            SetKernel();
        }

        private void SetKernel()
        {
            kernel = new StandardKernel();
            var modules = new List<NinjectModule>();
            modules.Add(new DefaultBidingModule());

            kernel.Load(modules);
        }

        public T Get<T>()
        {
            return kernel.Get<T>();
        }

        public T Get<T>(string path)
        {
            return kernel.Get<T>(path);
        }
    }

}
