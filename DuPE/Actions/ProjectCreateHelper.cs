﻿using DuPE.API;
using DuPE.DependencyInjection;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects;
using DuPE.GUI.NewProject;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DuPE.Actions
{
    public class ProjectCreateHelper
    {

        public static Project CreateNewProject(ILogger logger)
        {
            var newProjectUserControl = NinjectService.Instance.Get<NewProjectWindow>();
            var API = NinjectService.Instance.Get<DuPEApi>();

            newProjectUserControl.Owner = App.Current.MainWindow;

            if (newProjectUserControl.ShowDialog() == true)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                try
                {
                    var source = newProjectUserControl.SourceConnectionCreater.Get();
                    var target = newProjectUserControl.TargetConnectionCreater.Get();

                    API.CreateProject(
                        source.Name, newProjectUserControl.ProjectName,
                        source.Parameters,
                        target?.Parameters
                        );

                    return API.ActualProject;
                }
                catch (Exception ee)
                {
                    logger.Error(ee);
                    MessageToUser.Show(MainResource.Error, ee.Message);
                    return null;
                }
                finally
                {
                    Mouse.OverrideCursor = null;
                }
            }
            else return null;
        }
    }
}
