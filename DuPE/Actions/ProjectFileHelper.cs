﻿using DuPE.API;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects;
using DuPE.GUI.Tools;
using DuPE.Properties;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WinForms = System.Windows.Forms;

namespace DuPE.Actions
{
    public class ProjectFileHelper
    {
        private const string DUPE_PROJECT_FILE_PATERN = "DuPE files (*.dupe)|*.dupe";

        public static bool Save(DuPEApi API, ILogger logger, Project project, bool saveAs = false)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                if (string.IsNullOrWhiteSpace(project.Path) || saveAs)
                {
                    var sfd = new WinForms.SaveFileDialog();

                    sfd.Filter = DUPE_PROJECT_FILE_PATERN;
                    sfd.FilterIndex = 1;
                    sfd.RestoreDirectory = true;

                    if (sfd.ShowDialog() == WinForms.DialogResult.OK)
                    {
                        project.Path = sfd.FileName;
                        API.SaveProject();
                        Settings.Default.AppSettings.AddProject(project);
                    }
                    else
                        return false;
                }

                API.SaveProject();
                Settings.Default.AppSettings.AddProject(project);
                return true;
            }
            catch (Exception ee)
            {
                logger.Error(ee);
                MessageToUser.Show(MainResource.Error, ee.Message);
                return false;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        public static Project Load(DuPEApi API, ILogger logger, string path)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                if (string.IsNullOrWhiteSpace(path))
                {
                    WinForms.OpenFileDialog filDlg = new WinForms.OpenFileDialog();
                    filDlg.Title = MainResource.OpenProjectTittle;
                    filDlg.Filter = DUPE_PROJECT_FILE_PATERN;

                    if (filDlg.ShowDialog() == WinForms.DialogResult.OK)
                    {
                        path = filDlg.FileName;
                    }
                    else
                        return null;
                }

                API.LoadProject(path);
                Settings.Default.AppSettings.AddProject(API.ActualProject);
                return API.ActualProject;
            }
            catch (Exception ee)
            {
                Settings.Default.AppSettings.RemoveProject(path);
                logger.Error(ee);
                MessageToUser.Show(MainResource.Error, ee.Message);
                return null;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }
    }
}
