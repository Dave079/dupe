﻿using DuPE.GUI.Tables;
using DuPE.Tools;
using System;

namespace DuPE.Actions
{
    public class ItemsGetter
    {
        public static SelectedColumns GetSelectedColumns(Object content)
        {
            if (content is TablesUserControl)
            {
                return ((TablesUserControl)content).GetAllSelectedColumnsColumns();
            }
            else if (content is TableDetailUserControl)
            {
                return ((TableDetailUserControl)content).GetSelectedColumns();
            }

            return null;
        }

        public static SelectedColumns GetAllColumns(Object content)
        {
            if (content is TablesUserControl)
            {
                return ((TablesUserControl)content).GetAllSelectedColumnsColumns();
            }
            else if (content is TableDetailUserControl)
            {
                return ((TableDetailUserControl)content).GetSelectedColumns();
            }

            return null;
        }
    }
}
