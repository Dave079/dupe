﻿using DuPE.API;
using DuPE.DependencyInjection;
using DuPE.Domain.Projects;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Reports;
using DuPE.GUI;
using DuPE.GUI.ProjectUpdates;
using DuPE.GUI.Reports;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System.Collections.Generic;
using System.Windows.Input;

namespace DuPE.Actions
{
    public class ProjectCheck
    {
        public static bool CheckForUpdate(bool byTarget)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            DuPEApi api = NinjectService.Instance.Get<DuPEApi>();
            IList<ProjectDifference> baseDifferences;

            if (byTarget)
            {
                if (api.IsProjectSameAsTargetDatabase(out baseDifferences))
                {
                    InfoForUser(MessageForUserResource.ProjectIsUpToDate);
                    Mouse.OverrideCursor = null;
                    return false;
                }
            }
            else
            {
                if (api.IsProjectSameAsTargetDatabase(out baseDifferences))
                {
                    InfoForUser(MessageForUserResource.ProjectIsUpToDate);
                    Mouse.OverrideCursor = null;
                    return false;
                }
            }

            var usercontrol = new ProjectUpdateUserControl(baseDifferences);
            DialogWindow dialog = new DialogWindow(usercontrol);
            dialog.Owner = App.Current.MainWindow;
            Mouse.OverrideCursor = null;
            return (dialog.ShowDialog() == true);

        }

        private static void InfoForUser(string mssage)
        {
            Mouse.OverrideCursor = null;
            MessageToUser.Show(MessageForUserResource.Info, mssage);
        }

        public static void CheckForMasking()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            DuPEApi api = NinjectService.Instance.Get<DuPEApi>();

            if (!api.IsProjectReadyForMasking(out Report report, TypeOfControl.CompleteCheck))
            {
                var win = new ReportWindow(report.Messages, false);
                Mouse.OverrideCursor = null;

                if (win.ShowDialog() != true)
                    return;
            }
            else
                InfoForUser(MessageForUserResource.ProjectIsReadyToMasking);

            Mouse.OverrideCursor = null;
        }
    }
}
