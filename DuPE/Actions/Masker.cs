﻿using DuPE.API;
using DuPE.Domain.Masking;
using DuPE.Domain.Projects;
using DuPE.Domain.Reports;
using DuPE.GUI.Reports;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace DuPE.Actions
{
    public class Masker
    {
        public static bool CanExecuteMasking(MaskingType type, Project project)
        {
            if (project == null)
                return false;
            else
            {
                foreach (var tab in project.Tables)
                    if (tab.Columns != null)
                        foreach (var col in tab.Columns)
                        {
                            switch (type)
                            {
                                case MaskingType.AllRule:
                                    if (col.RuleForMasking != null)
                                        return true;
                                    break;
                                case MaskingType.OnlyUnusedRule:
                                    if (col.RuleForMasking != null && !col.RuleForMasking.WasUsed())
                                        return true;
                                    break;
                                case MaskingType.OnlyUsedRule:
                                    if (col.RuleForMasking != null && col.RuleForMasking.WasUsed())
                                        return true;
                                    break;
                                default:
                                    throw new Exception();
                            }
                        }
            }
            return false;
        }

        public static void StartMasking(DuPEApi API, MaskingType type, Action Refresh, Window main)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            if (!API.IsProjectReadyForMasking(out Report report, TypeOfControl.CompleteCheck))
            {
                var win = new ReportWindow(report.Messages, !report.IsInErrorMessage());
                Mouse.OverrideCursor = null;
                if (win.ShowDialog() != true)
                {
                    return;
                }
                else
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var count = API.StartMasking(type);

            stopwatch.Stop();
            Mouse.OverrideCursor = null;

            MessageToUser.Show(MessageForUserResource.Info, MessageForUserResource.MaskingCompleted + Environment.NewLine + String.Format(MessageForUserResource.CountOfUsedRules + Environment.NewLine + MessageForUserResource.MaskingTime, count, stopwatch.Elapsed));
            Refresh();

        }
    }
}
