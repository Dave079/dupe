﻿using DuPE.Domain.Columns;
using DuPE.Domain.Projects;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Windows;

namespace DuPE.Actions
{
    public class RuleDeleter
    {
        public static void DeleteRules(IList<Column> list, Action refresh,bool All = false)
        {
            string Question = "";
            if (All)
                Question = MessageForUserResource.AreYouSureYouWantToRemoveAllTheRules;
            else if (list.Count > 1)
                Question = MessageForUserResource.AreYouSureYouWantToRemoveSelectedRules;
            else
                Question = MessageForUserResource.AreYouSureYouWantToRemoveSelectedRule;

            if (MessageToUser.ShowYesNoQuestion(MessageForUserResource.Question, Question))
            {
                foreach (var column in list)
                {
                    column.RuleForMasking = null;
                }
                refresh();
            }
        }

        public static void DeleteAllRules(Project project, Action refresh)
        {
            List<Column> columns = new List<Column>();
            foreach (var table in project.Tables)
            {
                columns.AddRange(table.Columns);
            }
            DeleteRules(columns, refresh,true);
        }
    }
}
