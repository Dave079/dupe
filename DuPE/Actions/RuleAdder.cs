﻿using DuPE.Domain.Columns;
using DuPE.GUI;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace DuPE.Actions
{
    public class RuleAdder
    {
        public static void BulkRuleAdd(IList<Column> columns, Action refresh, Window main)
        {
            Dictionary<string, List<Column>> dict = new Dictionary<string, List<Column>>();

            foreach (var column in columns)
            {
                if (column.RuleForMasking != null)
                    continue;

                if (!dict.ContainsKey(column.Type.OriginalName))
                    dict.Add(column.Type.OriginalName, new List<Column>());
                dict[column.Type.OriginalName].Add(column);
            }

            var list = dict.Values.ToList().OrderBy(x => x.Count).ToList();

            foreach (var items in list)
            {
                var col = items.FirstOrDefault();
                if (DialogWindowGetter.GetDialogByDataType(col) != null)
                {
                    if (AddRule(items.FirstOrDefault(), refresh, main, false))
                    {
                        foreach (var item in items)
                        {
                            item.RuleForMasking = col.RuleForMasking.CreateCopy(item);
                        }
                    }
                }
                else
                    MessageToUser.Show(MessageForUserResource.Info, string.Format(MessageForUserResource.DataTypeIsNotSupported, col.Type.OriginalName));
            }
            refresh?.Invoke();
        }


        public static bool AddRule(Column column, Action refresh, Window main, bool withRefresh = true)
        {
            DialogWindow dialog = DialogWindowGetter.GetDialogByDataType(column);

            if (dialog != null)
            {
                dialog.Owner = App.Current.MainWindow;
                if (dialog.ShowDialog() == true)
                {
                    if (withRefresh)
                        refresh();

                    return true;
                }
            }
            else
                MessageToUser.Show(MessageForUserResource.Info,string.Format(MessageForUserResource.DataTypeIsNotSupported, column.Type.OriginalName));

            return false;
        }
    }
}
