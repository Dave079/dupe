﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.GUI;
using DuPE.GUI.RulesFormMasking;
using DuPE.Resources.Strings;

namespace DuPE.Actions
{
    public class DialogWindowGetter
    {
        public static DialogWindow GetDialogByDataType(Column column)
        {
            if (column.Type is NumberDataType)
                return new DialogWindow(new RuleForNumber(column), true, GetTittle(column));
            else if (column.Type is StringDataType)
                return new DialogWindow(new RuleForString(column), true, GetTittle(column));
            else if (column.Type is DateTimeDataType)
                return new DialogWindow(new RuleForDateTime(column), false, GetTittle(column));
            else if (column.Type is TimeDataType)
                return new DialogWindow(new RuleForTime(column), false, GetTittle(column));
            return null;
        }

        private static string GetTittle(Column column)
        {
            return string.Format(MainResource.ForDataType, column.Type.OriginalName.ToString());
        }
    }
}
