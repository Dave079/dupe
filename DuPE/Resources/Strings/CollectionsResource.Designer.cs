﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DuPE.Resources.Strings {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CollectionsResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CollectionsResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DuPE.Resources.Strings.CollectionsResource", typeof(CollectionsResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opravdu chcete vybranou kolekci odebrat?.
        /// </summary>
        public static string AreYouSureYouWantToRemoveTheCollection {
            get {
                return ResourceManager.GetString("AreYouSureYouWantToRemoveTheCollection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vyberte soubor pro import.
        /// </summary>
        public static string ChooseFileForImport {
            get {
                return ResourceManager.GetString("ChooseFileForImport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vyberte položku pro odebrání..
        /// </summary>
        public static string ChooseItemForRemove {
            get {
                return ResourceManager.GetString("ChooseItemForRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Přehed kolekcí.
        /// </summary>
        public static string CollectionSummary {
            get {
                return ResourceManager.GetString("CollectionSummary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chyba.
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ukázka.
        /// </summary>
        public static string Exmample {
            get {
                return ResourceManager.GetString("Exmample", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cesta k souboru.
        /// </summary>
        public static string FilePath {
            get {
                return ResourceManager.GetString("FilePath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Importovat.
        /// </summary>
        public static string Import {
            get {
                return ResourceManager.GetString("Import", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Import datové kolekce.
        /// </summary>
        public static string ImportTittle {
            get {
                return ResourceManager.GetString("ImportTittle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Název.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Počet záznamů.
        /// </summary>
        public static string NumberOfRecords {
            get {
                return ResourceManager.GetString("NumberOfRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Odebrat.
        /// </summary>
        public static string Remove {
            get {
                return ResourceManager.GetString("Remove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Typ.
        /// </summary>
        public static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
    }
}
