﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DuPE.Resources.Strings {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RulesResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RulesResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DuPE.Resources.Strings.RulesResource", typeof(RulesResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Přiřadit náhodně.
        /// </summary>
        public static string AssignRandomly {
            get {
                return ResourceManager.GetString("AssignRandomly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Přiřadit pomocí hashe.
        /// </summary>
        public static string AssignWithHash {
            get {
                return ResourceManager.GetString("AssignWithHash", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kolekce znaků.
        /// </summary>
        public static string CharCollection {
            get {
                return ResourceManager.GetString("CharCollection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kolekce.
        /// </summary>
        public static string Collection {
            get {
                return ResourceManager.GetString("Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Od.
        /// </summary>
        public static string From {
            get {
                return ResourceManager.GetString("From", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hashovací funkce.
        /// </summary>
        public static string HashFunction {
            get {
                return ResourceManager.GetString("HashFunction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zahashovaním původní hodnoty.
        /// </summary>
        public static string HashingOriginalValue {
            get {
                return ResourceManager.GetString("HashingOriginalValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vkládat hodnotu null s pravděpodobností.
        /// </summary>
        public static string InsertionNullWithProbability {
            get {
                return ResourceManager.GetString("InsertionNullWithProbability", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximum.
        /// </summary>
        public static string Max {
            get {
                return ResourceManager.GetString("Max", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximální délka.
        /// </summary>
        public static string MaxLength {
            get {
                return ResourceManager.GetString("MaxLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mininum.
        /// </summary>
        public static string Min {
            get {
                return ResourceManager.GetString("Min", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Minimální délka.
        /// </summary>
        public static string MinLength {
            get {
                return ResourceManager.GetString("MinLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Počet desetiných míst.
        /// </summary>
        public static string NumberOfDecimalPlaces {
            get {
                return ResourceManager.GetString("NumberOfDecimalPlaces", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Náhodně generované.
        /// </summary>
        public static string RandomGenerated {
            get {
                return ResourceManager.GetString("RandomGenerated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pravidlo pro maskování datumu.
        /// </summary>
        public static string RuleForDateMasking {
            get {
                return ResourceManager.GetString("RuleForDateMasking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pravidlo pro maskování čísla.
        /// </summary>
        public static string RuleForNumberMasking {
            get {
                return ResourceManager.GetString("RuleForNumberMasking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pravidlo pro maskování řetězce.
        /// </summary>
        public static string RuleForStringMasking {
            get {
                return ResourceManager.GetString("RuleForStringMasking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pravidlo pro maskování času.
        /// </summary>
        public static string RuleForTimeMasking {
            get {
                return ResourceManager.GetString("RuleForTimeMasking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vybrané z kolekce.
        /// </summary>
        public static string SelectedFromCollection {
            get {
                return ResourceManager.GetString("SelectedFromCollection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do.
        /// </summary>
        public static string To {
            get {
                return ResourceManager.GetString("To", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Byly zadány neočekávané hodnoty, zkuste je prosím zadat znovu..
        /// </summary>
        public static string UnexpectedValuesHaveBeenEntered_PleaseTryAgain {
            get {
                return ResourceManager.GetString("UnexpectedValuesHaveBeenEntered_PleaseTryAgain", resourceCulture);
            }
        }
    }
}
