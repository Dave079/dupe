﻿using System;
using System.Management;

namespace DuPE.Tools
{
    public static class PC_UniqueID
    {
        public static string GetUniqueIDBy_SystemDiskSerialNumber_MotherboardSerialNumber()
        {
            return HashCreater.GetHash(string.Format("D{{{0}}}_B{{{1}}}", GetSystemDiskSerialNumber(), BaseBoardID()));
        }

        private static string BaseBoardID()
        {
            return identifier("Win32_BaseBoard", "SerialNumber");
        }

        private static string cpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        //BIOS Identifier
        private static string biosId()
        {
            return identifier("Win32_BIOS", "Manufacturer")
            + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
            + identifier("Win32_BIOS", "IdentificationCode")
            + identifier("Win32_BIOS", "SerialNumber")
            + identifier("Win32_BIOS", "ReleaseDate")
            + identifier("Win32_BIOS", "Version");
        }
        //Main physical hard drive ID
        private static string diskId()
        {
            return identifier("Win32_DiskDrive", "Model")
            + identifier("Win32_DiskDrive", "Manufacturer")
            + identifier("Win32_DiskDrive", "Signature")
            + identifier("Win32_DiskDrive", "TotalHeads");
        }
        //Motherboard ID
        private static string baseId()
        {
            return identifier("Win32_BaseBoard", "Model")
            + identifier("Win32_BaseBoard", "Manufacturer")
            + identifier("Win32_BaseBoard", "Name")
            + identifier("Win32_BaseBoard", "SerialNumber");
        }
        //Primary video controller ID
        private static string videoId()
        {
            return identifier("Win32_VideoController", "DriverVersion")
            + identifier("Win32_VideoController", "Name");
        }
        //First enabled network card ID
        private static string macId()
        {
            return identifier("Win32_NetworkAdapterConfiguration",
                "MACAddress", "IPEnabled");
        }

        private static string GetSystemDiskSerialNumber()
        //Return a hardware identifier
        {

            string SystemDeviceID = GetSystemDeviceID();
            string DeviceID = "";
            string result = "";
            string inetrfaceType = "";
            string driveType = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass("Win32_DiskDrive");
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                try
                {
                    inetrfaceType = mo["InterfaceType"].ToString();
                    switch (inetrfaceType)
                    {
                        case ("SCSI"):
                        case ("HDC"):
                        case ("IDE"):
                            break;
                        default:
                            continue;
                    }

                    driveType = mo["MediaType"].ToString();
                    switch (driveType)
                    {
                        case ("Fixed hard disk media"):
                            break;
                        default:
                            continue;
                    }

                    DeviceID = mo["DeviceID"].ToString();
                    if (DeviceID == SystemDeviceID)
                    {
                        result = mo["SerialNumber"].ToString();
                        break;
                    }
                }
                catch (Exception)
                {

                }
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                throw new Exception("Disk serial number not found");
            }

            return result;
        }

        private static string GetSystemDeviceID()
        {
            var logicalDiskId = System.IO.Path.GetPathRoot(Environment.SystemDirectory).Replace(@"\", "");
            var deviceId = string.Empty;
            var query = "ASSOCIATORS OF {Win32_LogicalDisk.DeviceID='" + logicalDiskId + "'} WHERE AssocClass = Win32_LogicalDiskToPartition";
            var queryResults = new ManagementObjectSearcher(query);
            var partitions = queryResults.Get();

            foreach (var partition in partitions)
            {
                query = "ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition";
                queryResults = new ManagementObjectSearcher(query);
                var drives = queryResults.Get();

                foreach (var drive in drives)
                    deviceId = drive["DeviceID"].ToString();
            }
            return deviceId;
        }

        private static string identifier(string wmiClass, string wmiProperty)
        //Return a hardware identifier
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                result = mo[wmiProperty].ToString();
            }
            return result;
        }

        private static string identifier
        (string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            System.Management.ManagementClass mc =
        new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                }
            }
            return result;
        }
    }
}
