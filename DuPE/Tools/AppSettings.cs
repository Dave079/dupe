﻿using DuPE.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DuPE.Tools
{
    [Serializable]
    public class AppSettings
    {
        private const byte numberOfSoredRecentsProjects = 10;
        public List<RecentProject> RecentProjects { get; set; }
        public AppSettings()
        {

        }

        public void AddProject(Project project)
        {
            var storedProject = RecentProjects.Where(x => x.Name == project.Name && x.Path == project.Path).FirstOrDefault();

            if (storedProject == null)
                RecentProjects.Add(new RecentProject() { Name = project.Name, Path = project.Path, LastOpen = DateTime.Now });
            else
                storedProject.LastOpen = DateTime.Now;

            if (RecentProjects.Count > numberOfSoredRecentsProjects)
            {
                var projectForRemoce = RecentProjects.OrderByDescending(x => x.LastOpen).LastOrDefault();
                RemoveProject(projectForRemoce);
            }

        }

        public void RemoveProject(RecentProject recentProject)
        {
            var projectForRemoce = RecentProjects.Where(x => x.Name == recentProject.Name && x.Path == recentProject.Path).FirstOrDefault();
            if (projectForRemoce != null)
                RecentProjects.Remove(projectForRemoce);
        }

        public void RemoveProject(string path)
        {
            var projectForRemoce = RecentProjects.Where(x => x.Path == path).FirstOrDefault();
            if (projectForRemoce != null)
                RecentProjects.Remove(projectForRemoce);
        }
    }

    [Serializable]
    public class RecentProject
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public DateTime LastOpen { get; set; }
    }
}
