﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;

namespace DuPE.Tools
{
    public sealed class AppInfo
    {
        private static AppInfo instance;
        public string Name { get; set; }
        public string UniqueID { get; set; }
        public string Version { get; set; }
        public string VersionShort { get; set; }
        public string Location { get; set; }
        public string OS64x32bit { get; set; }
        public string ServicePack { get; set; }
        public string CustomerProfileName { get; set; }
        public string OperationSystem { get; set; }
        public string NameWithCustomer { get { return string.Format("{0} {1}", Name, CustomerProfileName); } }
        public string AppTittle { get { return string.Format("{0} {1} {2}", Name, VersionShort, CustomerProfileName); } }

        public const string StringDefaultData = "NA";

        public static AppInfo Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AppInfo();
                }
                return instance;
            }
        }

        private AppInfo()
        {
            OperationSystem = StringDefaultData;

            OS64x32bit = StringDefaultData;
            ServicePack = StringDefaultData;

            UniqueID = StringDefaultData;

            Name = StringDefaultData;
            Version = StringDefaultData;
            Location = StringDefaultData;
            CustomerProfileName = StringDefaultData;
        }

        public void Initialize(System.Reflection.Assembly assembly, string CustomerName = null)
        {
            var nameOS = (from x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().OfType<ManagementObject>()
                          select x.GetPropertyValue("Caption")).FirstOrDefault();
            OperationSystem = nameOS != null ? nameOS.ToString() : "Unknown";

            OperatingSystem os = Environment.OSVersion;
            String sp = os.ServicePack;

            OS64x32bit = Environment.Is64BitOperatingSystem ? "64" : "32";
            ServicePack = string.IsNullOrWhiteSpace(sp) ? "" : sp;

            UniqueID = PC_UniqueID.GetUniqueIDBy_SystemDiskSerialNumber_MotherboardSerialNumber();

            Name = assembly.GetName().Name;
            Version = assembly.GetName().Version.ToString();
            VersionShort = string.Format("{0}.{1}", assembly.GetName().Version.Major.ToString(), assembly.GetName().Version.Minor.ToString());
            Location = assembly.Location.ToString();
            CustomerProfileName = CustomerName;
        }

    }
}
