﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DuPE.Tools
{
    public class RunningProcessChecker
    {
        private static Mutex _mutex;

        public static bool isThisAppAlreadyRunning(string MutexName)
        {
            bool createdNew;
            _mutex = new Mutex(true, MutexName, out createdNew);

            if (!createdNew)
                return true;
            else
                return false;
        }

        public static Process FindRunningProcessWithSameName()
        {
            Process currentProcess = Process.GetCurrentProcess();

            var runningProcess = (from process in Process.GetProcesses()
                                  where
                                    process.Id != currentProcess.Id &&
                                    process.ProcessName.Equals(
                                      currentProcess.ProcessName,
                                      StringComparison.Ordinal)
                                  select process).FirstOrDefault();

            return runningProcess;
        }
    }
}
