﻿using DuPE.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Tools
{
    public class SettingsPreparer
    {
        public static void LoadAndPrepare()
        {
            if (Settings.Default.UpgradeRequired)
            {
                Settings.Default.Upgrade();
                Settings.Default.UpgradeRequired = false;
                Settings.Default.Save();
            }

            if (Settings.Default.AppSettings == null)
                Settings.Default.AppSettings = new AppSettings() { RecentProjects = new List<RecentProject>() };
        }

        public static void Save()
        {
            Settings.Default.Save();
        }
    }
}
