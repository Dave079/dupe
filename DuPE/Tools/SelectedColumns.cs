﻿using DuPE.Domain.Columns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Tools
{
    public class SelectedColumns
    {
        public IList<Column> WithPrimaryKey { get; private set; }
        public IList<Column> WithoutPrimaryKey { get; private set; }

        public SelectedColumns(IList<Column> WithPrimaryKey, IList<Column> WithoutPrimaryKey)
        {
            this.WithoutPrimaryKey = WithoutPrimaryKey;
            this.WithPrimaryKey = WithPrimaryKey;
        }
    }
}
