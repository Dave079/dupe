﻿using System.Security.Cryptography;
using System.Text;

namespace DuPE.Tools
{
    public class HashCreater
    {
        public static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            var br = sec.ComputeHash(bt);

            System.Text.StringBuilder ss = new System.Text.StringBuilder();

            for (int i = 0; i < br.Length; i++)
            {
                ss.Append(br[i].ToString("X2"));
                if ((i + 1) != br.Length && (i + 1) % 2 == 0)
                    ss.Append("-");
            }
            return ss.ToString();
        }
    }
}
