﻿using DuPE.API;
using DuPE.DependencyInjection;
using DuPE.Domain.Columns;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Tables;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.ProjectUpdates
{
    /// <summary>
    /// Interaction logic for ProjectUpdateUserControl.xaml
    /// </summary>
    public partial class ProjectUpdateUserControl : UserControl, IDialogAddEditContent
    {
        private IList<DifferenceViewModel> differences;
        private ILogger logger;

        public ProjectUpdateUserControl(IList<ProjectDifference> baseDifferences)
        {
            logger = NinjectService.Instance.Get<ILogger>();
            InitializeComponent();

            differences = CreateViewModels(baseDifferences);
            ItemsListView.ItemsSource = differences;
        }


        private IList<DifferenceViewModel> CreateViewModels(IList<ProjectDifference> projectDifferences)
        {
            var diffs = new List<DifferenceViewModel>();
            foreach (var projectDiff in projectDifferences)
            {
                var diffViewModel = new DifferenceViewModel() { Difference = projectDiff };

                if (projectDiff is TableDifference tableDiff)
                {
                    switch (tableDiff.Type)
                    {
                        case DifferenceType.Surplus:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.TableMisingInDB, GetDescription(tableDiff.Table));
                            diffViewModel.SolutionDescription = MessageForUserResource.TableMisingInDBSolution;
                            break;
                        case DifferenceType.Miss:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.TableMisingInProject, GetDescription(tableDiff.Table));
                            diffViewModel.SolutionDescription = MessageForUserResource.TableMisingInProjectSolution;
                            break;
                        case DifferenceType.AnotherType:
                        default:
                            throw new Exception();
                    }
                }
                else if (projectDiff is ColumnDifference columnDiff)
                {
                    switch (columnDiff.Type)
                    {
                        case DifferenceType.Surplus:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnMisingInDB, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnMisingInDBSolution;
                            break;
                        case DifferenceType.Miss:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnMisingInProject, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnMisingInProjectSolution;
                            break;
                        case DifferenceType.AnotherType:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnHaveAnotherType, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnHaveAnotherTypeSolution;
                            break;
                        case DifferenceType.AddFK:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnAddedForeignKey, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnAddedForeignKeySolution;
                            break;
                        case DifferenceType.RemoveFK:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnRemovedForeignKey, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnRemovedForeignKeySolution;
                            break;
                        case DifferenceType.AddPK:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnAddedPrimaryKey, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnAddedPrimaryKeySolution;
                            break;
                        case DifferenceType.RemovePK:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnRemovedPrimaryKey, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnRemovedPrimaryKeySolution;
                            break;
                        case DifferenceType.MovePK:
                            diffViewModel.DifferenceDescription = string.Format(MessageForUserResource.ColumnMovedPrimaryKey, GetDescription(columnDiff.Column));
                            diffViewModel.SolutionDescription = MessageForUserResource.ColumnMovedPrimaryKeySolution;
                            break;
                        default:
                            throw new Exception();
                    }
                }

                diffs.Add(diffViewModel);
            }

            return diffs;
        }

        private object GetDescription(Column column)
        {
            return string.Format("{0}.{1}.{2}.{3} ({4})", column.TableCatalog, column.TableSchema, column.TableName, column.ColumnName, column.Type.OriginalName);
        }

        private object GetDescription(Table table)
        {
            return string.Format("{0}.{1}.{2} ({3})", table.TableCatalog, table.TableSchema, table.TableName, table.TableType);
        }

        public string Title => "UPDATE";

        public bool CanExecuteSave()
        {
            return differences.Where(x => x.Update).Count() > 0;
        }

        public bool LeavingDialog(bool okDialog)
        {
            try
            {
                var api = NinjectService.Instance.Get<DuPEApi>();
                api.Update(differences.Where(x => x.Update).Select(x => x.Difference));
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
            return true;
        }

        public new UIElement Content()
        {
            return this;
        }
    }
}
