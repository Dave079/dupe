﻿using DuPE.Domain.Projects.PojectDifferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.GUI.ProjectUpdates
{
    public class DifferenceViewModel
    {
        public bool Update { get; set; }
        public string DifferenceDescription { get; set; }
        public string SolutionDescription { get; set; }
        public ProjectDifference Difference { get; set; }
    }
}
