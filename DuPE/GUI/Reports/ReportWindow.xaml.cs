﻿using DuPE.DependencyInjection;
using DuPE.Domain.Reports;
using DuPE.Resources.Strings;
using FirstFloor.ModernUI.Windows.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using WinForms = System.Windows.Forms;


namespace DuPE.GUI.Reports
{
    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : ModernWindow
    {
        private IReportExporter reportExporter;
        private IList<ReportMessage> reports;
        public ReportWindow(IList<ReportMessage> reports, bool canContinue)
        {
            reportExporter = NinjectService.Instance.Get<IReportExporter>();
            InitializeComponent();

            if (!canContinue)
                ContinueButton.IsEnabled = false;

            this.reports = reports;

            ReportListView.ItemsSource = this.reports;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(ReportListView.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Tittle");
            view.GroupDescriptions.Add(groupDescription);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            WinForms.SaveFileDialog filDlg = new WinForms.SaveFileDialog();
            filDlg.Title = ReportResource.ChooseFileNameForSave;
            filDlg.Filter = reportExporter.GetFileFilterForSuffix();

            if (filDlg.ShowDialog() == WinForms.DialogResult.OK)
            {
                reportExporter.SaveToFile(reports, filDlg.FileName);
            }
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
