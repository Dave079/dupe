﻿using DuPE.Properties;
using DuPE.Tools;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.StartView
{
    /// <summary>
    /// Interaction logic for StartUserControl.xaml
    /// </summary>
    public partial class StartUserControl : UserControl
    {
        public delegate void OpenProject(string path);
        public delegate void CreateProject();

        private OpenProject Open;
        private CreateProject Create;

        public StartUserControl()
        {
            InitializeComponent();
            ItemsControlOfReccentProjects.ItemsSource = Settings.Default.AppSettings.RecentProjects.OrderByDescending(x => x.LastOpen);
        }

        private void OpenProject_Click(object sender, RoutedEventArgs e)
        {
            Open(null);
            ItemsControlOfReccentProjects.ItemsSource = Settings.Default.AppSettings.RecentProjects.OrderByDescending(x => x.LastOpen);
        }

        private void CreateProject_Click(object sender, RoutedEventArgs e)
        {
            Create();
        }

        public void SetMethodOpenProject(OpenProject method)
        {
            Open = method;
        }

        public void SetMethodCreateProject(CreateProject method)
        {
            Create = method;
        }

        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var recentProject = (RecentProject)button.DataContext;
            Open(recentProject.Path);
            ItemsControlOfReccentProjects.ItemsSource = Settings.Default.AppSettings.RecentProjects.OrderByDescending(x => x.LastOpen);
        }
    }
}
