﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DuPE.GUI
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : ModernWindow
    {
        private IDialogAddEditContent DialogAddEditContent;
        private bool _validateAfterClickOnSaveButton;

        public DialogWindow(IDialogAddEditContent IDialogAddEditContent, bool ValidateAfterClickOnSaveButton = false,string title="")
        {
            Title = title;
            this.DialogAddEditContent = IDialogAddEditContent;
            _validateAfterClickOnSaveButton = ValidateAfterClickOnSaveButton;
            InitializeComponent();
            DataContext = DialogAddEditContent;
            MainContent.Children.Add(DialogAddEditContent.Content());
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CanExecuteSave(object sender, CanExecuteRoutedEventArgs e)
        {
            if (_validateAfterClickOnSaveButton)
                e.CanExecute = true;
            else
                e.CanExecute = DialogAddEditContent.CanExecuteSave();
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DialogAddEditContent.LeavingDialog(false);
            DialogResult = false;
            this.Close();
        }

        private void SaveExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (_validateAfterClickOnSaveButton)
            {
                if (!DialogAddEditContent.CanExecuteSave())
                    return;
            }

            if (DialogAddEditContent.LeavingDialog(true))
            {
                DialogResult = true;
                this.Close();
            }
        }
    }
}
