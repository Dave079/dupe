﻿using System.Collections.Generic;
using System.ComponentModel;

namespace DuPE.GUI.Tables
{
    public class TableViewModel : INotifyPropertyChanged
    {

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
        }

        private List<ColumnViewModel> _Columns;
        public List<ColumnViewModel> Columns
        {
            get { return _Columns; }
            set
            {
                _Columns = value;
                OnPropertyChanged("Columns");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ColumnViewModel : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsKey { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
