﻿using DuPE.Domain.Columns;
using DuPE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace DuPE.GUI.Tables
{
    /// <summary>
    /// Interaction logic for TableUserControl.xaml
    /// </summary>
    public partial class TableUserControl : UserControl
    {
        public static readonly RoutedCommand AddRule = new RoutedCommand();
        public static readonly RoutedCommand DeleteRule = new RoutedCommand();

        public delegate void UserAction(object sender, ActionWithRuleEventArgs e);
        public event UserAction OnUserAction;

        public TableUserControl()
        {
            InitializeComponent();
        }

        public SelectedColumns GetSelectedColumns()
        {
            var withKey = new List<Column>();
            var withoutKey = new List<Column>();

            foreach (Column col in ICColumns.SelectedItems)
                if (!col.IsforeignKey && !col.IsPrimaryKey)
                {
                    if ((DataContext as DuPE.Domain.Tables.Table).HavePrimaryKey)
                        withKey.Add(col);
                    else
                        withoutKey.Add(col);
                }

            return new SelectedColumns(withKey, withoutKey);
        }

        private void UserActionEvent(ActionWithRuleType action)
        {
            if (OnUserAction == null)
                return;
            ActionWithRuleEventArgs args = new ActionWithRuleEventArgs(ICColumns.SelectedItems.Cast<Column>().ToList(), action);
            OnUserAction(this, args);
        }

        private void CanExecuteDeleteRule(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            if (ICColumns.SelectedItem != null)
            {
                foreach (Column col in ICColumns.SelectedItems)
                {
                    if (col.RuleForMasking != null)
                    {
                        canExecute = true;
                        break;
                    }
                }
            }

            e.CanExecute = canExecute;
            e.Handled = true;
        }

        private void DeleteRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UserActionEvent(ActionWithRuleType.Delete);
        }

        private void CanExecuteAddRule(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanAddRule();
            e.Handled = true;
        }

        private void AddRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UserActionEvent(ActionWithRuleType.AddOrEdit);
        }

        private bool CanAddRule()
        {
            return (DataContext as DuPE.Domain.Tables.Table).HavePrimaryKey && ICColumns.SelectedItem != null &&
                !(ICColumns.SelectedItem as Column).IsPrimaryKey && !(ICColumns.SelectedItem as Column).IsforeignKey;
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CanAddRule())
                UserActionEvent(ActionWithRuleType.AddOrEdit);
        }
    }

    public class ActionWithRuleEventArgs : EventArgs
    {
        public List<Column> Columns { get; private set; }
        public ActionWithRuleType Action { get; private set; }

        public ActionWithRuleEventArgs(List<Column> columns, ActionWithRuleType action)
        {
            this.Columns = columns;
            this.Action = action;
        }
    }

    public enum ActionWithRuleType
    {
        AddOrEdit = 0,
        Delete = 1,
    }
}
