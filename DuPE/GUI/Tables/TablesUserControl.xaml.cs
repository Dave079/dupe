﻿using DuPE.Domain.Columns;
using DuPE.Tools;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace DuPE.GUI.Tables
{
    /// <summary>
    /// Interaction logic for TablesUserControl.xaml
    /// </summary>
    public partial class TablesUserControl : UserControl
    {
        public delegate void UserAction(object sender, ActionWithRuleEventArgs e);
        public event UserAction OnUserAction;

        public TablesUserControl(IList<Domain.Tables.Table> tables)
        {
            InitializeComponent();

            foreach (var table in tables)
            {
                var tab = new TableUserControl() { DataContext = table };
                tab.OnUserAction += tab_OnUserAction;
                ICTables.Children.Add(tab);
            }
        }

        void tab_OnUserAction(object sender, ActionWithRuleEventArgs e)
        {
            if (OnUserAction == null)
                return;

            OnUserAction(sender, e);
        }

        void tab_OnBulkRuleSet(object sender, EventArgs e)
        {
            BulkRuleSetEvent();
        }

        private void BulkRuleSetEvent()
        {

        }

        public SelectedColumns GetAllSelectedColumnsColumns()
        {
            var withKey = new List<Column>();
            var withoutKey = new List<Column>();

            var res = new List<Column>();
            foreach (TableUserControl item in ICTables.Children)
            {
                var selectedColumns = item.GetSelectedColumns();
                withKey.AddRange(selectedColumns.WithPrimaryKey);
                withoutKey.AddRange(selectedColumns.WithoutPrimaryKey);
            }
            return new SelectedColumns(withKey, withoutKey);
        }
    }

    public class BulkRuleSetEventArgs : EventArgs
    {
        public IList<Column> Columns { get; set; }

        public BulkRuleSetEventArgs(IList<Column> columns)
        {
            Columns = columns;
        }
    }
}
