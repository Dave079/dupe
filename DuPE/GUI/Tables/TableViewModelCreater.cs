﻿using System;
using System.Collections.Generic;

namespace DuPE.GUI.Tables
{
    public class TableViewModelCreater
    {
        public static IList<TableViewModel> Convert(IList<Domain.Tables.Table> tables)
        {
            if (tables == null)
                throw new ArgumentNullException();

            var list = new List<TableViewModel>();
            foreach (var table in tables)
            {
                var tabVIewModel = new TableViewModel() { Columns = new List<ColumnViewModel>() };
                tabVIewModel.Name = table.TableName;

                if (table.Columns != null)
                {
                    foreach (var column in table.Columns)
                    {
                        var colView = new ColumnViewModel();
                        colView.Name = column.ColumnName;
                        colView.Type = column.Type.OriginalName;
                        colView.IsKey = column.IsPrimaryKey || column.IsforeignKey;
                        tabVIewModel.Columns.Add(colView);
                    }
                }

                list.Add(tabVIewModel);
            }
            return list;
        }
    }
}
