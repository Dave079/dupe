﻿using DuPE.Domain.Columns;
using DuPE.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace DuPE.GUI.Tables
{
    /// <summary>
    /// Interaction logic for TableDetailUserControl.xaml
    /// </summary>
    public partial class TableDetailUserControl : UserControl
    {

        public static readonly RoutedCommand AddRule = new RoutedCommand();
        public static readonly RoutedCommand DeleteRule = new RoutedCommand();

        public delegate void UserAction(object sender, ActionWithRuleEventArgs e);
        public event UserAction OnUserAction;

        public TableDetailUserControl()
        {
            InitializeComponent();
        }

        public SelectedColumns GetSelectedColumns()
        {
            var res = new List<Column>();

            foreach (Column col in ICColumns.SelectedItems)
            {
                if (!col.IsforeignKey && !col.IsPrimaryKey)
                    res.Add(col);
            }

            if ((DataContext as DuPE.Domain.Tables.Table).HavePrimaryKey)
                return new SelectedColumns(res, new List<Column>());
            else
                return new SelectedColumns(new List<Column>(), res);
        }

        private void UserActionEvent(ActionWithRuleType action)
        {
            if (OnUserAction == null)
                return;
            ActionWithRuleEventArgs args = new ActionWithRuleEventArgs(ICColumns.SelectedItems.Cast<Column>().ToList(), action);
            OnUserAction(this, args);
        }

        private void CanExecuteDeleteRule(object sender, CanExecuteRoutedEventArgs e)
        {
            bool canExecute = false;
            if (ICColumns.SelectedItem != null)
            {
                foreach (Column col in ICColumns.SelectedItems)
                {
                    if (col.RuleForMasking != null)
                    {
                        canExecute = true;
                        break;
                    }
                }
            }

            e.CanExecute = canExecute;
            e.Handled = true;
        }

        private void DeleteRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UserActionEvent(ActionWithRuleType.Delete);
        }

        private void CanExecuteAddRule(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanAddRule();
            e.Handled = true;
        }

        private void AddRuleExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            UserActionEvent(ActionWithRuleType.AddOrEdit);
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CanAddRule())
                UserActionEvent(ActionWithRuleType.AddOrEdit);
        }

        private bool CanAddRule()
        {
            return (DataContext as DuPE.Domain.Tables.Table).HavePrimaryKey && ICColumns.SelectedItem != null &&
                !(ICColumns.SelectedItem as Column).IsPrimaryKey && !(ICColumns.SelectedItem as Column).IsforeignKey;
        }

    }
}
