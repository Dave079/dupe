﻿using FirstFloor.ModernUI.Windows.Controls;
using System.Windows;
using System.Windows.Input;

namespace DuPE.GUI
{
    /// <summary>
    /// Interaction logic for BaseWindow.xaml
    /// </summary>
    public partial class BaseWindow : ModernWindow
    {
        private IWindowContent WindowContent;

        public BaseWindow(IWindowContent WindowContent, string Name = "")
        {
            this.WindowContent = WindowContent;

            InitializeComponent();
            DataContext = WindowContent;
            MainContent.Children.Add(WindowContent.Content());

            if (!string.IsNullOrWhiteSpace(Name))
                TittleName.Content = Name;
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }
    }
}
