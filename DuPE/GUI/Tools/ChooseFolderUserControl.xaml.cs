﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;


namespace DuPE.GUI.Tools
{
    /// <summary>
    /// Interaction logic for ChooseFileUserControl.xaml
    /// </summary>
    public partial class ChooseFolderUserControl : UserControl
    {
        public delegate void PathChanged(object sender, PathEventArgs e);
        public event PathChanged OnPathChanged;

        public ChooseFolderUserControl()
        {
            FolderPath = "";
            DataContext = this;
            InitializeComponent();
        }

        public String FolderPath
        {
            get { return (String)GetValue(FolderPathProperty); }
            set { SetValue(FolderPathProperty, value); }
        }

        public static readonly DependencyProperty FolderPathProperty =
            DependencyProperty.Register("FolderPath", typeof(string),
              typeof(ChooseFolderUserControl), new PropertyMetadata(""));

        public String FolderPathName
        {
            get { return (String)GetValue(FolderPathNameProperty); }
            set { SetValue(FolderPathNameProperty, value); }
        }

        public static readonly DependencyProperty FolderPathNameProperty =
            DependencyProperty.Register("FolderPathName", typeof(string),
              typeof(ChooseFolderUserControl), new PropertyMetadata(""));

        public String DialogString
        {
            get { return (String)GetValue(DialogStringProperty); }
            set { SetValue(DialogStringProperty, value); }
        }

        public static readonly DependencyProperty DialogStringProperty =
            DependencyProperty.Register("DialogString", typeof(string),
              typeof(ChooseFolderUserControl), new PropertyMetadata(""));

        private void PathChangedEvent(string newPath, string oldPath)
        {
            if (OnPathChanged == null)
                return;
            PathEventArgs args = new PathEventArgs(newPath, oldPath);
            OnPathChanged(this, args);
        }

        private void Button_Click_folder(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folDlg = new WinForms.FolderBrowserDialog();
            folDlg.Description = DialogString;

            if (!string.IsNullOrWhiteSpace(FolderPath))
                folDlg.SelectedPath = FolderPath;

            if (folDlg.ShowDialog() == WinForms.DialogResult.OK)
            {
                if (FolderPath != folDlg.SelectedPath)
                {
                    string old = FolderPath;
                    FolderPath = folDlg.SelectedPath;
                    FolderPathTextBox.Text = FolderPath;

                    PathChangedEvent(FolderPath, old);
                }
            }
        }

        public void SetValidation(ValidationRule customRule)
        {
            Binding binding = BindingOperations.GetBinding(FolderPathTextBox, TextBlock.TextProperty);
            binding.ValidationRules.Clear();
            binding.ValidationRules.Add(customRule);
        }

        private void Button_Drop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null && files.Length > 0)
            {

                if (!Directory.Exists(files[0]))
                    return;

                string old = FolderPath;
                FolderPath = files[0];
                FolderPathTextBox.Text = FolderPath;

                PathChangedEvent(FolderPath, old);
            }
        }
    }

    public class PathEventArgs : EventArgs
    {
        public PathEventArgs(string newPath, string oldPath)
        {
            this.newPath = newPath;
            this.oldPath = oldPath;
        }

        public string newPath { get; set; }
        public string oldPath { get; set; }
    }
}