﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Markup;

namespace DuPE.GUI.Tools
{
    public class EnumToDictionnary : MarkupExtension
    {
        public Type TargetEnum { get; set; }
        public EnumToDictionnary(Type type) { TargetEnum = type; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var type = TargetEnum;
            if (type == null) return null;
            return RetrieveFromCacheOrAddIt(type);
        }

        private readonly Dictionary<Type, List<Tuple<Object, Object>>> _cache = new Dictionary<Type, List<Tuple<Object, Object>>>();
        private object RetrieveFromCacheOrAddIt(Type type)
        {
            if (!this._cache.ContainsKey(type))
            {
                var fields = type.GetFields().Where(field => field.IsLiteral);
                var values = new List<Tuple<Object, Object>>();
                foreach (var field in fields)
                {
                    var valueOfField = field.GetValue(type);
                    var newTuple = new Tuple<Object, Object>(valueOfField, valueOfField);
                    values.Add(newTuple);
                }
                this._cache[type] = values.OrderBy(x => x.Item2).ToList();
            }

            return this._cache[type];
        }
    }
}
