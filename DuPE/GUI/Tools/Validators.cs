﻿using DuPE.Resources.Strings;
using System;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace DuPE.GUI.Tools
{
    public class DateValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            var typedValue = (string)value;

            if (string.IsNullOrWhiteSpace(typedValue))
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);
            else
            {
                var val = typedValue.Replace(" ", "");

                if (!Regex.IsMatch(val, @"^(?:(?:31(\/|-|,|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|,|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9][0-9])?[0-9]{2})$|^(?:29(\/|-|,|\.)0?2\3(?:(?:(?:1[6-9]|[2-9][0-9])?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1[0-9]|2[0-8])(\/|-|,|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9][0-9])?[0-9]{2})$"))
                    return new ValidationResult(false, ValidatorsResource.InvalidFormat);
            }
            return ValidationResult.ValidResult;
        }
    }

    public class ProbabilityValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var typedValue = (string)value;
            double m;
            if (string.IsNullOrWhiteSpace(typedValue))
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);
            else
            {
                if (!(double.TryParse(typedValue, out m)))
                    return new ValidationResult
                    (false, ValidatorsResource.InvalidFormat);
                else if (m < 0 && m > 1)
                    return new ValidationResult
                   (false, ValidatorsResource.NumberMustBePositive);

            }
            return ValidationResult.ValidResult;
        }
    }

    public class NotEmptyCharValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            var typedValue = (string)value;

            if (string.IsNullOrWhiteSpace(typedValue))
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);
            else if (!Regex.IsMatch(((string)value), @"^[a-zA-Z0-9]+$"))
                return new ValidationResult(false, ValidatorsResource.ValueCanOnlyContainLettersAndNumbers);

            return ValidationResult.ValidResult;
        }
    }

    public class CoherentStringValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            var typedValue = (string)value;

            if (string.IsNullOrWhiteSpace(typedValue))
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);
            else if (typedValue.Contains(" "))
                return new ValidationResult(false, ValidatorsResource.ValueMustNotContainSpaces);

            return ValidationResult.ValidResult;
        }
    }

    public class NotEmptyStringValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            var typedValue = (string)value;
            if (string.IsNullOrWhiteSpace(typedValue))
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);

            return ValidationResult.ValidResult;
        }
    }

    public class TimeSpanValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || ((string)value).Trim() == "")
                return new ValidationResult(false, ValidatorsResource.ValueCanNotBeEmpty);
            else
            {
                if (!Regex.IsMatch(value as string, @"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$"))
                    return new ValidationResult(false, ValidatorsResource.InvalidTimeFormat);
            }
            return ValidationResult.ValidResult;
        }
    }
}
