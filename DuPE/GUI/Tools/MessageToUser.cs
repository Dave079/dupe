﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.GUI.Tools
{
    public class MessageToUser
    {
        public static void Show(string tittle, string message)
        {
            ModernDialog.ShowMessage(message, tittle, System.Windows.MessageBoxButton.OK, App.Current.MainWindow);
        }

        public static bool ShowYesNoQuestion(string tittle, string message)
        {
            return ModernDialog.ShowMessage(message, tittle, System.Windows.MessageBoxButton.YesNo, App.Current.MainWindow) == System.Windows.MessageBoxResult.Yes;
        }
    }
}
