﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;


namespace DuPE.GUI.Tools
{
    /// <summary>
    /// Interaction logic for ChooseFileUserControl.xaml
    /// </summary>
    public partial class ChooseFileUserControl : UserControl
    {
        public delegate void PathChanged(object sender, PathEventArgs e);
        public event PathChanged OnPathChanged;

        public ChooseFileUserControl()
        {
            DataContext = this;
            InitializeComponent();
        }

        public String FilePath
        {
            get { return (String)GetValue(FilePathProperty); }
            set
            {
                SetValue(FilePathProperty, value);
            }
        }

        public static readonly DependencyProperty FilePathProperty =
            DependencyProperty.Register("FilePath", typeof(string),
              typeof(ChooseFileUserControl), new PropertyMetadata(""));

        public String FilePathName
        {
            get { return (String)GetValue(FilePathNameProperty); }
            set { SetValue(FilePathNameProperty, value); }
        }

        public static readonly DependencyProperty FilePathNameProperty =
            DependencyProperty.Register("FilePathName", typeof(string),
              typeof(ChooseFileUserControl), new PropertyMetadata(""));

        public String DialogString
        {
            get { return (String)GetValue(DialogStringProperty); }
            set { SetValue(DialogStringProperty, value); }
        }

        public static readonly DependencyProperty DialogStringProperty =
            DependencyProperty.Register("DialogString", typeof(string),
              typeof(ChooseFileUserControl), new PropertyMetadata(""));

        public String ChooseFile
        {
            get { return (String)GetValue(ChooseFileProperty); }
            set { SetValue(ChooseFileProperty, value); }
        }

        public static readonly DependencyProperty ChooseFileProperty =
            DependencyProperty.Register("ChooseFile", typeof(string),
              typeof(ChooseFileUserControl), new PropertyMetadata(""));

        private void PathChangedEvent(string newPath, string oldPath)
        {
            if (OnPathChanged == null)
                return;
            PathEventArgs args = new PathEventArgs(newPath, oldPath);
            OnPathChanged(this, args);
        }

        private void Button_Click_File(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog filDlg = new WinForms.OpenFileDialog();
            filDlg.Title = DialogString;
            filDlg.Filter = ChooseFile;

            if (FilePath != null && FilePath != "")
            {
                string FolderPath;
                try
                {
                    FolderPath = System.IO.Path.GetDirectoryName(FilePath);
                }
                catch (ArgumentException)
                {
                    FolderPath = "";
                }

                filDlg.InitialDirectory = FolderPath;
            }

            if (filDlg.ShowDialog() == WinForms.DialogResult.OK)
            {
                if (FilePath != filDlg.FileName)
                {
                    string old = FilePath;
                    FilePath = filDlg.FileName;
                    filePathTextBox.Text = FilePath;


                    PathChangedEvent(FilePath, old);
                }
            }
        }

        private void Button_Drop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null && files.Length > 0)
            {

                if (!File.Exists(files[0]))
                    return;

                string old = FilePath;
                FilePath = files[0];
                filePathTextBox.Text = FilePath;


                PathChangedEvent(FilePath, old);
            }

        }
    }
}
