﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DuPE.GUI.Tools
{
    /// <summary>
    /// Interaction logic for DateTimeRangePicker.xaml
    /// </summary>
    public partial class DateTimePicker : UserControl
    {
        public DateTimePicker()
        {
            InitializeComponent();
            DataContext = new DatePickerViewModel(this);
        }

        public static readonly DependencyProperty DefaultDateTimeProperty =
            DependencyProperty.Register("SelectedDateTime", typeof(DateTime),
              typeof(DateTimePicker), new PropertyMetadata(DateTime.Now));

        public DateTime SelectedDateTime
        {
            get { return (DateTime)GetValue(DefaultDateTimeProperty); }
            set
            {
                SetValue(DefaultDateTimeProperty, value);
            }
        }

        public static readonly DependencyProperty LabelDescriptionProperty =
            DependencyProperty.Register("LabelDescription", typeof(string),
              typeof(DateTimePicker), new PropertyMetadata("Date:"));

        public string LabelDescription
        {
            get { return (string)GetValue(LabelDescriptionProperty); }
            set
            {
                SetValue(LabelDescriptionProperty, value);
            }
        }

    }

    public class DatePickerViewModel : INotifyPropertyChanged
    {
        private readonly DateTimePicker dateTimePicker;

        public string date { get { return dateTimePicker.SelectedDateTime.ToString("d.M.yyyy"); } set { dateTimePicker.SelectedDateTime = Convert.ToDateTime(value); OnPropertyChanged("date"); } }
        public string description
        {
            get { return dateTimePicker.LabelDescription; }
            set { dateTimePicker.LabelDescription = value; OnPropertyChanged("description"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public DatePickerViewModel(DateTimePicker dateTimePicker)
        {
            this.dateTimePicker = dateTimePicker;
        }

    }
}
