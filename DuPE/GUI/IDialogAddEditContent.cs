﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.GUI
{
    public interface IDialogAddEditContent : IWindowContent
    {
        bool LeavingDialog(bool okDialog);
        bool CanExecuteSave();
    }
}
