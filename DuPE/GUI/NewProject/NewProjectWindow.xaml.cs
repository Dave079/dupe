﻿using DuPE.API.Databases;
using DuPE.GUI.DBConnectionCreaters;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DuPE.GUI.NewProject
{
    /// <summary>
    /// Interaction logic for NewProjectWindow.xaml
    /// </summary>
    public partial class NewProjectWindow : ModernWindow
    {
        public IGraphicConnectionCreator SourceConnectionCreater;
        public IGraphicConnectionCreator TargetConnectionCreater;

        public string ProjectName { get { return databaseTypeChooserUserControl.ViewModel.Name; } private set { } }

        private Dictionary<string, Tuple<UserControl, UserControl>> contentDictionary;

        private DatabaseTypeChooserUserControl databaseTypeChooserUserControl;
        private UserControl actualUserControl;
        private string actualDBType;
        private bool onlySameTargetAndSourceDatabase = true;

        public NewProjectWindow(Dictionary<string, Tuple<UserControl, UserControl>> contentDictionary)
        {
            this.contentDictionary = contentDictionary;
           
            actualDBType = contentDictionary.Keys.FirstOrDefault();
            InitializeComponent();

            databaseTypeChooserUserControl = new DatabaseTypeChooserUserControl(contentDictionary.Keys.ToList());
            actualUserControl = databaseTypeChooserUserControl;
            MainContent.Children.Add(databaseTypeChooserUserControl);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (actualUserControl == databaseTypeChooserUserControl && databaseTypeChooserUserControl.IsDataOk())
            {
                actualDBType = databaseTypeChooserUserControl.GetSelectedDBType();
                MainContent.Children.Clear();
                MainContent.Children.Add(contentDictionary[actualDBType].Item1);
                actualUserControl = contentDictionary[actualDBType].Item1;
            }
            else if (actualUserControl == contentDictionary[actualDBType].Item1)
            {
                var icc = (IGraphicConnectionCreator)actualUserControl;
                if (icc.IsDataOk())
                {
                    if (onlySameTargetAndSourceDatabase)
                    {
                        TargetConnectionCreater = icc;
                        SourceConnectionCreater = icc;
                        DialogResult = true;
                        MainContent.Children.Clear();
                        Close();
                    }
                    else
                    {
                        SourceConnectionCreater = icc;
                        MainContent.Children.Clear();
                        MainContent.Children.Add(contentDictionary[actualDBType].Item2);
                        actualUserControl = contentDictionary[actualDBType].Item2;
                    }
                }
            }
            else if (actualUserControl == contentDictionary[actualDBType].Item2)
            {
                var icc = (IGraphicConnectionCreator)actualUserControl;
                if (icc.IsDataOk())
                {
                    TargetConnectionCreater = icc;
                    DialogResult = true;
                    MainContent.Children.Clear();
                    Close();
                }
            }
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            if (actualUserControl == databaseTypeChooserUserControl)
            {

            }
            else if (actualUserControl == contentDictionary[actualDBType].Item1)
            {
                MainContent.Children.Clear();
                MainContent.Children.Add(databaseTypeChooserUserControl);
                actualUserControl = databaseTypeChooserUserControl;
            }
            else if (actualUserControl == contentDictionary[actualDBType].Item2)
            {
                MainContent.Children.Clear();
                MainContent.Children.Add(contentDictionary[actualDBType].Item1);
                actualUserControl = contentDictionary[actualDBType].Item1;
            }
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CanExecuteSave(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
    }
}
