﻿using DuPE.API.Databases;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace DuPE.GUI.NewProject
{
    /// <summary>
    /// Interaction logic for DatabaseTypeChooserUserControl.xaml
    /// </summary>
    public partial class DatabaseTypeChooserUserControl : UserControl
    {
        public NewProjectViewModel ViewModel;

        public DatabaseTypeChooserUserControl(List<string> contentDictionary)
        {
            ViewModel = new NewProjectViewModel();

            InitializeComponent();
            bool isFirst = true;
            foreach (var item in contentDictionary)
            {
                if (isFirst)
                {
                    MainStackPanel.Children.Add(new RadioButton() { Name = item.ToString(), Content = item, IsChecked = true });
                    isFirst = false;
                }
                else
                    MainStackPanel.Children.Add(new RadioButton() { Name = item.ToString(), Content = item });
            }

            DataContext = ViewModel;
        }

        public string GetSelectedDBType()
        {
            foreach (var item in MainStackPanel.Children)
            {
                if (item is RadioButton rb && rb.IsChecked == true)
                {
                    return rb.Name;
                }
            }

            throw new NotImplementedException();
        }

        public bool IsDataOk()
        {
            if (string.IsNullOrWhiteSpace(ViewModel.Name))
            {
                ErrorText.Text = NewProjectResource.ProjectNameNotEntered;
                ErrorBox.Visibility = System.Windows.Visibility.Visible;
                return false;
            }

            ErrorBox.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }

        public class NewProjectViewModel
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }
    }
}
