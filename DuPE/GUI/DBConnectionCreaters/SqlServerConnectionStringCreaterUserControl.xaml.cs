﻿using DuPE.Resources.Strings;
using DuPE.SqlServer;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.DBConnectionCreaters
{
    /// <summary>
    /// Interaction logic for SqlServerConnectionStringCreater.xaml
    /// </summary>
    public partial class SqlServerConnectionStringCreaterUserControl : UserControl, IGraphicConnectionCreator
    {
        private SqlServerNewProjectViewModel newProject;

        private bool isSource = true;

        public SqlServerConnectionStringCreaterUserControl(bool isForSource)
        {

            InitializeComponent();

            if (isForSource)
            {
                TittleTextBlock.Text = NewProjectResource.SetDB;
                SameAsSourceCheckBox.Visibility = Visibility.Hidden;
            }
            else
            {
                TittleTextBlock.Text = NewProjectResource.SetTargetDB;
            }

            newProject = new SqlServerNewProjectViewModel();
#if DEBUG
            TabConnectionString.IsSelected = true;

            newProject.Server = @"10.0.0.18";
            newProject.Catalog = "PRUDIC_TEST";
            newProject.UserName = @"dupe2";
            PasswordBox.Password = "abc";
            newProject.Schema = @"dbo";
            newProject.SchemaForConnectionString = @"dbo";
            newProject.UserName = @"dupe2";

            newProject.ConnectionString = @"Data Source=10.0.0.18,1433;Initial Catalog=PRUDIC_TEST;Integrated Security=False;User ID=dupe2;Password=abc;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
#endif
            DataContext = newProject;
        }

        public ConnectionData Get()
        {
            if (SameAsSourceCheckBox.IsChecked == true)
            {
                return null;
            }
            else if (TabConnectionString.IsSelected)
                return new ConnectionData(SqlServerConstants.DB_NAME, new Dictionary<string, object>() { { SqlServerConstants.CONNECTION_STRING, newProject.ConnectionString }, { SqlServerConstants.SCHEMA, newProject.SchemaForConnectionString } });
            else if (TabForm.IsSelected)
                return new ConnectionData(SqlServerConstants.DB_NAME, new Dictionary<string, object>() { { SqlServerConstants.CONNECTION_STRING, SqlServerConnectionStringCreater.Create(newProject.Server, newProject.Catalog, newProject.UserName, PasswordBox.Password) }, { SqlServerConstants.SCHEMA, newProject.Schema } });
            else
                throw new Exception();
        }

        public bool IsDataOk()
        {
            if (SameAsSourceCheckBox.IsChecked == true)
            {

            }
            else if (TabForm.IsSelected &&
                 (string.IsNullOrWhiteSpace(newProject.Server) ||
                  string.IsNullOrWhiteSpace(newProject.Catalog) ||
                  string.IsNullOrWhiteSpace(newProject.Schema) ||
                  string.IsNullOrWhiteSpace(newProject.UserName) ||
                  string.IsNullOrWhiteSpace(PasswordBox.Password)
                 ))
            {
                ErrorText.Text = NewProjectResource.PleaseFillAllFields;
                ErrorBox.Visibility = System.Windows.Visibility.Visible;
                return false;
            }
            else if (TabConnectionString.IsSelected &&
                (string.IsNullOrWhiteSpace(newProject.SchemaForConnectionString) ||
                string.IsNullOrWhiteSpace(newProject.ConnectionString)))
            {
                ErrorText.Text = NewProjectResource.PleaseFillAllFields;
                ErrorBox.Visibility = System.Windows.Visibility.Visible;
                return false;
            }

            ErrorText.Text = "";
            ErrorBox.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }

        public bool IsSourceDB()
        {
            return isSource;
        }

        private class SqlServerNewProjectViewModel
        {
            public string ConnectionString { get; set; }
            public string Server { get; set; }
            public string Catalog { get; set; }
            public string Schema { get; set; }
            public string SchemaForConnectionString { get; set; }

            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }
}
