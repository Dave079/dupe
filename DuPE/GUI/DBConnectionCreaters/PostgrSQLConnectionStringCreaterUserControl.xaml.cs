﻿using DuPE.PostgreSQL;
using DuPE.Resources.Strings;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.DBConnectionCreaters
{
    /// <summary>
    /// Interaction logic for SqlServerConnectionStringCreater.xaml
    /// </summary>
    public partial class PostgrSQLConnectionStringCreaterUserControl : UserControl, IGraphicConnectionCreator
    {
        private PostgreSQLNewProjectViewModel newProject;

        public PostgrSQLConnectionStringCreaterUserControl(bool isForSource)
        {

            InitializeComponent();

            if (isForSource)
            {
                TittleTextBlock.Text = NewProjectResource.SetDB;
                SameAsSourceCheckBox.Visibility = Visibility.Hidden;
            }
            else
            {
                TittleTextBlock.Text = NewProjectResource.SetTargetDB;
            }

            newProject = new PostgreSQLNewProjectViewModel();
#if DEBUG
            TabConnectionString.IsSelected = true;

            newProject.Server = "10.0.0.18";//5432
            newProject.Catalog = "postgres";
            newProject.Schema = "public";
            newProject.SchemaForConnectionString = "public";
            newProject.UserName = "postgres";
            PasswordBox.Password = "postgres";
            newProject.ConnectionString = @"Server = 10.0.0.18; Port = 5432; Database = postgres; User Id = postgres; Password = postgres; CommandTimeout = 0;";
#endif
            DataContext = newProject;

        }

        public ConnectionData Get()
        {
            if (SameAsSourceCheckBox.IsChecked == true)
            {
                return null;
            }
            else if (TabConnectionString.IsSelected)
                return new ConnectionData(PostgreSQLConstants.DB_NAME, new Dictionary<string, object>() { { PostgreSQLConstants.CONNECTION_STRING, newProject.ConnectionString }, { PostgreSQLConstants.SCHEMA, newProject.SchemaForConnectionString } });
            else if (TabForm.IsSelected)
                return new ConnectionData(PostgreSQLConstants.DB_NAME, new Dictionary<string, object>() { { PostgreSQLConstants.CONNECTION_STRING, PostgreSQLConnectionStringCreater.Create(newProject.Server, newProject.Catalog, newProject.UserName, PasswordBox.Password) }, { PostgreSQLConstants.SCHEMA, newProject.Schema } });
            else
                throw new Exception();
        }

        public bool IsDataOk()
        {
            if (SameAsSourceCheckBox.IsChecked == true)
            {

            }
            else if (TabForm.IsSelected &&
               (string.IsNullOrWhiteSpace(newProject.Server) ||
                string.IsNullOrWhiteSpace(newProject.Catalog) ||
                string.IsNullOrWhiteSpace(newProject.Schema) ||
                string.IsNullOrWhiteSpace(newProject.UserName) ||
                string.IsNullOrWhiteSpace(PasswordBox.Password)
               ))
            {
                ErrorText.Text = NewProjectResource.PleaseFillAllFields;
                ErrorBox.Visibility = System.Windows.Visibility.Visible;
                return false;
            }
            else if (TabConnectionString.IsSelected &&
                (string.IsNullOrWhiteSpace(newProject.SchemaForConnectionString) ||
                 string.IsNullOrWhiteSpace(newProject.ConnectionString)))
            {
                ErrorText.Text = NewProjectResource.PleaseFillAllFields;
                ErrorBox.Visibility = System.Windows.Visibility.Visible;
                return false;
            }

            ErrorText.Text = "";
            ErrorBox.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }

        private class PostgreSQLNewProjectViewModel
        {
            public string ConnectionString { get; set; }
            public string Server { get; set; }
            public string Catalog { get; set; }
            public string Schema { get; set; }
            public string SchemaForConnectionString { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }
}
