﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.GUI.DBConnectionCreaters
{
    public interface IGraphicConnectionCreator
    {
        bool IsDataOk();
        ConnectionData Get();
    }
}
