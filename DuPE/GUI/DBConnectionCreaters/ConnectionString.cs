﻿using DuPE.API.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.GUI.DBConnectionCreaters
{
    public class ConnectionData
    {
        public string Name { get; private set; }
        public Dictionary<string, object> Parameters { get; private set; }

        public ConnectionData(string name, Dictionary<string, object> parameters)
        {
            this.Name = name;
            Parameters= parameters;
        }
    }
}
