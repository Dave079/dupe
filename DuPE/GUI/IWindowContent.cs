﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DuPE.GUI
{
    public interface IWindowContent
    {
        string Title { get; }
        UIElement Content();
    }
}
