﻿using DuPE.Domain.Collections;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DuPE.GUI.Collections
{
    /// <summary>
    /// Interaction logic for CollectionsViewUserControl.xaml
    /// </summary>
    public partial class CollectionsViewUserControl : UserControl, IWindowContent
    {
        private ICollectionRepository _colRep;
        private ILog logger = LogManager.GetLogger("App");


        public CollectionsViewUserControl(ICollectionRepository ColRep)
        {
            this._colRep = ColRep;
            InitializeComponent();
            RefreshItems();
        }

        public string Title
        {
            get { return CollectionsResource.CollectionSummary; }
        }

        public new UIElement Content()
        {
            return this;
        }

        private void ImportContextMenu_Click(object sender, RoutedEventArgs e)
        {
            var usercontrol = new ImportCollectionUserControl();
            DialogWindow dialog = new DialogWindow(usercontrol);
            dialog.Owner = App.Current.MainWindow;
            if (dialog.ShowDialog() == true)
                RefreshItems();
        }

        private void DeleteContextMenu_Click(object sender, RoutedEventArgs e)
        {

            if (ItemsListView.SelectedItem == null)
            {
                MessageToUser.Show(MessageForUserResource.Info, CollectionsResource.ChooseItemForRemove);
                return;
            }

            try
            {
                if (MessageToUser.ShowYesNoQuestion(MessageForUserResource.Question, CollectionsResource.AreYouSureYouWantToRemoveTheCollection))
                    if (_colRep.Delete((Collection)ItemsListView.SelectedItem))
                        RefreshItems();
            }
            catch (Exception ee)
            {
                logger.Error(ee);
                MessageToUser.Show(MainResource.Error, ee.Message);
            }
        }

        private void RefreshItems()
        {
            ItemsListView.ItemsSource = _colRep.GetAll();
            ItemsListView.Items.Refresh();
        }
    }
}
