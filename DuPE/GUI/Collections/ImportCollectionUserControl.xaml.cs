﻿using DuPE.DAL.Collections.Parsers;
using DuPE.DAL.Collections.Repository;
using DuPE.DependencyInjection;
using DuPE.Domain.Collections;
using DuPE.Domain.Logging;
using DuPE.Domain.RulesForMasking;
using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.Collections
{
    /// <summary>
    /// Interaction logic for ImportCollectionUserControl.xaml
    /// </summary>
    public partial class ImportCollectionUserControl : UserControl, IDialogAddEditContent
    {
        private ILogger logger;
        private CollectionForImportViewModel viewModel;

        public ImportCollectionUserControl()
        {
            logger = NinjectService.Instance.Get<ILogger>();
            viewModel = new CollectionForImportViewModel();
            InitializeComponent();

            ImportFilePathPicker.OnPathChanged += ImportFilePathPicker_OnPathChanged;
            DataContext = viewModel;
        }

        void ImportFilePathPicker_OnPathChanged(object sender, Tools.PathEventArgs e)
        {
            viewModel.PathFileV = ImportFilePathPicker.FilePath;
        }

        public bool LeavingDialog(bool okDialog)
        {

            if (!okDialog)
                return false;
            try
            {
                CollectionsImporter collectionsImporter = NinjectService.Instance.Get<CollectionsImporter>();
                collectionsImporter.ImportCollectionFromFile(ImportFilePathPicker.FilePath, new CollectionForImport(viewModel.PathFileV, viewModel.RuleTypeV, viewModel.Name));
            }
            catch (Exception e)
            {
                logger.Error(e);
                MessageToUser.Show(MainResource.Error, e.Message);
                return false;
            }

            return true;
        }

        public bool CanExecuteSave()
        {
            return IsValid(this);
        }

        public string Title
        {
            get { return CollectionsResource.ImportTittle; }
        }

        private bool IsValid(DependencyObject obj)
        {

            if (string.IsNullOrWhiteSpace(viewModel.PathFileV))
                return false;

            return !Validation.GetHasError(obj) &&
                LogicalTreeHelper.GetChildren(obj)
                .OfType<DependencyObject>()
                .All(IsValid);
        }

        public new UIElement Content()
        {
            return this;
        }
    }

    public class CollectionForImportViewModel
    {
        public RuleType RuleTypeV { get; set; }
        public string PathFileV { get; set; }
        public string Name { get; set; }

        public CollectionForImportViewModel()
        {
            RuleTypeV = RuleType.String;
        }
    }
}
