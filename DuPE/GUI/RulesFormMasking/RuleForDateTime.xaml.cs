﻿using DuPE.DAL.Collections.Repository;
using DuPE.DependencyInjection;
using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.RulesForMasking;
using DuPE.Resources.Strings;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.RulesFormMasking
{
    /// <summary>
    /// Interaction logic for RuleFOrDateTime.xaml
    /// </summary>
    public partial class RuleForDateTime : UserControl, IDialogAddEditContent
    {
        private RuleForDatetimeViewModel ruleViewModel;
        private Column column;
        private ICollectionRepository CollectionRepository;

        public RuleForDateTime(Column column)
        {
            this.CollectionRepository = NinjectService.Instance.Get<ICollectionRepository>();
            this.column = column;
            ruleViewModel = new RuleForDatetimeViewModel(column);
            InitializeComponent();
            DataContext = ruleViewModel;
            CollectionCombobox.ItemsSource = CollectionRepository.GetAllWithoutDataByType(RuleType.DateTime);
        }

        public bool LeavingDialog(bool okDialog)
        {
            if (okDialog)
            {
                var rule = new RuleForDateTimeMasking(column);
                rule.NullProbability = double.Parse(ruleViewModel.NullProbability);
                column.RuleForMasking = rule;

                if (TabRandom.IsSelected)
                {
                    rule.From = DateTimePickeFrom.SelectedDateTime;
                    rule.To = DateTimePickeTo.SelectedDateTime;
                }
                else if (TabCollection.IsSelected)
                {
                    rule.collection = (Collection)CollectionCombobox.SelectedItem;
                    rule.selectFromCollectionByHash = ByHash.IsChecked == true;
                }

                return true;
            }

            return false;
        }

        public bool CanExecuteSave()
        {
            return IsValid(this);
        }

        private bool IsValid(DependencyObject obj)
        {
            return !Validation.GetHasError(obj) &&
                LogicalTreeHelper.GetChildren(obj)
                .OfType<DependencyObject>()
                .All(IsValid);
        }

        public string Title
        {
            get { return RulesResource.RuleForDateMasking; }
        }

        public new UIElement Content()
        {
            return this;
        }
    }

    public class RuleForDatetimeViewModel : RuleViewModel
    {
        public string From { get; set; }
        public string To { get; set; }

        public RuleForDatetimeViewModel(Column col)
            : base(col)
        {

        }
    }
}
