﻿using DuPE.DAL.Collections.Repository;
using DuPE.DependencyInjection;
using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using DuPE.Resources.Strings;
using System;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.RulesFormMasking
{
    /// <summary>
    /// Interaction logic for RuleForNumber.xaml
    /// </summary>
    public partial class RuleForNumber : UserControl, IDialogAddEditContent
    {
        private RuleForNumberViewModel ruleForNumberViewModel;
        private Column column;
        private NumberDataType type;
        private ICollectionRepository collectionRepository;

        public RuleForNumber(Column column)
        {
            collectionRepository = NinjectService.Instance.Get<ICollectionRepository>();
            this.column = column;
            InitializeComponent();

            ruleForNumberViewModel = new RuleForNumberViewModel(column);

            type = (NumberDataType)column.Type;
            if (type.NumberOfDecimalPLaces == 0)
            {
                ruleForNumberViewModel.NUmberOfDecimalPLaces = "0";
                NumberOfDecimalPlacesTextBox.IsEnabled = false;
            }

            DataContext = ruleForNumberViewModel;
            CollectionCombobox.ItemsSource = collectionRepository.GetAllWithoutDataByType(RuleType.Number);
        }

        public bool LeavingDialog(bool okDialog)
        {
            if (okDialog)
            {
                var rule = new RuleForNumberMasking(column);
                rule.NullProbability = double.Parse(ruleForNumberViewModel.NullProbability);
                column.RuleForMasking = rule;

                if (TabRandom.IsSelected)
                {
                    rule.Min = double.Parse(ruleForNumberViewModel.Min);
                    rule.Max = double.Parse(ruleForNumberViewModel.Max);
                    rule.NUmberOfDecimalPLaces = ushort.Parse(ruleForNumberViewModel.NUmberOfDecimalPLaces);
                }
                else if (TabCollection.IsSelected)
                {
                    rule.collection = (Collection)CollectionCombobox.SelectedItem;
                    rule.selectFromCollectionByHash = ByHash.IsChecked == true;
                }
                else if (TabHash.IsSelected)
                {
                    rule.HashTypeForMasking = ruleForNumberViewModel.hashType;
                }

                return true;
            }

            return false;
        }

        public bool CanExecuteSave()
        {
            double min, max;
            ushort decimalPlaces;
            double res2;

            if (TabRandom.IsSelected)
            {
                if (!double.TryParse(ruleForNumberViewModel.Min, out min) ||
                    !double.TryParse(ruleForNumberViewModel.Max, out max) ||
                    !double.TryParse(ruleForNumberViewModel.NullProbability, out res2) ||
                    !ushort.TryParse(ruleForNumberViewModel.NUmberOfDecimalPLaces, out decimalPlaces)
                )
                {
                    ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                    return false;
                }
                else
                {
                    if (type.Max < max)
                    {
                        ShowError(string.Format(MessageForUserResource.InsertedMaxValueIsBiggerThenDataType, type.Max, max));
                        return false;
                    }
                    else if (type.Min > min)
                    {
                        ShowError(string.Format(MessageForUserResource.InsertedMinValueIsSmallerThenDataType, type.Min, min));
                        return false;
                    }
                    else if (min>max)
                    {
                        ShowError(MessageForUserResource.MinIsBiggerThenMax);
                        return false;
                    }
                    else if (type.NumberOfDecimalPLaces < decimalPlaces)
                    {
                        ShowError(string.Format(MessageForUserResource.NumberOfDecimalPlacesIsBiggerThenDataTyp, type.NumberOfDecimalPLaces, decimalPlaces));
                        return false;
                    }
                }
            }
            else if (TabCollection.IsSelected &&
                (CollectionCombobox.SelectedItem == null))
            {
                ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                return false;
            }
            else if (TabHash.IsSelected && ruleForNumberViewModel.hashType == HashType.NA)
            {
                ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                return false;
            }

            ErrorBox.Visibility = Visibility.Hidden;
            return true;
        }

        private void ShowError(string message)
        {
            ErrorText.Text = message;
            ErrorBox.Visibility = Visibility.Visible;
        }

        public string Title
        {
            get { return RulesResource.RuleForNumberMasking; }
        }

        public new UIElement Content()
        {
            return this;
        }
    }

    public class RuleForNumberViewModel : RuleViewModel
    {
        public string Min { get; set; }
        public string Max { get; set; }
        public string NUmberOfDecimalPLaces { get; set; }

        public RuleForNumberViewModel(Column col)
            : base(col)
        {
            if (col.RuleForMasking != null)
            {
                var rule = col.RuleForMasking as RuleForNumberMasking;
                Max = rule.Max.ToString();
                Min = rule.Min.ToString();
                NUmberOfDecimalPLaces = rule.NUmberOfDecimalPLaces.ToString();
                NullProbability = rule.NullProbability.ToString();
            }
        }
    }
}
