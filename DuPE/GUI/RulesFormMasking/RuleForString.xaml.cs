﻿using DuPE.DAL.Collections.Repository;
using DuPE.DependencyInjection;
using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using DuPE.Resources.Strings;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.RulesFormMasking
{
    /// <summary>
    /// Interaction logic for RuleForString.xaml
    /// </summary>
    public partial class RuleForString : UserControl, IDialogAddEditContent
    {
        private RuleForStringViewModel ruleForStringViewModel;
        private Column column;
        private StringDataType type;
        private ICollectionRepository CollectionRepository;

        public RuleForString(Column column)
        {
            CollectionRepository = NinjectService.Instance.Get<ICollectionRepository>();
            this.column = column;
            type = (StringDataType)column.Type;
            ruleForStringViewModel = new RuleForStringViewModel(column);
            InitializeComponent();
            DataContext = ruleForStringViewModel;
            CollectionCombobox.ItemsSource = CollectionRepository.GetAllWithoutDataByType(RuleType.String);
        }

        public bool LeavingDialog(bool okDialog)
        {
            if (okDialog)
            {
                var rule = new RuleForStringMasking(column);
                rule.NullProbability = double.Parse(ruleForStringViewModel.NullProbability);
                column.RuleForMasking = rule;

                if (TabRandom.IsSelected)
                {
                    rule.MinLength = int.Parse(ruleForStringViewModel.MinLength);
                    rule.MaxLenght = int.Parse(ruleForStringViewModel.MaxLength);
                    rule.CharCollection = ruleForStringViewModel.CharCollection;
                }
                else if (TabCollection.IsSelected)
                {
                    rule.collection = (Collection)CollectionCombobox.SelectedItem;
                    rule.selectFromCollectionByHash = ByHash.IsChecked == true;
                }
                else if (TabHash.IsSelected)
                {
                    rule.MaxLenght = type.CharacterMaximumLength ?? 100;
                    if (rule.MaxLenght < 0)
                        rule.MaxLenght = 100;

                    rule.HashTypeForMasking = ruleForStringViewModel.hashType;
                }
                return true;
            }
            return false;
        }

        public bool CanExecuteSave()
        {
            uint min, max; ;
            double res2;

            if (TabRandom.IsSelected)
            {
                if (!uint.TryParse(ruleForStringViewModel.MinLength, out min) ||
                    !uint.TryParse(ruleForStringViewModel.MaxLength, out max) ||
                    !double.TryParse(ruleForStringViewModel.NullProbability, out res2) ||
                    string.IsNullOrWhiteSpace(ruleForStringViewModel.CharCollection))
                {
                    ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                    return false;
                }
                else
                {
                    if (type.CharacterMaximumLength != null && type.CharacterMaximumLength < max)
                    {
                        ShowError(string.Format(MessageForUserResource.InsertedMaxValueIsBiggerThenDataType, type.CharacterMaximumLength, max));
                        return false;
                    }

                    if (min > max)
                    {
                        ShowError(MessageForUserResource.MinIsBiggerThenMax);
                        return false;
                    }
                }
            }

            else if (TabCollection.IsSelected &&
                (CollectionCombobox.SelectedItem == null))
            {
                ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                return false;
            }
            else if (TabHash.IsSelected && ruleForStringViewModel.hashType == HashType.NA)
            {
                ShowError(RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain);
                return false;
            }

            ErrorBox.Visibility = Visibility.Hidden;
            return true;
        }

        private void ShowError(string message)
        {
            ErrorText.Text = message;
            ErrorBox.Visibility = Visibility.Visible;
        }

        public string Title
        {
            get { return RulesResource.RuleForStringMasking; }
        }

        public new UIElement Content()
        {
            return this;
        }
    }

    public class RuleForStringViewModel : RuleViewModel
    {
        public string MinLength { get; set; }
        public string MaxLength { get; set; }
        public string CharCollection { get; set; }

        public RuleForStringViewModel(Column col)
            : base(col)
        {
            if (col.RuleForMasking != null)
            {
                var rule = col.RuleForMasking as RuleForStringMasking;
                MaxLength = rule.MaxLenght.ToString();
                MinLength = rule.MinLength.ToString();
                CharCollection = rule.CharCollection;
                NullProbability = rule.NullProbability.ToString();
            }
        }
    }
}
