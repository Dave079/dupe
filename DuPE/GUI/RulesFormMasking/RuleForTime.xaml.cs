﻿using DuPE.DAL.Collections.Repository;
using DuPE.DependencyInjection;
using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.RulesForMasking;
using DuPE.Resources.Strings;
using System;
using System.Windows;
using System.Windows.Controls;

namespace DuPE.GUI.RulesFormMasking
{
    /// <summary>
    /// Interaction logic for RuleForTime.xaml
    /// </summary>
    public partial class RuleForTime : UserControl, IDialogAddEditContent
    {
        private RuleFortimeViewModel ruleViewModel;
        private Column column;
        private ICollectionRepository collectionRepository;

        public RuleForTime(Column column)
        {
            collectionRepository = NinjectService.Instance.Get<ICollectionRepository>(); 
            this.column = column;
            ruleViewModel = new RuleFortimeViewModel(column);
            InitializeComponent();
            DataContext = ruleViewModel;
            CollectionCombobox.ItemsSource = collectionRepository.GetAllWithoutDataByType(RuleType.Time);
        }

        public bool LeavingDialog(bool okDialog)
        {
            if (okDialog)
            {
                var rule = new RuleForTimeMasking(column);
                rule.NullProbability = double.Parse(ruleViewModel.NullProbability);
                column.RuleForMasking = rule;

                if (TabRandom.IsSelected)
                {
                    rule.From = TimeSpan.Parse(ruleViewModel.From);
                    rule.To = TimeSpan.Parse(ruleViewModel.To);
                }
                else if (TabCollection.IsSelected)
                {
                    rule.collection = (Collection)CollectionCombobox.SelectedItem;
                    rule.selectFromCollectionByHash = ByHash.IsChecked == true;
                }
               
                return true;
            }

            return false;
        }

        public bool CanExecuteSave()
        {
            TimeSpan res;
            double res2;

            if (TabRandom.IsSelected &&
                (
                !TimeSpan.TryParse(ruleViewModel.From, out res) ||
                !TimeSpan.TryParse(ruleViewModel.To, out res) ||
                !double.TryParse(ruleViewModel.NullProbability, out res2))
                )
            {
                ErrorText.Text = RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain;
                ErrorBox.Visibility = Visibility.Visible;
                return false;
            }
            else if (TabCollection.IsSelected &&
                (CollectionCombobox.SelectedItem == null))
            {
                ErrorText.Text = RulesResource.UnexpectedValuesHaveBeenEntered_PleaseTryAgain;
                ErrorBox.Visibility = Visibility.Visible;
                return false;
            }

            ErrorBox.Visibility = Visibility.Hidden;
            return true;
        }

        public string Title
        {
            get { return RulesResource.RuleForNumberMasking; }
        }

        public new UIElement Content()
        {
            return this;
        }

    }

    public class RuleFortimeViewModel : RuleViewModel
    {
        public string From { get; set; }
        public string To { get; set; }

        public RuleFortimeViewModel(Column col)
            : base(col)
        {

        }
    }

}
