﻿using DuPE.Domain.Columns;
using DuPE.Domain.RulesForMasking;

namespace DuPE.GUI.RulesFormMasking
{
    public class RuleViewModel
    {
        public string NullProbability { get; set; }
        public bool CanBeNull { get; set; }
        public HashType hashType { get; set; }

        public RuleViewModel(Column column)
        {
            hashType = HashType.NA;
            CanBeNull = column.IsNullable;
            NullProbability = "0";
        }
    }
}
