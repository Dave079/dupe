﻿using DuPE.GUI.Tools;
using DuPE.Resources.Strings;
using FirstFloor.ModernUI.Windows.Controls;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace DuPE
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : ModernWindow
    {
        public About()
        {
            InitializeComponent();
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            string ver = assembly.GetName().Version.ToString();
            AboutViewModel about = new AboutViewModel() { AppName = assembly.GetName().Name, AppVersion = assembly.GetName().Version.ToString() };// + " "+CustomerProfile.Name };
            DataContext = about;
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(@"http://" + (DataContext as AboutViewModel).CompanyWeb);
        }

        private void Hyperlink_Click_1(object sender, RoutedEventArgs e)
        {
            Process.Start(@"mailto:" + (DataContext as AboutViewModel).CompanyEmail);
        }

        private void HelpExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MessageToUser.Show(MessageForUserResource.Info, MessageForUserResource.HelpIsNotImplemented);
        }

        private void CloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
    }

    public class AboutViewModel
    {
        public string DeveloperName { get { return _developerName; } set { _developerName = value; } }
        private string _developerName;

        public string DeveloperEmail { get { return _developerEmail; } set { _developerEmail = value; } }
        private string _developerEmail;

        public string AppName { get { return _appName; } set { _appName = value; } }
        private string _appName;

        public string AppVersion { get { return _appVersion; } set { _appVersion = value; } }
        private string _appVersion;

        public string AppDescription { get { return _appDescription; } set { _appDescription = value; } }
        private string _appDescription;

        public string CompanyName { get { return _companyName; } set { _companyName = value; } }
        private string _companyName;

        public string CompanyWeb { get { return _companyWeb; } set { _companyWeb = value; } }
        private string _companyWeb;

        public string CompanyEmail { get { return _companyEmail; } set { _companyEmail = value; } }
        private string _companyEmail;

        public string Licence { get { return _Licence; } set { _Licence = value; } }
        private string _Licence;

        public AboutViewModel()
        {
            _companyWeb = "www.katedrainformatiky.cz";
            _companyName = "VŠB-TUO Ostrava";
            _developerEmail = "david.prudic@gmail.com";
            _developerName = "David Prudič";
            _companyEmail = "david.prudic@gmail.com";
            _Licence = MessageForUserResource.TheApplicationWasDevelopedInTheFrameworkOfTheDiplomaThesis;
        }
    }
}
