﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DuPE.Domain.RuleProcesors;
using DuPE.Domain.RulesForMasking;
using System.Collections.Generic;
using DuPE.Domain.Columns;

namespace DuPE.Tests
{
    [TestClass]
    public class RulesTests
    {
        [TestMethod]
        public void TestRuleForNumberProcesor()
        {
            ProcesorForNumberRule procesor = new ProcesorForNumberRule(new RuleForNumberMasking(new Column())
            {
                Min = -20,
                Max = 20,
                NUmberOfDecimalPLaces = 4,
                NullProbability = 0.1
            });

            var ListOfResults = new List<string>();

            for (int i = 0; i < 20; i++)
            {
                //ListOfResults.Add(procesor.GetNextValue());
            }

        }

        [TestMethod]
        public void TestRuleForStringProcesor()
        {
            ProcesorForStringRule procesor = new ProcesorForStringRule(new RuleForStringMasking(new Column()) { MinLength = 2, MaxLenght = 10, NullProbability = 0.1, CharCollection = ".-'asdfghjkl1234567890+ěščřžýáíé" });

            var ListOfResults = new List<string>();

            for (int i = 0; i < 20; i++)
            {
                //ListOfResults.Add(procesor.GetNextValue());
            }

        }

    }
}
