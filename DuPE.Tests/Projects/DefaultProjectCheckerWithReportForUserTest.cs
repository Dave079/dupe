﻿using DuPE.Domain.Logging;
using DuPE.Domain.Projects;
using DuPE.Domain.Reports;
using DuPE.Domain.Tables;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace DuPE.Tests.Projects
{
    [TestClass]
    public class DefaultProjectCheckerWithReportForUserTest
    {
        [TestMethod]
        public void ComlpeteCheckIsOK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(0, report1.Messages.Count);

            project.Tables[0].Columns.RemoveAt(0);
            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report2, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(0, report2.Messages.Count);
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKMissingColumnWithoutRule()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns.RemoveAt(0);

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report1.Messages.Count);
            Assert.IsTrue(report1.IsInWarnrMessage());
            Assert.IsFalse(report1.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKOtherDataTypeOnCOlumnWithoutRule()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[0].Type = new Domain.DataTypes.StringDataType("testttttype", 1);

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report1.Messages.Count);
            Assert.IsTrue(report1.IsInWarnrMessage());
            Assert.IsFalse(report1.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKMissingCOlumnWithRule()
        {
            var ddd = new Mock<IGeneralRepository>();
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());
            repoTables[0].Columns.RemoveAt(1);

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report2, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report2.Messages.Count);
            Assert.IsTrue(report2.IsInErrorMessage());
            Assert.IsFalse(report2.IsInWarnrMessage());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKColumnHaveOtherDataType()
        {
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[1].Type = new Domain.DataTypes.StringDataType("testtype", 1);

            var ddd = new Mock<IGeneralRepository>();
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report.Messages.Count);
            Assert.IsTrue(report.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKChangePK()
        {
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[2].IsPrimaryKey = false;

            var ddd = new Mock<IGeneralRepository>();
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report.Messages.Count);
            Assert.IsTrue(report.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKChangeFK()
        {
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[1].IsforeignKey = true;

            var ddd = new Mock<IGeneralRepository>();
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report, TypeOfControl.CompleteCheck, ddd.Object));
            Assert.AreEqual(1, report.Messages.Count);
            Assert.IsTrue(report.IsInErrorMessage());
        }

        [TestMethod]
        public void CompleteCheckWithouWarningCheckIsOK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(0, report1.Messages.Count);

            project.Tables[0].Columns.RemoveAt(0);
            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report2, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(0, report2.Messages.Count);
        }

        [TestMethod]
        public void ComlpeteCheckWithouWarningIsOKMissingCOlumnWithoutRule()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns.RemoveAt(0);

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(1, report1.Messages.Count);
            Assert.IsTrue(report1.IsInWarnrMessage());
            Assert.IsFalse(report1.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckWithouWarningIsOKOtherDataTypeOnCOlumnWithoutRule()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[0].Type = new Domain.DataTypes.StringDataType("testttttype", 1);

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsTrue(projectChecker.IsProjectReadyForMasking(project, out Report report1, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(1, report1.Messages.Count);
            Assert.IsTrue(report1.IsInWarnrMessage());
            Assert.IsFalse(report1.IsInErrorMessage());
        }

        [TestMethod]
        public void ComlpeteCheckWithouWarningIsntOKMissingCOlumnWithRule()
        {
            var ddd = new Mock<IGeneralRepository>();
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            repoTables[0].Columns.RemoveAt(1);

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report2, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(1, report2.Messages.Count);
            Assert.IsTrue(report2.IsInErrorMessage());
            Assert.IsFalse(report2.IsInWarnrMessage());
        }

        [TestMethod]
        public void ComlpeteCheckWithouWarningIsntOKColumnHaveOtherDataType()
        {
            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[1].Type = new Domain.DataTypes.StringDataType("testtype", 1);

            var ddd = new Mock<IGeneralRepository>();
            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            Domain.Projects.DefaultProjectCheckerWithReportForUser projectChecker = new Domain.Projects.DefaultProjectCheckerWithReportForUser(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectReadyForMasking(project, out Report report, TypeOfControl.CompleteCheckWithouWarning, ddd.Object));
            Assert.AreEqual(1, report.Messages.Count);
            Assert.IsTrue(report.IsInErrorMessage());
        }
    }
}