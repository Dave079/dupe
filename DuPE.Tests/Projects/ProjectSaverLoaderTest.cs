﻿using DuPE.DAL.Projects;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Projects;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Tables;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Tests.Projects
{
    [TestClass]
    public class ProjectSaverLoaderTest
    {
        [TestMethod]
        public void SaveAndLoadPoject()
        {
            Project project = new Project() { Tables = new List<Table>(), Name = "TestProject1", SourceConnetionString = "NA-SQL-S", TargetConnetionString = "NA-SQL-T", Path = "test.xml" , SourceSchema  = "schema1", TargetSchema = "schema2"};
            project.Tables.Add(TableCreater.GetTable1());

            ProjectSaverAndLoader psl = new ProjectSaverAndLoader();
            psl.Save(project);
            var loadedProject = psl.Load(project.Path);

            Assert.AreEqual(project.SourceConnetionString, loadedProject.SourceConnetionString);
            Assert.AreEqual(project.TargetConnetionString, loadedProject.TargetConnetionString);

            Assert.AreEqual(project.SourceSchema, loadedProject.SourceSchema);
            Assert.AreEqual(project.TargetSchema, loadedProject.TargetSchema);

            Assert.AreEqual(project.Database, loadedProject.Database);
            Assert.AreEqual(project.Name, loadedProject.Name);
            Assert.AreEqual(project.Path, loadedProject.Path);
            Assert.AreEqual(project.Tables.Count, loadedProject.Tables.Count);

            for (int i = 0; i < project.Tables.Count; i++)
            {
                var table = project.Tables[i];
                var loadedTable = loadedProject.Tables[i];

                Assert.AreEqual(table.Columns.Count, loadedTable.Columns.Count);
                Assert.AreEqual(table.HavePrimaryKey, loadedTable.HavePrimaryKey);
                Assert.AreEqual(table.TableCatalog, loadedTable.TableCatalog);
                Assert.AreEqual(table.TableName, loadedTable.TableName);
                Assert.AreEqual(table.TableSchema, loadedTable.TableSchema);
                Assert.AreEqual(table.TableType, loadedTable.TableType);

                for (int j = 0; j < table.Columns.Count; j++)
                {
                    var column = table.Columns[i];
                    var loadedColumn = loadedTable.Columns[i];

                    Assert.AreEqual(column.ColumnDefault, loadedColumn.ColumnDefault);
                    Assert.AreEqual(column.ColumnName, loadedColumn.ColumnName);
                    Assert.AreEqual(column.IsforeignKey, loadedColumn.IsforeignKey);
                    Assert.AreEqual(column.IsNullable, loadedColumn.IsNullable);
                    Assert.AreEqual(column.IsPrimaryKey, loadedColumn.IsPrimaryKey);
                    Assert.AreEqual(column.IsRuleSet, loadedColumn.IsRuleSet);
                    Assert.AreEqual(column.RuleForMasking, loadedColumn.RuleForMasking);
                    Assert.AreEqual(column.TableCatalog, loadedColumn.TableCatalog);
                    Assert.AreEqual(column.TableName, loadedColumn.TableName);
                    Assert.AreEqual(column.TableSchema, loadedColumn.TableSchema);
                    Assert.AreEqual(column.Type.OriginalName, loadedColumn.Type.OriginalName);
                    Assert.AreEqual(column.WasRuleUsed, loadedColumn.WasRuleUsed);
                }
            }

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void LoadWithException()
        {
            Project project = new Project() { Tables = new List<Table>(), Name = "TestProject1", SourceConnetionString = "NA-SQL-S", TargetConnetionString = "NA-SQL-T", Path = null };
            ProjectSaverAndLoader psl = new ProjectSaverAndLoader();
            var loadedProject = psl.Load(project.Path);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveWithException()
        {
            ProjectSaverAndLoader psl = new ProjectSaverAndLoader();
            psl.Save(null);
        }
    }
}
