﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Tests.Projects
{
    public class TableCreater
    {
        public static Table GetTable1(string tableName = "table1")
        {
            Column columnString = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnOne",
                ColumnDefault = "DefaultValue",
                IsforeignKey = true,
                IsNullable = true,
                Type = new StringDataType("TestStringType", 123),
            };

            //columnString.RuleForMasking = new RuleForStringMasking(columnString);

            //columnString.RuleForMasking = new RuleForStringMasking(columnString) { CharCollection = "1234567890", MaxLenght = 100, MinLength = 10, NullProbability = 0.8 };

            Column columnNumber = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnTwo",
                ColumnDefault = "12",
                IsforeignKey = false,
                IsPrimaryKey = true,
                IsNullable = true,
                Type = new NumberDataType("TestNumberType", 0, 65432, 0),
            };
            columnNumber.RuleForMasking = new RuleForNumberMasking(columnNumber) { Max = 9999, Min = 1, wasUsed = true, NullProbability = 0.8 };

            Column columnDateTime = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnThree",
                ColumnDefault = "11.1.2100",
                IsforeignKey = false,
                IsNullable = false,
                IsPrimaryKey = false,

                Type = new DateTimeDataType("TestDateTimeType", DateTime.Now, DateTime.Now.AddDays(23)),
            };
            columnDateTime.RuleForMasking = new RuleForDateTimeMasking(columnString) { From = DateTime.Now, To = DateTime.Now.AddDays(23), NullProbability = 0.8 };

            Column columnTime = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnTime",
                ColumnDefault = "11:11",
                IsforeignKey = false,
                IsNullable = false,
                IsPrimaryKey = false,

                Type = new TimeDataType("TestTimeType", new TimeSpan(0, 23, 0), new TimeSpan(1, 0, 0)),
            };
            columnTime.RuleForMasking = new RuleForTimeMasking(columnString) { From = new TimeSpan(0, 23, 0), To = new TimeSpan(1, 0, 0), NullProbability = 0.8 };


            Column columnUnsupported = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnFour",
                ColumnDefault = "NA",
                IsforeignKey = false,
                IsNullable = false,
                IsPrimaryKey = false,

                Type = new UnsupportedDataType("TestUnsupportedType"),
            };

            Column columnUnknown = new Column()
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnFive",
                ColumnDefault = "na",
                IsforeignKey = false,
                IsNullable = false,
                IsPrimaryKey = false,

                Type = new UnknownDataType("TestUnknownType"),
            };
            var Columns = new List<Column>();
            Columns.Add(columnString);
            Columns.Add(columnDateTime);
            Columns.Add(columnNumber);
            Columns.Add(columnTime);
            Columns.Add(columnUnsupported);
            Columns.Add(columnUnknown);

            Table table = new Table(Columns)
            {
                TableCatalog = "catalog1",
                TableName = tableName,
                TableSchema = "schema1",
                TableType = "DEFALUT_TEST_TYPE"
            };

            return table;
        }
    }
}
