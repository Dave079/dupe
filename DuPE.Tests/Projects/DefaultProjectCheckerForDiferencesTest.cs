﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Reports;
using DuPE.Domain.Tables;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Tests.Projects
{
    [TestClass]
    public class DefaultProjectCheckerForDiferencesTest
    {
        [TestMethod]
        public void ComlpeteCheckIsOK()
        {
            var ddd = new Mock<IGeneralRepository>();
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());

            Assert.IsTrue(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(0, diff1.Count);
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKMissingColumn()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());

            project.Tables[0].Columns.RemoveAt(0);
            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.Miss);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKSurplpusColumn()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });
            project.Tables[0].Columns.Add(new Column()
            {
                TableCatalog = "catalog1",
                TableName = project.Tables[0].Columns[0].TableName,
                TableSchema = "schema1",
                ColumnName = "TestColumnTwoSS",
                ColumnDefault = "12",
                IsforeignKey = false,
                IsPrimaryKey = true,
                IsNullable = true,
                Type = new NumberDataType("TestNumberType", 0, 65432, 0),
            });

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.Surplus);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKAnotherType()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            ddd.Setup(m => m.GetAll()).Returns(new List<Table>() { TableCreater.GetTable1() });
            project.Tables[0].Columns[1].Type = new Domain.DataTypes.StringDataType("testtype",1);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.AnotherType);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKOMisingTable()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1(), TableCreater.GetTable1("table2") };

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());

            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.Miss);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(TableDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKOSurplusTable()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1(), TableCreater.GetTable1("table2") } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.Surplus);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(TableDifference)).ToString());
        }


        [TestMethod]
        public void ComlpeteCheckIsntOKMovePK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[2].IsPrimaryKey = false;
            repoTables[0].Columns[1].IsPrimaryKey = true;


            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.MovePK);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKRemovePK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[2].IsPrimaryKey = false;
            repoTables[0].CheckPrimaryKey();

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.RemovePK);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKAddPK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            project.Tables[0].Columns[2].IsPrimaryKey = false;
            project.Tables[0].CheckPrimaryKey();

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.AddPK);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKRemoveFK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[0].IsforeignKey = false;
            repoTables[0].CheckPrimaryKey();

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.RemoveFK);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

        [TestMethod]
        public void ComlpeteCheckIsntOKAddFK()
        {
            var ddd = new Mock<IGeneralRepository>();

            var project = new Project() { Tables = new List<Table>() { TableCreater.GetTable1() } };
            var repoTables = new List<Table>() { TableCreater.GetTable1() };
            repoTables[0].Columns[3].IsforeignKey = true;
            repoTables[0].CheckPrimaryKey();

            ddd.Setup(m => m.GetAll()).Returns(repoTables);

            DefaultProjectCheckerForDiferences projectChecker = new DefaultProjectCheckerForDiferences(new EmptyLogger());


            Assert.IsFalse(projectChecker.IsProjectSameAsTargetDatabase(project, out IList<ProjectDifference> diff1, ddd.Object));
            Assert.AreEqual(1, diff1.Count);

            Assert.AreEqual(diff1[0].Type, DifferenceType.AddFK);
            Assert.AreEqual(diff1[0].GetType().ToString(), (typeof(ColumnDifference)).ToString());
        }

    }
}