﻿using DuPE.DAL.Collections.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DuPE.Tests.Collections
{
    [TestClass]
    public class CollectionTxtParserTest
    {
        private static string FileWithNumberPath = @"TestingData\Numbers.txt";
        private static string FileWithStringPath = @"TestingData\Strings.txt";
        private static string FileWithDateTimePath = @"TestingData\Dates.txt";
        private static string FileWithTimePath = @"TestingData\Times.txt";

        private static string FileWithNumberExceptionPath = @"TestingData\NumbersWithException.txt";
        private static string FileWithDateTimeExceptionPath = @"TestingData\DatesWithException.txt";

        private const string StringsCollectionName = "strings";
        private const string NumbersCollectionName = "numbers";
        private const string DatetimesCollectionName = "dates";
        private const string TimesCollectionName = "times";


        [ClassInitialize]
        public static void TestClassIni(TestContext value)
        {
            CheckFile(FileWithNumberPath);
            CheckFile(FileWithStringPath);
            CheckFile(FileWithDateTimePath);
            CheckFile(FileWithNumberExceptionPath);
            CheckFile(FileWithDateTimeExceptionPath);
        }

        private static void CheckFile(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("File for testing not found.", path);
        }

        [TestMethod]
        public void ParseWithoutException()
        {
            CollectionTxtFileParser collectionParser = new CollectionTxtFileParser();

            var stringColl = collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithStringPath, Domain.RulesForMasking.RuleType.String, StringsCollectionName));
            var numberColl = collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithNumberPath, Domain.RulesForMasking.RuleType.Number, NumbersCollectionName));
            var dateTimeColl = collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithDateTimePath, Domain.RulesForMasking.RuleType.DateTime, DatetimesCollectionName));
            var timeColl = collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithTimePath, Domain.RulesForMasking.RuleType.Time, TimesCollectionName));

            Assert.AreEqual(StringsCollectionName, stringColl.Name);
            CollectionAssert.AreEqual(new List<string> { "Arzumanová Karina", "Bauca i sastre Andreu", "Benatzká Karína, Ing.", "Benátzká Karina, Ing." }, stringColl.Data);

            Assert.AreEqual(NumbersCollectionName, numberColl.Name);
            CollectionAssert.AreEqual(new List<string> { "1", "2", "3", "4", "5" }, numberColl.Data);

            Assert.AreEqual(DatetimesCollectionName, dateTimeColl.Name);
            CollectionAssert.AreEqual(new List<string> { "01.01.2012 0:00:00", "01.02.2012 0:00:00", "01.03.2012 0:00:00", "01.04.2012 0:00:00" }, dateTimeColl.Data);

            Assert.AreEqual(TimesCollectionName, timeColl.Name);
            CollectionAssert.AreEqual(new List<string> { "10:00:00", "10:00:01", "01:01:01" }, timeColl.Data);

        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ParseNumbersWithException()
        {
            CollectionTxtFileParser collectionParser = new CollectionTxtFileParser();
            collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithNumberExceptionPath, Domain.RulesForMasking.RuleType.Number, NumbersCollectionName));
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ParseDateTimeWithException()
        {
            CollectionTxtFileParser collectionParser = new CollectionTxtFileParser();
            collectionParser.GetData(new Domain.Collections.CollectionForImport(FileWithDateTimeExceptionPath, Domain.RulesForMasking.RuleType.DateTime, DatetimesCollectionName));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParseWithArgumentException()
        {
            CollectionTxtFileParser collectionParser = new CollectionTxtFileParser();
            collectionParser.GetData(null);
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ParseWithFielNotFoundException()
        {
            CollectionTxtFileParser collectionParser = new CollectionTxtFileParser();
            collectionParser.GetData(new Domain.Collections.CollectionForImport("", Domain.RulesForMasking.RuleType.DateTime, DatetimesCollectionName));
        }
    }
}
