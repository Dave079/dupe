﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DuPE.DAL.Collections.Repository;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Collections;
using System.IO;
using System.Threading;

namespace DuPE.Tests.Collections
{
    [TestClass]
    public class CollectionRepositoryTest
    {
        private const string CorrectCollectionName = "Animals";
        private const string CorrectCollectionName2 = "Animals2";
        private const string CorrectCollectionName3 = "Animals3";

        private const string IncorrectCollectionName = "Animmals";
        private const string IncorectPathForData = @"TestingsssData\";
        private const string CorectPathForData = @"TestingData\";

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var filePath = CreateFilePath(CorrectCollectionName2);
            if (File.Exists(filePath))
                File.Delete(filePath);

            if (File.Exists(CreateFilePath(CorrectCollectionName3)))
                File.Delete(CreateFilePath(CorrectCollectionName3));
        }

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentException))]
        //public void GetWithExpcetion()
        //{
        //    CollectionRepository cr = new CollectionRepository(IncorectPathForData);
        //}

        [TestMethod]
        public void IsExistTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            Assert.IsTrue(cr.IsExists(new CollectionForImport("", RuleType.String, CorrectCollectionName)));
            Assert.IsFalse(cr.IsExists(new CollectionForImport("", RuleType.String, IncorrectCollectionName)));
        }

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public void IsExistExceptionTest()
        //{
        //    CollectionRepository cr = new CollectionRepository(CorectPathForData);
        //    Assert.IsTrue(cr.IsExists(null));
        //}

        [TestMethod]
        public void SaveCollectionTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            cr.Save(new Collection() { Name = CorrectCollectionName2, Type = RuleType.String });
            Assert.IsTrue(File.Exists(CreateFilePath(CorrectCollectionName2)));
            File.Delete(CreateFilePath(CorrectCollectionName2));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveCollectionExpcetionTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            cr.Save(null);
        }

        [TestMethod]
        public void GetAllWIthoutDataTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            var res = cr.GetAllWithoutData();
            Assert.AreEqual(3, res.Count);

            foreach (var collection in res)
            {
                Assert.AreEqual(0, collection.Data.Count);
            }
        }


        [TestMethod]
        public void GetAllWithoutDataByTypeTest()
        {
            CollectionRepository cr = new CollectionRepository();
            var res = cr.GetAllWithoutDataByType(RuleType.Number);

            Assert.AreEqual(1, res.Count);
            Assert.AreEqual(0, res[0].Data.Count);
        }

        [TestMethod]
        public void GetByTypeAndNameTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            var res = cr.Get(CorrectCollectionName, RuleType.String);

            Assert.AreNotEqual(null, res);
            Assert.AreEqual(CorrectCollectionName, res.Name);
            Assert.AreEqual(RuleType.String, res.Type);
            Assert.AreEqual(13, res.Data.Count);
        }

        [TestMethod]
        public void DeleteTest()
        {

            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            cr.Save(new Collection() { Name = CorrectCollectionName3, Type = RuleType.String });

            Assert.IsTrue(File.Exists(CreateFilePath(CorrectCollectionName3)));
            Assert.IsTrue(cr.Delete(new Collection() { Type = RuleType.String, Name = CorrectCollectionName3 }));
            Assert.IsFalse(File.Exists(CreateFilePath(CorrectCollectionName3)));

            File.Delete(CreateFilePath(CorrectCollectionName3));
        }

        [TestMethod]
        public void Delete2Test()
        {
            Assert.IsFalse(File.Exists(CreateFilePath(IncorrectCollectionName)));
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            Assert.IsFalse(cr.Delete(new Collection() { Type = RuleType.String, Name = IncorrectCollectionName }));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DeleteExceptionTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            cr.Delete(null);
        }

        [TestMethod]
        public void GetAllWIthDataTest()
        {
            CollectionRepository cr = new CollectionRepository(CorectPathForData);
            var res = cr.GetAll();
            Assert.AreEqual(3, res.Count);

            foreach (var collection in res)
            {
                Assert.AreNotEqual(0, collection.Data.Count);
            }
        }


        private static string CreateFilePath(string name)
        {
            return Path.Combine(CorectPathForData, string.Format("{0}_{1}.{2}", RuleType.String.ToString(), name, "dupeColl"));
        }
    }
}
