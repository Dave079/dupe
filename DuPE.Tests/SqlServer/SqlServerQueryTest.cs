﻿using DuPE.Domain.Logging;
using DuPE.SqlServer.Database;
using DuPE.SqlServer.Database.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Tests.SqlServer
{
    [TestClass]
    public class SqlServerQueryTest
    {
        //[TestMethod]
        public void GetTables()
        {
            string ConnectionString = "Data Source=10.0.0.18,1433;Initial Catalog=PRUDIC_TEST;Integrated Security=False;User ID=dupe2;Password=abc;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
            SessionFactoryBuilder sfb = new SessionFactoryBuilder(new EmptyLogger(), ConnectionString);
            TableRepository tableRep = new TableRepository(sfb);
            var tables = tableRep.GetAll();
        }
    }
}
