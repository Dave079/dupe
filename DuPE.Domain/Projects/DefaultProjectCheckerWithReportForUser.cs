﻿using System;
using DuPE.Domain.Logging;
using DuPE.Domain.Reports;
using DuPE.Domain.Tables;
using System.Linq;
using DuPE.Domain.Columns;
using System.Collections.Generic;
using DuPE.Domain.Resources;

namespace DuPE.Domain.Projects
{
    public class DefaultProjectCheckerWithReportForUser : IProjectCheckerForMasking
    {
        private ILogger logger;

        public DefaultProjectCheckerWithReportForUser(ILogger logger)
        {
            this.logger = logger;
        }

        public bool IsProjectReadyForMasking(Project project, out Report report, TypeOfControl type, IGeneralRepository repository)
        {
            report = new Report(logger);
            var tables = repository.GetAll();

            CheckTables(project, report, tables);

            switch (type)
            {
                case TypeOfControl.CompleteCheck:
                    return !(report.IsInErrorMessage() || report.IsInWarnrMessage());
                case TypeOfControl.CompleteCheckWithouWarning:
                    return !report.IsInErrorMessage();
                default:
                    throw new NotImplementedException();
            }
        }

        private void CheckTables(Project project, Report report, IList<Table> tablesFromDBList)
        {
            var tablesFromDB = tablesFromDBList.ToDictionary(x => GetKey(x));
            foreach (var table in project.Tables)
            {
                CheckTable(report, tablesFromDB, table);
            }
        }

        private void CheckTable(Report report, System.Collections.Generic.Dictionary<string, Table> tablesFromDB, Table table)
        {
            if (tablesFromDB.TryGetValue(GetKey(table), out Table dbTabe))
            {
                CheckColumns(report, table, dbTabe);
            }
            else
            {
                if (HaveColumnWithRule(table))
                {
                    report.AddErrorMessage(StringsResource.TableMisingInTargerDatabase, string.Format(StringsResource.TableMisingInTargerDatabaseDetail, GetKey(table)));
                }
                else
                {
                    report.AddWarnMessage(StringsResource.TableMisingInTargerDatabaseButTableDoesntHaveRulesOnColumns, string.Format(StringsResource.TableMisingInTargerDatabaseButTableDoesntHaveRulesOnColumnsDetail, GetKey(table)));
                }
            }
        }

        private void CheckColumns(Report report, Table table, Table dbTabe)
        {
            var columnsFromDB = dbTabe.Columns.ToDictionary(x => GetKey(x));
            var withRule = table.Columns.Where(x => x.RuleForMasking != null).ToList().Count > 0;
            foreach (var column in table.Columns)
            {
                CheckColumn(report, columnsFromDB, column, withRule);
            }
        }

        private void CheckColumn(Report report, System.Collections.Generic.Dictionary<string, Column> columnsFromDB, Column column, bool withRule)
        {
            if (columnsFromDB.TryGetValue(GetKey(column), out Column dbColumn))
            {
                if (column.IsPrimaryKey != dbColumn.IsPrimaryKey)
                {
                    if (withRule)
                    {
                        if (column.IsPrimaryKey)
                        {
                            report.AddErrorMessage(StringsResource.ColumnHasPrimaryKeyUnlikeTarget, string.Format(StringsResource.ColumnHasPrimaryKeyUnlikeTargetDetail, GetKey(column)));
                        }
                        else
                            report.AddErrorMessage(StringsResource.ColumnDoesNotHasPrimaryKeyUnlikeTheTarget, string.Format(StringsResource.ColumnDoesNotHasPrimaryKeyUnlikeTheTargetDetail, GetKey(column)));
                    }
                    else
                    {
                        if (column.IsPrimaryKey)
                        {
                            report.AddWarnMessage(StringsResource.ColumnHasPrimaryKeyUnlikeTheTargetButTableDoesntHaveRule, string.Format(StringsResource.ColumnHasPrimaryKeyUnlikeTheTargetButTableDoesntHaveRuleDetail, GetKey(column)));
                        }
                        else
                        {
                            report.AddWarnMessage(StringsResource.ColumnDoesNotHasPrimaryKeyUnlikeTargetButTableDoesntHaveRule, string.Format(StringsResource.ColumnDoesNotHasPrimaryKeyUnlikeTargetButTableDoesntHaveRule, GetKey(column)));
                        }
                    }
                }
                else if (column.IsforeignKey != dbColumn.IsforeignKey)
                {
                    if (withRule)
                    {
                        if (column.IsforeignKey)
                        {
                            report.AddWarnMessage(StringsResource.ColumnHasForeignKeyUnlikeTheTargetButColumnDoesntHaveRule, string.Format(StringsResource.ColumnHasForeignKeyUnlikeTheTargetButColumnDoesntHaveRuleDetail, GetKey(column)));
                        }
                        else
                        {
                            report.AddErrorMessage(StringsResource.ColumnDoesNotHasForeignKeyUnlikeTargetButColumnHaveRule, string.Format(StringsResource.ColumnDoesNotHasForeignKeyUnlikeTargetButColumnHaveRuleDetail, GetKey(column)));
                        }
                    }
                    else
                    {
                        if (column.IsforeignKey)
                        {
                            report.AddWarnMessage(StringsResource.ColumnHasForeignKeyUnlikeTargetButDoesntHaveRule, string.Format(StringsResource.ColumnHasForeignKeyUnlikeTargetButDoesntHaveRuleDetail, GetKey(column)));
                        }
                        else
                        {
                            report.AddWarnMessage(StringsResource.ColumnDoesNotHasForeignKeyUnlikeTargetButColumnDoesntHaveRule, string.Format(StringsResource.ColumnDoesNotHasForeignKeyUnlikeTargetButColumnDoesntHaveRuleDetail, GetKey(column)));
                        }
                    }
                }
                else if (column.Type.GetType().ToString() != dbColumn.Type.GetType().ToString())
                {
                    if (column.RuleForMasking != null)
                    {
                        report.AddErrorMessage(StringsResource.ColumnHaveDifirentDataType, string.Format(StringsResource.ColumnHaveDifirentDataTypeDetail, GetKey(column), column.Type.GetType().ToString(), dbColumn.Type.GetType().ToString()));
                    }
                    else
                    {
                        report.AddWarnMessage(StringsResource.ColumnHaveDifirentDataTypeButDoesntHaveRule, string.Format(StringsResource.ColumnHaveDifirentDataTypeButDoesntHaveRuleDetail, GetKey(column), column.Type.GetType().ToString(), dbColumn.Type.GetType().ToString()));
                    }
                }
                else
                {
                    if (column.Type.IsOkForMasking(dbColumn.Type) && column.IsNullable == dbColumn.IsNullable && column.ColumnDefault == dbColumn.ColumnDefault)
                    {
                        if (logger.IsDebugEnabled)
                            report.AddDebugMessage(StringsResource.ColumnIsOK, string.Format(StringsResource.ColumnIsOKDetail, GetKey(column)));
                    }
                    else
                    {
                        if (column.RuleForMasking != null)
                        {
                            report.AddErrorMessage(StringsResource.ColumnHaveDifirentDataTypeSettings, string.Format(StringsResource.ColumnHaveDifirentDataTypeSettingsDetail, GetKey(column), column.Type.GetType().ToString(), dbColumn.Type.GetType().ToString()));
                        }
                        else
                        {
                            report.AddWarnMessage(StringsResource.ColumnHaveDifirentDataTypeSettingsButDoesntHaveRule, string.Format(StringsResource.ColumnHaveDifirentDataTypeSettingsButDoesntHaveRuleDetail, GetKey(column), column.Type.GetType().ToString(), dbColumn.Type.GetType().ToString()));
                        }
                    }
                }
            }
            else
            {
                if (column.RuleForMasking != null)
                {
                    report.AddErrorMessage(StringsResource.ColumnMisingInTargerDatabase, string.Format(StringsResource.ColumnMisingInTargerDatabaseDetail, GetKey(column)));
                }
                else
                {
                    report.AddWarnMessage(StringsResource.ColumnMisingInTargerDatabaseButDoesntHaveRule, string.Format(StringsResource.ColumnMisingInTargerDatabaseButDoesntHaveRuleDetail, GetKey(column)));
                }
            }
        }

        private bool HaveColumnWithRule(Table table)
        {
            return table.Columns.FirstOrDefault(x => x.RuleForMasking != null) != null;
        }

        private string GetKey(Table table)
        {
            return string.Format("{0}.{1}.{2} ({3})", table.TableCatalog, table.TableSchema, table.TableName, table.TableType);
        }

        private string GetKey(Column column)
        {
            return string.Format("{0}.{1}.{2}.{3} ({4})", column.TableCatalog, column.TableSchema, column.TableName, column.ColumnName, column.Type.OriginalName);
        }

    }
}
