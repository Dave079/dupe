﻿using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Projects
{
    public class Project
    {
        public string Name { get; set; }
        public string Database { get; set; }
        public string Path { get; set; }
        public string SourceConnetionString { get; set; }
        public string TargetConnetionString { get; set; }
        public string SourceSchema { get; set; }
        public string TargetSchema { get; set; }
        public IList<Table> Tables { get; set; }
        public DateTime? LastSave { get; set; }
        public Project()
        {

        }
    }
}
