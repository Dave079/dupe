﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuPE.Domain.Columns;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Tables;

namespace DuPE.Domain.Projects
{
    public class DefaultProjectUpdater : IProjectUpdater
    {
        public void Update(IEnumerable<ProjectDifference> differences, Project actualProject)
        {
            foreach (var diff in differences)
            {
                if (diff is TableDifference tableDiff)
                {
                    UpdateTable(tableDiff, actualProject);
                }
                else if (diff is ColumnDifference columnDiff)
                {
                    UpdateColumn(columnDiff, actualProject);
                }
            }
        }

        private void UpdateColumn(ColumnDifference columnDiff, Project actualProject)
        {
            Column column = null;
            Table table = null;
            switch (columnDiff.Type)
            {
                case DifferenceType.Surplus:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    table.Columns.Remove(column);
                    break;
                case DifferenceType.Miss:
                    table = FindTable(columnDiff.Column, actualProject);
                    table.Columns.Add(columnDiff.Column);
                    break;
                case DifferenceType.AnotherType:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    table.Columns.Remove(column);
                    table.Columns.Add(columnDiff.Column);
                    break;
                case DifferenceType.AddFK:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    column.IsforeignKey = true;
                    column.RuleForMasking = null;
                    break;
                case DifferenceType.RemoveFK:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    column.IsforeignKey = false;
                    break;
                case DifferenceType.AddPK:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    column.IsPrimaryKey = true;
                    table.CheckPrimaryKey();
                    break;
                case DifferenceType.RemovePK:
                    table = FindTable(columnDiff.Column, actualProject);
                    column = FindColumn(table, columnDiff.Column);
                    column.IsPrimaryKey = false;
                    foreach (var col in table.Columns)
                        col.RuleForMasking = null;
                    table.CheckPrimaryKey();
                    break;
                case DifferenceType.MovePK:
                    table = FindTable(columnDiff.Column, actualProject);
                    foreach (var col in table.Columns)
                        col.IsPrimaryKey = false;
                    column = FindColumn(table, columnDiff.Column);
                    column.IsPrimaryKey = true;
                    column.RuleForMasking = null;
                    break;

                default:
                    throw new Exception();
            }
        }

        private Column FindColumn(Table table, Column column)
        {
            var findedColumn = table.Columns.FirstOrDefault(x => x.ColumnName == column.ColumnName);
            if (table == null)
                throw new Exception();
            return findedColumn;
        }

        private Table FindTable(Column column, Project actualProject)
        {
            var table = actualProject.Tables.FirstOrDefault(x => x.TableName == column.TableName && x.TableCatalog == column.TableCatalog && x.TableSchema == column.TableSchema);
            if (table == null)
                throw new Exception();
            return table;
        }
        private void UpdateTable(TableDifference tableDiff, Project actualProject)
        {
            Table table = null;

            switch (tableDiff.Type)
            {
                case DifferenceType.Surplus:
                    table = FindTable(tableDiff.Table, actualProject);
                    actualProject.Tables.Remove(table);
                    break;
                case DifferenceType.Miss:
                    actualProject.Tables.Add(tableDiff.Table);
                    break;
                case DifferenceType.AnotherType:
                default:
                    throw new Exception();
            }
        }

        private Table FindTable(Table table, Project actualProject)
        {
            return actualProject.Tables.FirstOrDefault(x => x.TableName == table.TableName && x.TableCatalog == table.TableCatalog && x.TableSchema == table.TableSchema);
        }
    }
}
