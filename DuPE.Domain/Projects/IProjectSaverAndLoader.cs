﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.Domain.Projects
{
    public interface IProjectSaverAndLoader
    {
        void Save(Project objectToSave);
        Project Load(string path);
    }
}
