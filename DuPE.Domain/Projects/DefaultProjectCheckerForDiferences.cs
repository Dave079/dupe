﻿using DuPE.Domain.Columns;
using DuPE.Domain.Logging;
using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DuPE.Domain.Projects
{
    public class DefaultProjectCheckerForDiferences : IProjectCheckerForUpdate
    {
        private ILogger logger;

        public DefaultProjectCheckerForDiferences(ILogger Logger)
        {
            this.logger = Logger;
        }

        public bool IsProjectSameAsTargetDatabase(Project project, out IList<ProjectDifference> differences, IGeneralRepository repository)
        {
            differences = new List<ProjectDifference>();
            var tables = repository.GetAll();

            CheckTables(project, tables, differences);

            return differences.Count == 0;
        }

        private void CheckTables(Project project, IList<Table> tablesFromDB, IList<ProjectDifference> diff)
        {
            var containsList = tablesFromDB.Intersect(project.Tables).ToList();
            var surpluslist = project.Tables.Except(tablesFromDB).ToList();
            var missList = tablesFromDB.Except(project.Tables).ToList();

            foreach (var surplusTable in surpluslist)
                diff.Add(new TableDifference() { Table = surplusTable, Type = DifferenceType.Surplus });

            foreach (var missTable in missList)
                diff.Add(new TableDifference() { Table = missTable, Type = DifferenceType.Miss });

            var dictContainsTables = containsList.ToDictionary(x => GetKey(x));

            foreach (var projectTable in project.Tables)
            {
                if (dictContainsTables.ContainsKey(GetKey(projectTable)))
                    CheckTable(diff, dictContainsTables[GetKey(projectTable)], projectTable);
            }
        }

        private void CheckTable(IList<ProjectDifference> diff, Table tableFromDB, Table tableFromProject)
        {
            var containsList = tableFromDB.Columns.Intersect(tableFromProject.Columns).ToList();
            var surpluslist = tableFromProject.Columns.Except(tableFromDB.Columns).ToList();
            var missList = tableFromDB.Columns.Except(tableFromProject.Columns).ToList();

            foreach (var surplusTable in surpluslist)
                diff.Add(new ColumnDifference() { Column = surplusTable, Type = DifferenceType.Surplus });

            foreach (var missTable in missList)
                diff.Add(new ColumnDifference() { Column = missTable, Type = DifferenceType.Miss });

            var dictContainsColumns = containsList.ToDictionary(x => GetKey(x));


            foreach (var projectColumn in tableFromProject.Columns)
            {
                if (dictContainsColumns.ContainsKey(GetKey(projectColumn)))
                    CheckColumn(diff, dictContainsColumns[GetKey(projectColumn)], projectColumn);
            }

            CheckPrimaryKey(diff, tableFromDB, tableFromProject);
        }

        private void CheckPrimaryKey(IList<ProjectDifference> diff, Table tableFromDB, Table tableFromProject)
        {
            if (tableFromProject.HavePrimaryKey != tableFromDB.HavePrimaryKey)
            {
                if (tableFromProject.HavePrimaryKey)
                {
                    var columnWithKey = tableFromProject.Columns.Where(x => x.IsPrimaryKey).FirstOrDefault();
                    diff.Add(new ColumnDifference() { Column = columnWithKey, Type = DifferenceType.RemovePK });
                }
                else
                {
                    var columnWithKey = tableFromDB.Columns.Where(x => x.IsPrimaryKey).FirstOrDefault();
                    diff.Add(new ColumnDifference() { Column = columnWithKey, Type = DifferenceType.AddPK });
                }
            }
            else
            {
                if (tableFromProject.HavePrimaryKey)
                {
                    var columnWithKeyFromProject = tableFromProject.Columns.Where(x => x.IsPrimaryKey).FirstOrDefault();
                    var columnWithKeyFromDB = tableFromDB.Columns.Where(x => x.IsPrimaryKey).FirstOrDefault();

                    if (GetKey(columnWithKeyFromDB) != GetKey(columnWithKeyFromProject))
                    {
                        diff.Add(new ColumnDifference() { Column = columnWithKeyFromDB, Type = DifferenceType.MovePK });
                    }
                }
            }
        }

        private void CheckColumn(IList<ProjectDifference> diff, Column columnFromDB, Column projectColumn)
        {
            if (!IsSameTypeAndOther(projectColumn, columnFromDB))
                diff.Add(new ColumnDifference() { Column = columnFromDB, Type = DifferenceType.AnotherType });

            if (projectColumn.IsforeignKey != columnFromDB.IsforeignKey)
            {
                if (projectColumn.IsforeignKey)
                {
                    diff.Add(new ColumnDifference() { Column = columnFromDB, Type = DifferenceType.RemoveFK });
                }
                else
                {
                    diff.Add(new ColumnDifference() { Column = columnFromDB, Type = DifferenceType.AddFK });
                }
            }
        }

        private bool IsSameTypeAndOther(Column projectColumn, Column columnFromDB)
        {
            if (projectColumn.IsNullable != columnFromDB.IsNullable)
                return false;

            if (projectColumn.ColumnDefault != columnFromDB.ColumnDefault)
                return false;

            if (projectColumn.Type.GetType().ToString() != columnFromDB.Type.GetType().ToString())
                return false;

            if (projectColumn.Type.IsSame(columnFromDB.Type))
                return true;

            return false;
        }

        private string GetKey(Table table)
        {
            return string.Format("{0}.{1}.{2}({3})", table.TableCatalog, table.TableSchema, table.TableName, table.TableType);
        }

        private string GetKey(Column column)
        {
            return string.Format("{0}.{1}.{2}.{3}", column.TableCatalog, column.TableSchema, column.TableName, column.ColumnName);
        }
    }
}
