﻿using DuPE.Domain.Projects.PojectDifferences;
using DuPE.Domain.Reports;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Projects
{
    public interface IProjectCheckerForMasking
    {
        bool IsProjectReadyForMasking(Project project, out Report report, TypeOfControl type, IGeneralRepository repository);
    }

    public interface IProjectCheckerForUpdate
    {
        bool IsProjectSameAsTargetDatabase(Project project, out IList<ProjectDifference> differences, IGeneralRepository repository);
    }
}
