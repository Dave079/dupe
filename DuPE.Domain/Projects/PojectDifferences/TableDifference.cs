﻿using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Projects.PojectDifferences
{
    public class TableDifference : ProjectDifference
    {
        public Table Table { get; set; }
    }
}
