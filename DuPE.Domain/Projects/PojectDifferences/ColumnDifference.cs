﻿using DuPE.Domain.Columns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Projects.PojectDifferences
{
    public class ColumnDifference : ProjectDifference
    {
        public Column Column { get; set; }
    }
}
