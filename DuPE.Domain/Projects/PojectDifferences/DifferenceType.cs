﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Projects.PojectDifferences
{
    public enum DifferenceType
    {
        Surplus,
        Miss,
        AnotherType,
        RemovePK,
        AddPK,
        MovePK,
        RemoveFK,
        AddFK,
    }
}
