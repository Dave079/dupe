﻿using DuPE.Domain.Projects.PojectDifferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Projects
{
    public interface IProjectUpdater
    {
        void Update(IEnumerable<ProjectDifference> differences, Project actualProject);
    }
}
