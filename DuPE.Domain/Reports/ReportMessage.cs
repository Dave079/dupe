﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Reports
{
    public class ReportMessage
    {
        public string Tittle { get; private set; }
        public string Description { get; private set; }
        public ReportMessageType Type { get; private set; }

        public ReportMessage(string tittle, string description, ReportMessageType type)
        {
            Tittle = tittle;
            Description = description;
            Type = type;
        }

        public override string ToString()
        {
            return string.Format("[{0}]-{1} ({2})", Type, Tittle, Description);
        }
    }
}
