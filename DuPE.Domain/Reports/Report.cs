﻿using DuPE.Domain.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Reports
{
    public class Report
    {
        public IList<ReportMessage> Messages { get; private set; }
        private ILogger Logger;

        public Report(IList<ReportMessage> Messages, ILogger Logger = null)
        {
            this.Messages = Messages;
            this.Logger = Logger;
        }

        public Report(ILogger Logger = null)
        {
            this.Logger = Logger;
            this.Messages = new List<ReportMessage>();
        }

        public void Clear()
        {
            Messages.Clear();
        }
        public void AddDebugMessage(string tittle, string message)
        {
            AddMessage(tittle, message, ReportMessageType.Debug);
        }

        public void AddInfoMessage(string tittle, string message)
        {
            AddMessage(tittle, message, ReportMessageType.Info);
        }

        public void AddWarnMessage(string tittle, string message)
        {
            AddMessage(tittle, message, ReportMessageType.Warn);
        }

        public void AddErrorMessage(string tittle, string message)
        {
            AddMessage(tittle, message, ReportMessageType.Error);
        }
        public void AddMessage(string tittle, string message, ReportMessageType type)
        {
            Messages?.Add(new ReportMessage(tittle, message, type));

            switch (type)
            {
                case ReportMessageType.Debug:
                    Logger?.Debug(message);
                    break;
                case ReportMessageType.Info:
                    Logger?.Info(message);
                    break;
                case ReportMessageType.Warn:
                    Logger?.Warn(message);
                    break;
                case ReportMessageType.Error:
                    Logger?.Error(message);
                    break;
                default:
                    new NotImplementedException();
                    break;
            }
        }

        public bool IsInErrorMessage()
        {
            return Messages.FirstOrDefault(x => x.Type == ReportMessageType.Error) != null;
        }

        public bool IsInWarnrMessage()
        {
            return Messages.FirstOrDefault(x => x.Type == ReportMessageType.Warn) != null;
        }
    }
}
