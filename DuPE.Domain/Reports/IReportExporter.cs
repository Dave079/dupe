﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Reports
{
    public interface IReportExporter
    {
        void SaveToFile(IList<ReportMessage> reports, string filePath);
        string GetFileFilterForSuffix();

    }
}
