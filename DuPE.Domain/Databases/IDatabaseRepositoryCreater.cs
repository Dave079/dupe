﻿using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Databases
{
    public interface IDatabaseRepositoryCreater
    {
        string Name { get; }
        IGeneralRepository GetGeneralRepository(Dictionary<string, object> arguments);
        IGeneralRepository GetGeneralRepository(string connectionString, string schema);
    }
}
