﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;

namespace DuPE.Domain.Columns
{
    public class Column
    {
        public string TableCatalog { get; set; }
        public string TableSchema { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ColumnDefault { get; set; }
        public bool IsNullable { get; set; }
        public DataType Type { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsforeignKey { get; set; }

        public IRuleForMasking RuleForMasking { get; set; }

        public bool IsRuleSet { get { return RuleForMasking != null; } }
        public bool WasRuleUsed { get { return RuleForMasking != null ? RuleForMasking.WasUsed() : false; } }

        public override bool Equals(object obj)
        {
            Column eo = (Column)obj;
            return eo.TableCatalog == this.TableCatalog
                && eo.TableSchema == this.TableSchema
                && eo.TableName == this.TableName
                && eo.ColumnName == this.ColumnName;
                //&& eo.Type.GetType().ToString() == this.Type.GetType().ToString();
        }

        public override int GetHashCode()
        {
            return TableCatalog.GetHashCode() ^ TableSchema.GetHashCode() ^ TableName.GetHashCode() ^ ColumnName.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}-{4}", TableCatalog, TableSchema, TableName, ColumnName, Type);
        }

        public Column()
        {

        }
    }
}
