﻿using DuPE.Domain.RulesForMasking;
using System;

namespace DuPE.Domain.DataTypes
{
    public abstract class DataType
    {
        public string OriginalName { get; private set; }
        public DataType(string OriginalName)
        {
            if (string.IsNullOrWhiteSpace(OriginalName))
                throw new ArgumentException();

            this.OriginalName = OriginalName;
        }

        public abstract bool IsSame(DataType type);
        public abstract bool IsOkForMasking(DataType type);
    }
}
