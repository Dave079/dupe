﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.DataTypes
{
    public class TimeDataType : DataType
    {
        public TimeSpan From { get; private set; }
        public TimeSpan To { get; private set; }

        public TimeDataType(string OriginalName, TimeSpan From, TimeSpan To) : base(OriginalName)
        {
            this.From = From;
            this.To = To;
        }

        public override bool IsSame(DataType type)
        {
            return true;
        }

        public override bool IsOkForMasking(DataType type)
        {
            return true;
        }
    }
}
