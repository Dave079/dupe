﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.DataTypes
{
    public class NumberDataType : DataType
    {
        public double Min { get; private set; }
        public double Max { get; private set; }
        public uint NumberOfDecimalPLaces { get; private set; }

        public NumberDataType(string OriginalName, double Min, double Max, uint NumberOfDecimalPLaces) : base(OriginalName)
        {
            this.Min = Min;
            this.Max = Max;
            this.NumberOfDecimalPLaces = NumberOfDecimalPLaces;
        }

        public override bool IsSame(DataType type)
        {
            var tp = type as NumberDataType;
            return Min == tp.Min && Max == tp.Max && NumberOfDecimalPLaces == tp.NumberOfDecimalPLaces;
        }

        public override bool IsOkForMasking(DataType type)
        {
            var tp = type as NumberDataType;
            return (Min >= tp.Min && Max <= tp.Max);
        }
    }
}
