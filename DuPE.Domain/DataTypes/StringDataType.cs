﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.DataTypes
{
    public class StringDataType : DataType
    {
        public int? CharacterMaximumLength { get; private set; }

        public StringDataType(string OriginalName, int? CharacterMaximumLength) : base(OriginalName)
        {
            this.CharacterMaximumLength = CharacterMaximumLength;
        }

        public override bool IsSame(DataType type)
        {
            var tp = type as StringDataType;
            return CharacterMaximumLength == tp.CharacterMaximumLength;
        }

        public override bool IsOkForMasking(DataType type)
        {
            var tp = type as StringDataType;

            if ((CharacterMaximumLength != null && tp.CharacterMaximumLength != null && CharacterMaximumLength > tp.CharacterMaximumLength)
                || (tp.CharacterMaximumLength != null && CharacterMaximumLength == null))
                return false;
            else
                return true;
        }
    }
}
