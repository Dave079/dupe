﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.DataTypes
{
    public class DateTimeDataType : DataType
    {
        public DateTime From { get; private set; }
        public DateTime To { get; private set; }

        public DateTimeDataType(string OriginalName, DateTime From, DateTime To) : base(OriginalName)
        {
            this.From = From;
            this.To = To;
        }

        public override bool IsSame(DataType type)
        {
            return true;
        }

        public override bool IsOkForMasking(DataType type)
        {
            return true;
        }
    }
}
