﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.DataTypes
{
    public class UnknownDataType : DataType
    {
        public UnknownDataType(string OriginalName) : base(OriginalName)
        {
        }

        public override bool IsOkForMasking(DataType type)
        {
            return true;
        }

        public override bool IsSame(DataType type)
        {
            return true;
        }
    }
}
