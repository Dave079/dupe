﻿using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.Projects;
using DuPE.Domain.Resources;
using DuPE.Domain.RuleProcesors;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Masking
{
    public class MaskingProcesor
    {
        private IGeneralRepository _generalRepositry;
        private ICollectionRepository _collectionRepository;

        private Dictionary<string, Collection> _collections;

        public MaskingProcesor(IGeneralRepository GeneralRepositry, ICollectionRepository CollectionRepository)
        {
            this._generalRepositry = GeneralRepositry;
            this._collectionRepository = CollectionRepository;
        }

        public int StartMasking(Project project, MaskingType type)
        {
            _collections = new Dictionary<string, Collection>();
            var rules = GetAllRules(project);
            IList<IRuleProcesor> procesor = new List<IRuleProcesor>();
            int usedRuleCount = 0;
            foreach (var rule in rules)
            {
                switch (type)
                {
                    case MaskingType.AllRule:
                        break;
                    case MaskingType.OnlyUnusedRule:
                        if (rule.WasUsed())
                            continue;
                        break;
                    case MaskingType.OnlyUsedRule:
                        if (!rule.WasUsed())
                            continue;
                        break;
                    default:
                        throw new Exception();
                }
                usedRuleCount++;

                switch (rule.GetRuleType())
                {
                    case RuleType.String:
                        procesor.Add(new ProcesorForStringRule(rule));
                        break;
                    case RuleType.Number:
                        procesor.Add(new ProcesorForNumberRule(rule));
                        break;
                    case RuleType.DateTime:
                        procesor.Add(new ProcesorForDateTimeRule(rule));
                        break;
                    case RuleType.Time:
                        procesor.Add(new ProcessorForTimeRule(rule));
                        break;
                    default:
                        break;
                }
            }
            _generalRepositry.Update(procesor);

            return usedRuleCount;
        }

        private string GetKEyForCollection(Collection collection)
        {
            return string.Format("{0}_{1}", collection.Type.ToString(), collection.Name);
        }

        private List<IRuleForMasking> GetAllRules(Project project)
        {
            var res = new List<IRuleForMasking>();
            Column primaryKey;

            foreach (var table in project.Tables)
            {
                primaryKey = null;

                foreach (var col in table.Columns)
                {
                    if (col.RuleForMasking != null)
                    {
                        if (primaryKey == null)
                        {
                            primaryKey = GetPrimaryKeyFromTable(table);
                        }

                        if (col.RuleForMasking.GetCollection() != null)
                        {
                            string key = GetKEyForCollection(col.RuleForMasking.GetCollection());
                            if (!_collections.ContainsKey(key))
                            {
                                var collection = _collectionRepository.Get(col.RuleForMasking.GetCollection().Name, col.RuleForMasking.GetCollection().Type);
                                if (collection == null)
                                {
                                    throw new Exception(string.Format(StringsResource.CollectionDoesNotExist, key));
                                }
                                else
                                    _collections.Add(key, collection);
                            }

                            col.RuleForMasking.SetCollection(_collections[key]);

                        }
                        col.RuleForMasking.SetPrimaryKey(primaryKey);
                        res.Add(col.RuleForMasking);
                    }
                }
            }
            return res;
        }

        private Column GetPrimaryKeyFromTable(Tables.Table table)
        {
            foreach (var item in table.Columns)
            {
                if (item.IsPrimaryKey)
                    return item;
            }

            throw new Exception(StringsResource.PrimaryKeyNotFound);
        }
    }

    public enum MaskingType
    {
        AllRule,
        OnlyUnusedRule,
        OnlyUsedRule
    }
}
