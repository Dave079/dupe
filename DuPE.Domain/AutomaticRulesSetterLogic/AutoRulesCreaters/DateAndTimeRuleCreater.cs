﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.AutomaticRulesSetterLogic.AutoRulesCreaters
{
    public class DateTimeRuleCreater : IRuleCreater
    {
        public RulesForMasking.IRuleForMasking CreateRule(Columns.Column column)
        {
            var type = (DateTimeDataType)column.Type;

            return new RuleForDateTimeMasking(column) { From = type.From, To = type.To };
        }
    }
}
