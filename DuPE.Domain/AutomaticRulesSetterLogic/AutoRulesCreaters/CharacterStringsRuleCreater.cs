﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.AutomaticRulesSetterLogic.AutoRulesCreaters
{
    public class CharacterStringsRuleCreater : IRuleCreater
    {
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const int defaultLength = 10;

        public IRuleForMasking CreateRule(Column column)
        {
            var dataType = (StringDataType)column.Type;

            int lenghth = 0;

            if ((lenghth = dataType.CharacterMaximumLength ?? defaultLength) == -1)
                lenghth = defaultLength;

            lenghth = defaultLength;

            if (lenghth > 1000)
                lenghth = 34;

            return new RuleForStringMasking(column) { CharCollection = chars, MinLength = 1, MaxLenght = lenghth };
        }
    }
}
