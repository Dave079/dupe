﻿using DuPE.Domain.Columns;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.AutomaticRulesSetterLogic.AutoRulesCreaters
{
    public interface IRuleCreater
    {
        IRuleForMasking CreateRule(Column column);
    }
}
