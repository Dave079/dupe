﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;

namespace DuPE.Domain.AutomaticRulesSetterLogic.AutoRulesCreaters
{
    public class ExactNumericsRuleCreater : IRuleCreater
    {
        public IRuleForMasking CreateRule(Column column)
        {
            double min = 0;
            double max = 0;
            uint numberOfDecimalPLaces = 0;
            double nullProbability = 0;

            if (column.IsNullable)
                nullProbability = 0.15;

            var type = (NumberDataType)column.Type;
            min = type.Min;
            max = type.Max;
            numberOfDecimalPLaces = type.NumberOfDecimalPLaces;

            return new RuleForNumberMasking(column) { Min = min, Max = max, NUmberOfDecimalPLaces = numberOfDecimalPLaces, NullProbability = nullProbability };
        }
    }
}
