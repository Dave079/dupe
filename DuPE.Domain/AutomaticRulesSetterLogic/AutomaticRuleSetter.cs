﻿using DuPE.Domain.AutomaticRulesSetterLogic.AutoRulesCreaters;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Tables;
using System.Collections.Generic;

namespace DuPE.Domain.AutomaticRulesSetterLogic
{
    public class AutomaticRuleSetter
    {
        public void AddRuleForColumnWithoutRule(IEnumerable<Table> tables)
        {
            foreach (var table in tables)
            {
                if (table.HavePrimaryKey)
                    foreach (var column in table.Columns)
                    {
                        if (!(column.IsforeignKey || column.IsPrimaryKey || column.RuleForMasking != null))
                            AddRule(column, table);
                    }
            }
        }

        private void AddRule(Column column, Table table)
        {
            IRuleCreater RuleCreater = null;

            if (column.Type is NumberDataType)
                RuleCreater = new ExactNumericsRuleCreater();
            else if (column.Type is StringDataType)
                RuleCreater = new CharacterStringsRuleCreater();
            else if (column.Type is NumberDataType)
                RuleCreater = new DateTimeRuleCreater();

            if (RuleCreater != null)
                column.RuleForMasking = RuleCreater.CreateRule(column);
        }
    }
}
