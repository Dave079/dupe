﻿using DuPE.Domain.RulesForMasking;
using System;

namespace DuPE.Domain.RuleProcesors
{
    public class ProcesorForNumberRule : BaseRuleProcesor, IRuleProcesor
    {
        private RuleForNumberMasking _ruleForNUmmberMasking;

        public ProcesorForNumberRule(IRuleForMasking ruleFormasking)
            : base(ruleFormasking)
        {
            _ruleForNUmmberMasking = (RuleForNumberMasking)ruleFormasking;
        }

        protected override string GetRandomValue()
        {
            var res = _random.Next((int)_ruleForNUmmberMasking.Min, (int)_ruleForNUmmberMasking.Max);

            if (_ruleForNUmmberMasking.NUmberOfDecimalPLaces == 0)
                return res.ToString();
            else
            {
                double res2 = Math.Round(res + _random.NextDouble(), (int)_ruleForNUmmberMasking.NUmberOfDecimalPLaces);
                return res2.ToString();//.Replace(',', '.');
            }
        }

        protected override string GeHashedValue(string key, object value)
        {
            var res = BitConverter.ToInt64(GetHashedValue(value.ToString()), 0);

            res %=(int)_ruleForNUmmberMasking.Max;

            return Math.Abs(res).ToString();
        }

        public IRuleProcesor GetCopy()
        {
            return new ProcesorForNumberRule(_ruleForNUmmberMasking);
        }

        public RuleType GetRuleType()
        {
            return RuleType.Number;
        }
    }
}
