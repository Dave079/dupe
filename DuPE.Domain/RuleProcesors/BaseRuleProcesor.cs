﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using System;
using System.Security.Cryptography;
using System.Text;

namespace DuPE.Domain.RuleProcesors
{
    public abstract class BaseRuleProcesor
    {
        protected Random _random;
        private IRuleForMasking _rule;

        protected abstract string GetRandomValue();
        protected abstract string GeHashedValue(string key, object value);

        public BaseRuleProcesor(IRuleForMasking rule)
        {
            this._rule = rule;
            this._rule.Used();
            _random = new Random(RandomInitializer.Instance.GetIniNumber());
        }

        public string GetSchemaName()
        {
            return _rule.GetSchemaName();
        }

        public string GetCatalogName()
        {
            return _rule.GetCatalogName();
        }

        public string GetTableName()
        {
            return _rule.GetTableName();
        }

        public string GetColumnName()
        {
            return _rule.GetColumnName();
        }

        public string GetPrimaryKeyName()
        {
            return _rule.GetPrimaryKeyName();
        }

        public DataType GetPrimaryKeyDataType()
        {
            return _rule.GetPrimaryKeyDataType();
        }

        public DataType GetColumnDataType()
        {
            return _rule.GetColumnDataType();
        }

        protected string GetValueFromCollection(string key)
        {
            string res;

            if (_rule.SelectFromCollectionByHash())
            {
                res = _rule.GetCollection().Data[GetIdByHash(key) % _rule.GetCollection().Data.Count];
            }
            else
            {
                res = _rule.GetCollection().Data[_random.Next(_rule.GetCollection().Data.Count)];
            }


            if (res.Length > 20)
                res = res.Substring(0, 20);

            return res;

        }

        public string GetNextValue(string key, object value)
        {
            if (_rule.GetNullProbability() != 0 && _random.NextDouble() <= _rule.GetNullProbability())
                return null;

            if (_rule.GetCollection() != null)
                return GetValueFromCollection(key);

            if (_rule.GetHashType() != HashType.NA)
                return GeHashedValue(key, value);

            return GetRandomValue();
        }

        public bool WithValue()
        {
            return _rule.GetHashType() != HashType.NA;
        }

        protected byte[] GetHashedValue(string value)
        {
            switch (_rule.GetHashType())
            {
                case HashType.SHA1:
                    using (var sha = SHA1.Create())
                    {
                        return sha.ComputeHash(Encoding.ASCII.GetBytes(value));
                    }
                case HashType.SHA2_256:
                    using (var sha = SHA256.Create())
                    {
                        return sha.ComputeHash(Encoding.ASCII.GetBytes(value));
                    }
                case HashType.SHA2_384:
                    using (var sha = SHA384.Create())
                    {
                        return sha.ComputeHash(Encoding.ASCII.GetBytes(value));
                    }
                case HashType.SHA2_512:
                    using (var sha = SHA512.Create())
                    {
                        return sha.ComputeHash(Encoding.ASCII.GetBytes(value));
                    }
                case HashType.MD5:
                    using (var sha = MD5.Create())
                    {
                        return sha.ComputeHash(Encoding.ASCII.GetBytes(value));
                    }
                case HashType.NA:
                default:
                    throw new Exception();
            }
        }

        private int GetIdByHash(string key)
        {
            using (var sha1 = SHA1.Create())
            {
                return Math.Abs((int)(BitConverter.ToInt64(sha1.ComputeHash(Encoding.ASCII.GetBytes(key)), 0) % int.MaxValue));
            }
        }

    }
}
