﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.RuleProcesors
{
    public class ProcessorForTimeRule : BaseRuleProcesor, IRuleProcesor
    {
        private RuleForTimeMasking _ruleForMasking;

        public ProcessorForTimeRule(IRuleForMasking ruleFormasking)
            : base(ruleFormasking)
        {
            _ruleForMasking = (RuleForTimeMasking)ruleFormasking;
        }

        protected override string GetRandomValue()
        {
            var res = _random.Next(0, (int)(_ruleForMasking.To - _ruleForMasking.From).TotalSeconds);
            return string.Format("{0}", (DateTime.MinValue.AddSeconds(res)).ToString("HH:mm:ss")); //ToString("MM.dd.yyyy HH:mm:ss"));
        }

        protected override string GeHashedValue(string key, object value)
        {
            throw new NotImplementedException();
        }


        public IRuleProcesor GetCopy()
        {
            return new ProcesorForDateTimeRule(_ruleForMasking);
        }

        public RuleType GetRuleType()
        {
            return RuleType.Time;
        }
    }
}
