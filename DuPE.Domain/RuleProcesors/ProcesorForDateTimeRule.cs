﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RuleProcesors
{
    public class ProcesorForDateTimeRule : BaseRuleProcesor, IRuleProcesor
    {
        private RuleForDateTimeMasking _ruleForMasking;

        public ProcesorForDateTimeRule(IRuleForMasking ruleFormasking)
            : base(ruleFormasking)
        {
            _ruleForMasking = (RuleForDateTimeMasking)ruleFormasking;
        }

        protected override string GetRandomValue()
        {
            var res = _random.Next(0, (int)(_ruleForMasking.To - _ruleForMasking.From).TotalSeconds);
            return string.Format("{0}", _ruleForMasking.From.AddSeconds(res).ToString("o")); //ToString("MM.dd.yyyy HH:mm:ss"));
        }

        protected override string GeHashedValue(string key, object value)
        {
            throw new NotImplementedException();
        }


        public IRuleProcesor GetCopy()
        {
            return new ProcesorForDateTimeRule(_ruleForMasking);
        }

        public RuleType GetRuleType()
        {
            return RuleType.DateTime;
        }
    }
}
