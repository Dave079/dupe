﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RuleProcesors
{
    public sealed class RandomInitializer
    {

        private static RandomInitializer _instance;
        private Random _randomGenerator;

        public static RandomInitializer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RandomInitializer();
                }
                return _instance;
            }
        }

        private RandomInitializer()
        {
            _randomGenerator = new Random(DateTime.Now.Second);
        }

        public int GetIniNumber()
        {
            return _randomGenerator.Next();
        }


    }
}
