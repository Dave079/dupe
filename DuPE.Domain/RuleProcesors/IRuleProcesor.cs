﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RuleProcesors
{
    public interface IRuleProcesor
    {
        string GetNextValue(string key, object value = null);
        string GetCatalogName();
        string GetSchemaName();
        string GetTableName();
        string GetColumnName();
        string GetPrimaryKeyName();
        DataType GetPrimaryKeyDataType();
        DataType GetColumnDataType();
        RuleType GetRuleType();
        bool WithValue();
        IRuleProcesor GetCopy();

    }
}
