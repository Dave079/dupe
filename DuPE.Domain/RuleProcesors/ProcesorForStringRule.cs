﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RuleProcesors
{
    public class ProcesorForStringRule : BaseRuleProcesor, IRuleProcesor
    {
        private RuleForStringMasking _ruleForStringMasking;

        public ProcesorForStringRule(IRuleForMasking ruleFormasking)
            : base(ruleFormasking)
        {
            _ruleForStringMasking = (RuleForStringMasking)ruleFormasking;
        }

        protected override string GetRandomValue()
        {
            int length = _random.Next(_ruleForStringMasking.MinLength, _ruleForStringMasking.MaxLenght);
            string res = "";
            for (int i = 0; i < length; i++)
            {
                res += _ruleForStringMasking.CharCollection[_random.Next(_ruleForStringMasking.CharCollection.Count())];
            }

            return string.Format("{0}", res);
        }

        protected override string GeHashedValue(string key, object value)
        {
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in GetHashedValue((string)value))
                s.Append(b.ToString("x2").ToLower());

            var newValue = s.ToString();

            if (newValue.Length >= _ruleForStringMasking.MaxLenght)
                newValue = newValue.Substring(0, _ruleForStringMasking.MaxLenght);

            return newValue;
        }


        public IRuleProcesor GetCopy()
        {
            return new ProcesorForStringRule(_ruleForStringMasking);
        }

        public RuleType GetRuleType()
        {
            return RuleType.String;
        }

    }
}
