﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Logging
{
    public class EmptyLogger : ILogger
    {
        public bool IsDebugEnabled { get { return false; } }

        public bool IsErrorEnabled { get { return false; } }

        public bool IsFatalEnabled { get { return false; } }

        public bool IsInfoEnabled { get { return false; } }

        public bool IsWarnEnabled { get { return false; } }

        public void Debug(object message)
        {
        }

        public void Debug(object message, Exception exception)
        {
        }

        public void Error(object message)
        {
        }

        public void Error(object message, Exception exception)
        {
        }

        public void Fatal(object message)
        {
        }

        public void Fatal(object message, Exception exception)
        {
        }

        public void Info(object message)
        {
        }

        public void Info(object message, Exception exception)
        {
        }

        public void Warn(object message)
        {
        }

        public void Warn(object message, Exception exception)
        {
        }
    }
}
