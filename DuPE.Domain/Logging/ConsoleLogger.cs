﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.Logging
{
    public class ConsoleLogger : ILogger
    {
        public ConsoleLogger
            (bool isDebugEnabled = false,
            bool isInfoEnabled = true,
            bool isWarnEnabled = true,
            bool isErrorEnabled = true,
            bool isFatalEnabled = true,
            string DateTmeFormat = "yyyy MM.dd. HH:mm:ss.fff")
        {
            this.isDebugEnabled = isDebugEnabled;
            this.isInfoEnabled = isInfoEnabled;
            this.isWarnEnabled = isWarnEnabled;
            this.isErrorEnabled = isErrorEnabled;
            this.isFatalEnabled = isFatalEnabled;
        }

        private bool isDebugEnabled;
        public bool IsDebugEnabled { get { return isDebugEnabled; } }

        private bool isErrorEnabled;
        public bool IsErrorEnabled { get { return isErrorEnabled; } }

        private bool isFatalEnabled;
        public bool IsFatalEnabled { get { return isFatalEnabled; } }

        private bool isInfoEnabled;
        public bool IsInfoEnabled { get { return isInfoEnabled; } }

        private bool isWarnEnabled;
        public bool IsWarnEnabled { get { return isWarnEnabled; } }


        public void Debug(object message)
        {
            Console.WriteLine(string.Format("{0} [DEBUG] -> {1}",DateTime.Now,message));
        }

        public void Debug(object message, Exception exception)
        {
            Console.WriteLine(string.Format("{0} [DEBUG] -> {1}", DateTime.Now,message));
            Console.WriteLine(exception);
        }

        public void Error(object message)
        {
            Console.WriteLine(string.Format("{0} [ERROR] -> {1}", DateTime.Now,message));
        }

        public void Error(object message, Exception exception)
        {
            Console.WriteLine(string.Format("{0} [ERROR] -> {1}", DateTime.Now,message));
            Console.WriteLine(exception);
        }

        public void Fatal(object message)
        {
            Console.WriteLine(string.Format("{0} [FATAL] -> {1}", DateTime.Now,message));
        }

        public void Fatal(object message, Exception exception)
        {
            Console.WriteLine(string.Format("{0} [FATAL] -> {1}", DateTime.Now,message));
            Console.WriteLine(exception);
        }

        public void Info(object message)
        {
            Console.WriteLine(string.Format("{0} [INFO] -> {1}", DateTime.Now,message));
        }

        public void Info(object message, Exception exception)
        {
            Console.WriteLine(string.Format("{0} [INFO] -> {1}", DateTime.Now,message));
            Console.WriteLine(exception);
        }

        public void Warn(object message)
        {
            Console.WriteLine(string.Format("{0} [WARN] -> {1}", DateTime.Now,message));
        }

        public void Warn(object message, Exception exception)
        {
            Console.WriteLine(string.Format("{0} [WARN] -> {1}", DateTime.Now,message));
            Console.WriteLine(exception);
        }
    }
}
