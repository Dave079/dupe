﻿using DuPE.Domain.RuleProcesors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Tables
{
    public interface IGeneralRepository
    {
        void Update(IList<IRuleProcesor> procesor);
        IList<Table> GetAll();
        string GetConnectionString();
        string GetSchema();
    }
}
