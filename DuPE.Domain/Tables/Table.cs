﻿using DuPE.Domain.Columns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.Domain.Tables
{
    public class Table
    {
        public string TableCatalog { get; set; }
        public string TableSchema { get; set; }
        public string TableName { get; set; }
        public string TableType { get; set; }

        public IList<Column> Columns { get; set; }
        public bool HavePrimaryKey { get; private set; }

        public Table(IList<Column> Columns)
        {
            this.Columns = Columns;
            CheckPrimaryKey();
        }

        public void CheckPrimaryKey()
        {
            HavePrimaryKey = false;
            foreach (var column in Columns)
            {
                if (column.IsPrimaryKey)
                {
                    HavePrimaryKey = true;
                    break;
                }
            }
        }

        public Table()
        {

        }

        public override bool Equals(object obj)
        {
            Table eo = (Table)obj;
            return eo.TableCatalog == this.TableCatalog
                && eo.TableSchema == this.TableSchema
                && eo.TableName == this.TableName;
        }

        public override int GetHashCode()
        {
            return TableCatalog.GetHashCode() ^ TableSchema.GetHashCode() ^ TableName.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}", TableCatalog, TableSchema, TableName, TableType);
        }
    }
}
