﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public enum HashType
    {
        NA,
        SHA1,
        SHA2_256,
        SHA2_384,
        SHA2_512,
        MD5
    }
}