﻿using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public class RuleForDateTimeMasking : BaseRuleForMasking, IRuleForMasking
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public IRuleForMasking CreateCopy(Columns.Column column)
        {
            var newRule = new RuleForDateTimeMasking(column);
            CopyTo(newRule);
            newRule.From = From;
            newRule.To = To;

            return newRule;
        }

        public RuleForDateTimeMasking(Columns.Column Column)
            : base(RuleType.DateTime, Column)
        {

        }

        protected override string GetOtherDescription()
        {
            return string.Format(StringsResource.DateRuleOtherDescription, From, To);
        }
    }
}
