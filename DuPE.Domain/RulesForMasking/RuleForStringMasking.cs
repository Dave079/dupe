﻿using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public class RuleForStringMasking : BaseRuleForMasking, IRuleForMasking
    {
        public int MaxLenght { get; set; }
        public int MinLength { get; set; }
        public string CharCollection { get; set; }

        public IRuleForMasking CreateCopy(Columns.Column column)
        {
            var newRule = new RuleForStringMasking(column);
            CopyTo(newRule);
            newRule.MaxLenght = MaxLenght;
            newRule.MinLength = MinLength;
            newRule.CharCollection = CharCollection;

            return newRule;
        }

    
        public RuleForStringMasking(Columns.Column Column)
            : base(RuleType.String, Column)
        {

        }

        protected override string GetOtherDescription()
        {
            return string.Format(StringsResource.StringRuleOtherDecription, MinLength, MaxLenght, CharCollection);
        }
    }
}