﻿using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public interface IRuleForMasking
    {
        RuleType GetRuleType();
        string GetDescription();
        int GetOrderId();
        void SetOrderId(int order);
        void SetCollection(Collection collection);

        void SetPrimaryKey(Column colmn);

        string GetCatalogName();
        string GetSchemaName();
        string GetTableName();
        string GetColumnName();
        string GetPrimaryKeyName();
        Collection GetCollection();
        bool SelectFromCollectionByHash();
        HashType GetHashType();
        double GetNullProbability();
        DataType GetColumnDataType();
        DataType GetPrimaryKeyDataType();

        bool WasUsed();
        void Used();

        IRuleForMasking CreateCopy(Column column);
    }
}
