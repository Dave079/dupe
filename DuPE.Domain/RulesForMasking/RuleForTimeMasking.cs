﻿using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.Domain.RulesForMasking
{
    public class RuleForTimeMasking : BaseRuleForMasking, IRuleForMasking
    {
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }

        public IRuleForMasking CreateCopy(Columns.Column column)
        {
            var newRule = new RuleForTimeMasking(column);
            CopyTo(newRule);
            newRule.From = From;
            newRule.To = To;

            return newRule;
        }

        public RuleForTimeMasking(Columns.Column Column)
            : base(RuleType.Time, Column)
        {

        }
    
        protected override string GetOtherDescription()
        {
            return string.Format(StringsResource.StringRuleOtherDecription, From, To);
        }
    }
}
