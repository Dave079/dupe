﻿using DuPE.Domain.Collections;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public abstract class BaseRuleForMasking
    {
        protected RuleType type;
        protected int order;
        protected string columnName;
        protected string tableName;
        protected string schemaName;
        protected string catalogName;
        protected DataType columnDataType;

        protected Column primaryKey;

        public double NullProbability { get; set; }

        public Collection collection { get; set; }
        public bool selectFromCollectionByHash { get; set; }

        public HashType HashTypeForMasking { get; set; }

        protected abstract string GetOtherDescription();

        public bool wasUsed;

        protected void CopyTo(BaseRuleForMasking rule)
        {
            rule.type = type;
            rule.order = order;
            rule.NullProbability = NullProbability;
            rule.collection = collection;
            rule.selectFromCollectionByHash = selectFromCollectionByHash;
            rule.HashTypeForMasking = HashTypeForMasking;
            rule.columnDataType = columnDataType;
        }

        public BaseRuleForMasking()
        {

        }
        public BaseRuleForMasking(RuleType Type, Columns.Column Column)
        {
            this.type = Type;
            this.columnName = Column.ColumnName;
            this.tableName = Column.TableName;
            this.catalogName = Column.TableCatalog;
            this.schemaName = Column.TableSchema;
            this.HashTypeForMasking = HashType.NA;
            this.columnDataType = Column.Type;
            this.collection = null;
        }

        public string GetDescription()
        {
            if (collection != null)
                return string.Format(StringsResource.BaseRuleDescription, GetColumnName(), collection.Name, NullProbability);
            else
                return string.Format("{0} -> {1},Null:{2}%", GetColumnName(), GetOtherDescription(), NullProbability);
        }

        public HashType GetHashType()
        {
            return HashTypeForMasking;
        }

        public RuleType GetRuleType()
        {
            return type;
        }

        public int GetOrderId()
        {
            return order;
        }

        public void SetOrderId(int id)
        {
            order = id;
        }

        public string GetSchemaName()
        {
            return schemaName;
        }

        public string GetCatalogName()
        {
            return catalogName;
        }


        public string GetTableName()
        {
            return tableName;
        }

        public string GetColumnName()
        {
            return columnName;
        }

        public string GetPrimaryKeyName()
        {
            return primaryKey.ColumnName;
        }

        public DataType GetPrimaryKeyDataType()
        {
            return primaryKey.Type;
        }

        public void SetPrimaryKey(Column primaryKeyColumn)
        {
            primaryKey = primaryKeyColumn;

        }

        public Collection GetCollection()
        {
            return collection;
        }

        public void SetCollection(Collection coll)
        {
            collection = coll;
        }

        public bool SelectFromCollectionByHash()
        {
            return selectFromCollectionByHash;
        }

        public void Used()
        {
            wasUsed = true;
        }

        public bool WasUsed()
        {
            return wasUsed;
        }

        public double GetNullProbability()
        {
            return NullProbability;
        }

        public DataType GetColumnDataType()
        {
            return columnDataType;
        }


    }
}
