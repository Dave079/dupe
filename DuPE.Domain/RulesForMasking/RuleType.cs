﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public enum RuleType
    {
        String,
        Number,
        DateTime,
        Time
    }
}
