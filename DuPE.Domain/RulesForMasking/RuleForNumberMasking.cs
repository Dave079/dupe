﻿using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.RulesForMasking
{
    public class RuleForNumberMasking : BaseRuleForMasking, IRuleForMasking
    {
        public double Max { get; set; }
        public double Min { get; set; }
        public uint NUmberOfDecimalPLaces { get; set; }

        public IRuleForMasking CreateCopy(Columns.Column column)
        {
            var newRule = new RuleForNumberMasking(column);
            CopyTo(newRule);
            newRule.Max = Max;
            newRule.Min = Min;
            newRule.NUmberOfDecimalPLaces = NUmberOfDecimalPLaces;

            return newRule;
        }

        public RuleForNumberMasking(Columns.Column Column)
            : base(RuleType.Number, Column)
        {

        }

        protected override string GetOtherDescription()
        {
            return string.Format(StringsResource.NumberRuleOtherDecription, Min, Max);
        }

    }
}
