﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Collections
{
    public class CollectionForImport
    {
        public string FilePath { get; private set; }
        public RuleType Type { get; private set; }
        public string Name { get; private set; }

        public CollectionForImport(string FilePath, RuleType Type, string Name)
        {
            this.FilePath = FilePath;
            this.Type = Type;
            this.Name = Name;
        }
    }
}
