﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Collections
{
    public interface ICollectionRepository
    {
        bool IsExists(CollectionForImport collectionForImport);
        void Save(Collection collection);
        IList<Collection> GetAllWithoutData();
        IList<Collection> GetAll();
        IList<Collection> GetAllWithoutDataByType(RuleType type);
        Collection Get(string name, RuleType type);
        bool Delete(Collection collection);
    }
}
