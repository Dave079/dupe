﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Collections
{
    public interface ICollectionParser
    {
        Collection GetData(CollectionForImport coll);
    }
}
