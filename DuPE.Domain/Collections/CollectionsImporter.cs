﻿using DuPE.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Collections
{
    public class CollectionsImporter
    {
        private ICollectionRepository _collectionRepository;
        private ICollectionParser _collectionParser;

        public CollectionsImporter(ICollectionRepository collectionRepository, ICollectionParser collectionParser)
        {
            _collectionRepository = collectionRepository;
            _collectionParser = collectionParser;
        }

        public void ImportCollectionFromFile(string filePath, CollectionForImport collectionForImport)
        {
            if (_collectionRepository.IsExists(collectionForImport))
            {
                throw new Exception(string.Format(StringsResource.CollectionNameAlreadyExist, collectionForImport.Name));
            }

            var newCollection = _collectionParser.GetData(collectionForImport);
            _collectionRepository.Save(newCollection);
        }
    }
}
