﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.Domain.Collections
{
    public class Collection
    {
        public List<string> Data { get; set; }
        public string Name { get; set; }
        public RuleType Type { get; set; }

        public Collection()
        {
            Data = new List<string>();
        }

        public override string ToString()
        {
            return string.Format("{0}, type: {1},record count: {2}", Name, Type, Data == null ? 0 : Data.Count);
        }

    }
}
