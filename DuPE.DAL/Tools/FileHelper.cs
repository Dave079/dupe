﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Tools
{
    public class FileHelper
    {
        public static void SaveFileAsXml<T>(T objectToSave, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                XmlSerializer bf = new XmlSerializer(typeof(T));

                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                bf.Serialize(fs, objectToSave, ns);
                fs.Close();
            }
        }

        public static T LoadXmlFile<T>(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                XmlSerializer bf = new XmlSerializer(typeof(T));

                var item = (((T)bf.Deserialize(fs)));
                fs.Close();
                return item;
            }
        }

        public static void CheckPath(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
