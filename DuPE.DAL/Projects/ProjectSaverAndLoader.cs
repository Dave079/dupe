﻿using DuPE.DAL.Projects.ObjectForSave;
using DuPE.DAL.Tools;
using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Projects;
using DuPE.Domain.Projects.ObjectForSave;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DuPE.DAL.Projects
{
    public class ProjectSaverAndLoader : IProjectSaverAndLoader
    {
        private const string encryptionKey = "69f8129823c14591a88be206e8161afe";
        private const string initialisationVector = "4d9b00f63974590d0ecff55416d5ee35";

        public void Save(Project objectToSave)
        {
            if (objectToSave == null)
                throw new ArgumentNullException();

            var crypt = new AESCBCPKCS7(encryptionKey, initialisationVector);

            objectToSave.LastSave = DateTime.Now;
            var projectForSave = new ProjectXML(objectToSave);
            projectForSave.SourceConnetionString = crypt.EncryptText(projectForSave.SourceConnetionString);
            projectForSave.TargetConnetionString = crypt.EncryptText(projectForSave.TargetConnetionString);

            FileHelper.SaveFileAsXml<ProjectXML>(projectForSave, objectToSave.Path);
        }

        public Project Load(string path)
        {
            if (!File.Exists(path))
            {
                throw new ArgumentException(String.Format(StringsResource.FileNotFound,path));
            }

            var crypt = new AESCBCPKCS7(encryptionKey, initialisationVector);
            ProjectXML item = FileHelper.LoadXmlFile<ProjectXML>(path);

            item.SourceConnetionString = crypt.DecryptText(item.SourceConnetionString);
            item.TargetConnetionString = crypt.DecryptText(item.TargetConnetionString);

            return CreateActualProject(item, path);
        }

        private Project CreateActualProject(ProjectXML item, string path)
        {
            Project res = new Project();
            res.Path = path;
            res.Name = item.Name;
            res.Database = item.Database;

            res.SourceConnetionString = item.SourceConnetionString;
            res.TargetConnetionString = item.TargetConnetionString;

            res.SourceSchema = item.SourceSchema;
            res.TargetSchema = item.TargetSchema;

            res.LastSave = item.LastSave;

            res.Tables = new List<Table>();
            if (item.Tables != null)
                foreach (var tab in item.Tables)
                {
                    res.Tables.Add(new Table(CreateColumns(tab))
                    {
                        TableCatalog = tab.TableCatalog,
                        TableName = tab.TableName,
                        TableSchema = tab.TableSchema,
                        TableType = tab.TableType
                    });
                }

            return res;
        }

        private IList<Column> CreateColumns(TableXML tab)
        {
            var columns = new Dictionary<string, Column>();
            if (tab.Columns != null)
                foreach (var col in tab.Columns)
                {
                    var column = new Column()
                    {
                        TableCatalog = tab.TableCatalog,
                        TableSchema = tab.TableSchema,
                        TableName = tab.TableName,
                        ColumnName = col.ColumnName,
                        ColumnDefault = col.ColumnDefault,
                        Type = CreateDataTpye(col),
                        IsNullable = col.IsNullable,
                        IsPrimaryKey = col.IsPrimaryKey,
                        IsforeignKey = col.IsforeignKey
                    };

                    if (!columns.ContainsKey(column.ColumnName))
                        columns.Add(column.ColumnName, column);
                }

            AddRules(tab, columns);

            return columns.Values.ToList();
        }

        private DataType CreateDataTpye(ColumnXML col)
        {
            if (col.NumberDateType != null)
            {
                return new NumberDataType(col.NumberDateType.OriginalName, col.NumberDateType.Min, col.NumberDateType.Max, col.NumberDateType.NUmberOfDecimalPLaces);
            }
            else if (col.StringDataType != null)
            {
                return new StringDataType(col.StringDataType.OriginalName, col.StringDataType.MaxLength);
            }
            else if (col.DateTimeDataType != null)
            {
                return new DateTimeDataType(col.DateTimeDataType.OriginalName, col.DateTimeDataType.From, col.DateTimeDataType.To);
            }
            else if (col.TimeDataType != null)
            {
                return new TimeDataType(col.TimeDataType.OriginalName, col.TimeDataType.From, col.TimeDataType.To);
            }
            else if (col.UnsupportedDataType != null)
            {
                return new UnsupportedDataType(col.UnsupportedDataType.OriginalName);
            }
            else if (col.UnknownDataType != null)
            {
                return new UnknownDataType(col.UnknownDataType.OriginalName);
            }
            else
            {
                throw new Exception("Xmlcolumn doesnt have datatype");
            }
        }

        private void AddRules(TableXML tab, Dictionary<string, Column> columns)
        {
            AddNumberRules(tab, columns);
            AddDateTimeRules(tab, columns);
            AddStringRules(tab, columns);
            AddTimeRules(tab, columns);
        }

      

        private void AddNumberRules(TableXML tab, Dictionary<string, Column> columns)
        {
            if (tab.NumberRules != null)
                foreach (var NumberRule in tab.NumberRules)
                {
                    if (!columns.ContainsKey(NumberRule.ColumnName))
                        continue;

                    var column = columns[NumberRule.ColumnName];
                    {
                        if (string.IsNullOrWhiteSpace(NumberRule.CollectionName))
                            column.RuleForMasking = new RuleForNumberMasking(column) { HashTypeForMasking = NumberRule.HashTypeForMasking, wasUsed = NumberRule.WasUsed, Min = NumberRule.Min, Max = NumberRule.Max, NUmberOfDecimalPLaces = NumberRule.NUmberOfDecimalPLaces, NullProbability = NumberRule.NullProbability };
                        else
                            column.RuleForMasking = new RuleForNumberMasking(column) { HashTypeForMasking = NumberRule.HashTypeForMasking, wasUsed = NumberRule.WasUsed, selectFromCollectionByHash = NumberRule.useHash, NullProbability = NumberRule.NullProbability, collection = new Domain.Collections.Collection() { Name = NumberRule.CollectionName, Type = RuleType.Number } };
                    }
                }
        }

        private void AddDateTimeRules(TableXML tab, Dictionary<string, Column> columns)
        {
            if (tab.DateTimeRules != null)
                foreach (var DateTimeRule in tab.DateTimeRules)
                {
                    if (!columns.ContainsKey(DateTimeRule.ColumnName))
                        continue;

                    var column = columns[DateTimeRule.ColumnName];
                    {
                        if (string.IsNullOrWhiteSpace(DateTimeRule.CollectionName))
                            column.RuleForMasking = new RuleForDateTimeMasking(column) { HashTypeForMasking = DateTimeRule.HashTypeForMasking, wasUsed = DateTimeRule.WasUsed, From = DateTimeRule.From, To = DateTimeRule.To, NullProbability = DateTimeRule.NullProbability };
                        else
                            column.RuleForMasking = new RuleForDateTimeMasking(column) { HashTypeForMasking = DateTimeRule.HashTypeForMasking, wasUsed = DateTimeRule.WasUsed, selectFromCollectionByHash = DateTimeRule.useHash, NullProbability = DateTimeRule.NullProbability, collection = new Domain.Collections.Collection() { Name = DateTimeRule.CollectionName, Type = RuleType.DateTime } };
                    }
                }
        }

        private void AddTimeRules(TableXML tab, Dictionary<string, Column> columns)
        {
            if (tab.TimeRules != null)
                foreach (var DateTimeRule in tab.TimeRules)
                {
                    if (!columns.ContainsKey(DateTimeRule.ColumnName))
                        continue;

                    var column = columns[DateTimeRule.ColumnName];
                    {
                        if (string.IsNullOrWhiteSpace(DateTimeRule.CollectionName))
                            column.RuleForMasking = new RuleForTimeMasking(column) { HashTypeForMasking = DateTimeRule.HashTypeForMasking, wasUsed = DateTimeRule.WasUsed, From = DateTimeRule.From, To = DateTimeRule.To, NullProbability = DateTimeRule.NullProbability };
                        else
                            column.RuleForMasking = new RuleForDateTimeMasking(column) { HashTypeForMasking = DateTimeRule.HashTypeForMasking, wasUsed = DateTimeRule.WasUsed, selectFromCollectionByHash = DateTimeRule.useHash, NullProbability = DateTimeRule.NullProbability, collection = new Domain.Collections.Collection() { Name = DateTimeRule.CollectionName, Type = RuleType.DateTime } };
                    }
                }
        }

        private void AddStringRules(TableXML tab, Dictionary<string, Column> columns)
        {
            if (tab.StringRules != null)
                foreach (var StringRule in tab.StringRules)
                {
                    if (!columns.ContainsKey(StringRule.ColumnName))
                        continue;

                    var column = columns[StringRule.ColumnName];
                    {
                        if (string.IsNullOrWhiteSpace(StringRule.CollectionName))
                            column.RuleForMasking = new RuleForStringMasking(column) { HashTypeForMasking = StringRule.HashTypeForMasking, wasUsed = StringRule.WasUsed, MinLength = StringRule.MinLength, MaxLenght = StringRule.MaxLength, CharCollection = StringRule.CharCollection, NullProbability = StringRule.NullProbability };
                        else
                            column.RuleForMasking = new RuleForStringMasking(column) { HashTypeForMasking = StringRule.HashTypeForMasking, wasUsed = StringRule.WasUsed, selectFromCollectionByHash = StringRule.useHash, NullProbability = StringRule.NullProbability, collection = new Domain.Collections.Collection() { Name = StringRule.CollectionName, Type = RuleType.String } };
                    }
                }
        }
    }
}
