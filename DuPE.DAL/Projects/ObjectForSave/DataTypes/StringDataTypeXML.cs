﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.DataTypes
{
    [XmlType("StringDataType")]
    public class StringDataTypeXML
    {
        [XmlAttribute]
        public string OriginalName { get; set; }
        [XmlElement]
        public int? MaxLength { get; set; }
    }
}
