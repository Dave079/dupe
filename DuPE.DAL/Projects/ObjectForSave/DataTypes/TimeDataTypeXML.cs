﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.DataTypes
{
    [XmlType("TimeDataType")]
    public class TimeDataTypeXML
    {
        private string format = @"h\:mm\:ss";

        [XmlAttribute]
        public string OriginalName { get; set; }


        [XmlIgnore]
        public TimeSpan From { get; set; }

        [XmlAttribute("from")]
        public string FromInString
        {
            get { return this.From.ToString(format); }
            set { TimeSpan tmp; TimeSpan.TryParse(value, out tmp); From = tmp; }
        }


        [XmlIgnore]
        public TimeSpan To { get; set; }

        [XmlAttribute("To")]
        public string ToInString
        {
            get { return this.To.ToString(format); }
            set { TimeSpan tmp; TimeSpan.TryParse(value, out tmp); To = tmp; }
        }


        public TimeDataTypeXML()
        {

        }
    }
}
