﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.DataTypes
{
    [XmlType("UnknownDataType")]
    public class UnknownDataTypeXML
    {
        [XmlAttribute]
        public string OriginalName { get; set; }

        public UnknownDataTypeXML()
        {

        }
    }
}
