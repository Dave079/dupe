﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.DataTypes
{
    [XmlType("DateTimeDataType")]
    public class DateTimeDataTypeXML
    {
        [XmlAttribute]
        public string OriginalName { get; set; }
        [XmlAttribute]
        public DateTime From { get; set; }
        [XmlAttribute]
        public DateTime To { get; set; }

        public DateTimeDataTypeXML()
        {

        }
    }
}
