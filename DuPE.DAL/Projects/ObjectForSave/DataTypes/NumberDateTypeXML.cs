﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.DataTypes
{
    [XmlType("NumberDateType")]
    public class NumberDateTypeXML
    {
        [XmlAttribute]
        public string OriginalName { get; set; }
        [XmlElement]
        public double Max { get; set; }
        [XmlElement]
        public double Min { get; set; }
        [XmlElement]
        public uint NUmberOfDecimalPLaces { get; set; }

        public NumberDateTypeXML()
        {

        }
    }
}
