﻿using DuPE.DAL.Projects.ObjectForSave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.Domain.Projects.ObjectForSave
{
    [XmlType("Project")]
    public class ProjectXML
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Path { get; set; }
        [XmlAttribute]
        public string SourceConnetionString { get; set; }
        [XmlAttribute]
        public string TargetConnetionString { get; set; }
        [XmlAttribute]
        public string SourceSchema { get; set; }
        [XmlAttribute]
        public string TargetSchema { get; set; }
        [XmlAttribute]
        public string Database { get; set; }

        [XmlElement("Table")]
        public List<TableXML> Tables { get; set; }

        [XmlElement("TimestampOfLastSave")]
        public DateTime? LastSave { get; set; }
        public ProjectXML()
        {
        }

        public ProjectXML(Projects.Project project)
        {
            this.Name = project.Name;
            this.SourceConnetionString = project.SourceConnetionString;
            this.TargetConnetionString = project.TargetConnetionString;

            this.SourceSchema = project.SourceSchema;
            this.TargetSchema = project.TargetSchema;

            this.Database = project.Database;

            this.LastSave = project.LastSave;

            Tables = new List<TableXML>();
            if (project.Tables != null)
                foreach (var tab in project.Tables)
                {
                    Tables.Add(new TableXML(tab));
                }
        }
    }
}
