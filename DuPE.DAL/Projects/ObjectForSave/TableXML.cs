﻿using DuPE.DAL.Projects.ObjectForSave.Rules;
using DuPE.Domain.RulesForMasking;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave
{
    [XmlType("Table")]
    public class TableXML
    {
        [XmlAttribute]
        public string TableCatalog { get; set; }
        [XmlAttribute]
        public string TableSchema { get; set; }
        [XmlAttribute]
        public string TableName { get; set; }
        [XmlAttribute]
        public string TableType { get; set; }

        public List<ColumnXML> Columns { get; set; }

        public List<NumberRuleXML> NumberRules { get; set; }
        public List<StringRuleXML> StringRules { get; set; }
        public List<DateTimeRuleXML> DateTimeRules { get; set; }
        public List<TimeRuleXML> TimeRules { get; set; }

        public TableXML()
        {

        }

        public TableXML(Table table)
        {
            this.TableCatalog = table.TableCatalog;
            this.TableSchema = table.TableSchema;
            this.TableName = table.TableName;
            this.TableType = table.TableType;
            Columns = new List<ColumnXML>();
            if (table.Columns != null)
            {
                foreach (var col in table.Columns)
                {
                    Columns.Add(new ColumnXML(col));

                    if (col.RuleForMasking != null)
                    {
                        switch (col.RuleForMasking.GetRuleType())
                        {
                            case DuPE.Domain.RulesForMasking.RuleType.Number:
                                if (NumberRules == null)
                                    NumberRules = new List<NumberRuleXML>();

                                NumberRules.Add(new NumberRuleXML((RuleForNumberMasking)col.RuleForMasking) {ColumnName = col.ColumnName });
                                break;
                            case DuPE.Domain.RulesForMasking.RuleType.String:
                                if (StringRules == null)
                                    StringRules = new List<StringRuleXML>();
                                StringRules.Add(new StringRuleXML((RuleForStringMasking)col.RuleForMasking) { ColumnName = col.ColumnName });
                                break;
                            case DuPE.Domain.RulesForMasking.RuleType.DateTime:
                                if (DateTimeRules == null)
                                    DateTimeRules = new List<DateTimeRuleXML>();
                                DateTimeRules.Add(new DateTimeRuleXML((RuleForDateTimeMasking)col.RuleForMasking) { ColumnName = col.ColumnName });
                                break;
                            case DuPE.Domain.RulesForMasking.RuleType.Time:
                                if (TimeRules == null)
                                    TimeRules = new List<TimeRuleXML>();
                                TimeRules.Add(new TimeRuleXML((RuleForTimeMasking)col.RuleForMasking) { ColumnName = col.ColumnName });
                                break;
                            default:
                                break;
                        }
                    }

                }
            }

        }

    }
}
