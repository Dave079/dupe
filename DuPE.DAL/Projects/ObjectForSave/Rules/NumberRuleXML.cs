﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.Rules
{
    [XmlType("NumberRule")]
    public class NumberRuleXML
    {
        [XmlAttribute]
        public string ColumnName { get; set; }
        [XmlAttribute]
        public double Max { get; set; }
        [XmlAttribute]
        public double Min { get; set; }

        [XmlAttribute]
        public uint NUmberOfDecimalPLaces { get; set; }

        [XmlAttribute]
        public double NullProbability { get; set; }

        [XmlAttribute]
        public string CollectionName { get; set; }
        [XmlAttribute]
        public bool useHash { get; set; }

        [XmlAttribute]
        public bool WasUsed { get; set; }

        [XmlAttribute]
        public HashType HashTypeForMasking { get; set; }

        public NumberRuleXML(RuleForNumberMasking rule)
        {
            
            this.Max = rule.Max;
            this.Min = rule.Min;
            this.NUmberOfDecimalPLaces = rule.NUmberOfDecimalPLaces;
            this.NullProbability = rule.NullProbability;
            this.WasUsed = rule.wasUsed;
            this.HashTypeForMasking = rule.HashTypeForMasking;

            if (rule.collection != null)
            {
                this.CollectionName = rule.collection.Name;
                this.useHash = rule.selectFromCollectionByHash;
            }
        }

        public NumberRuleXML()
        {

        }
    }
}
