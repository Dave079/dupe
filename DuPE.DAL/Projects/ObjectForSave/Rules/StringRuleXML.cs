﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.Rules
{
    [XmlType("StringRule")]
    public class StringRuleXML
    {
        [XmlAttribute]
        public string ColumnName { get; set; }
        [XmlAttribute]
        public int MaxLength { get; set; }
        [XmlAttribute]
        public int MinLength { get; set; }
        [XmlAttribute]
        public string CharCollection { get; set; }

        [XmlAttribute]
        public double NullProbability { get; set; }

        [XmlAttribute]
        public string CollectionName { get; set; }
        [XmlAttribute]
        public bool useHash { get; set; }

        [XmlAttribute]
        public bool WasUsed { get; set; }

        [XmlAttribute]
        public HashType HashTypeForMasking { get; set; }


        public StringRuleXML(RuleForStringMasking rule)
        {
            this.MaxLength = rule.MaxLenght;
            this.MinLength = rule.MinLength;
            this.NullProbability = rule.NullProbability;
            this.CharCollection = rule.CharCollection;
            this.WasUsed = rule.wasUsed;
            this.HashTypeForMasking = rule.HashTypeForMasking;

            if (rule.collection != null)
            {
                this.CollectionName = rule.collection.Name;
                this.useHash = rule.selectFromCollectionByHash;
            }
        }

        public StringRuleXML()
        {

        }
    }
}
