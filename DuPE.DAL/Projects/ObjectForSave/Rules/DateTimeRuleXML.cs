﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.Rules
{
    [XmlType("DateTimeRule")]
    public class DateTimeRuleXML
    {
        [XmlAttribute]
        public string ColumnName { get; set; }
        [XmlAttribute]
        public DateTime From { get; set; }
        [XmlAttribute]
        public DateTime To { get; set; }

        [XmlAttribute]
        public double NullProbability { get; set; }

        [XmlAttribute]
        public string CollectionName { get; set; }
        [XmlAttribute]
        public bool useHash { get; set; }

        [XmlAttribute]
        public bool WasUsed { get; set; }

        [XmlAttribute]
        public HashType HashTypeForMasking { get; set; }

        public DateTimeRuleXML(RuleForDateTimeMasking rule)
        {
            this.From = rule.From;
            this.To = rule.To;
            this.NullProbability = rule.NullProbability;
            this.WasUsed = rule.wasUsed;
            this.HashTypeForMasking = rule.HashTypeForMasking;

            if (rule.collection != null)
            {
                this.CollectionName = rule.collection.Name;
                this.useHash = rule.selectFromCollectionByHash;
            }
        }

        public DateTimeRuleXML()
        {

        }
    }
}
