﻿using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave.Rules
{

    [XmlType("TimeRule")]
    public class TimeRuleXML
    {
        private string format = @"h\:mm\:ss";

        [XmlIgnore]
        public TimeSpan From { get; set; }

        [XmlAttribute("from")]
        public string FromInString
        {
            get { return this.From.ToString(format); }
            set { TimeSpan tmp; TimeSpan.TryParse(value, out tmp); From = tmp; }
        }


        [XmlIgnore]
        public TimeSpan To { get; set; }

        [XmlAttribute("To")]
        public string ToInString
        {
            get { return this.To.ToString(format); }
            set { TimeSpan tmp; TimeSpan.TryParse(value, out tmp); To = tmp; }
        }

        [XmlAttribute]
        public string ColumnName { get; set; }

        [XmlAttribute]
        public double NullProbability { get; set; }

        [XmlAttribute]
        public string CollectionName { get; set; }
        [XmlAttribute]
        public bool useHash { get; set; }

        [XmlAttribute]
        public bool WasUsed { get; set; }

        [XmlAttribute]
        public HashType HashTypeForMasking { get; set; }

        public TimeRuleXML(RuleForTimeMasking rule)
        {
            this.From = rule.From;
            this.To = rule.To;
            this.NullProbability = rule.NullProbability;
            this.WasUsed = rule.wasUsed;
            this.HashTypeForMasking = rule.HashTypeForMasking;

            if (rule.collection != null)
            {
                this.CollectionName = rule.collection.Name;
                this.useHash = rule.selectFromCollectionByHash;
            }
        }

        public TimeRuleXML()
        {

        }
    }
}