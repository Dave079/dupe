﻿using DuPE.DAL.Projects.ObjectForSave.DataTypes;
using DuPE.Domain.Columns;
using System.Xml.Serialization;

namespace DuPE.DAL.Projects.ObjectForSave
{
    [XmlType("Column")]
    public class ColumnXML
    {
        [XmlAttribute]
        public string ColumnName { get; set; }
        [XmlAttribute]
        public string ColumnDefault { get; set; }
        [XmlAttribute]
        public bool IsNullable { get; set; }
        [XmlAttribute]
        public bool IsPrimaryKey { get; set; }
        [XmlAttribute]
        public bool IsforeignKey { get; set; }

        [XmlElement]
        public StringDataTypeXML StringDataType { get; set; }
        [XmlElement]
        public NumberDateTypeXML NumberDateType { get; set; }
        [XmlElement]
        public DateTimeDataTypeXML DateTimeDataType { get; set; }
        [XmlElement]
        public TimeDataTypeXML TimeDataType { get; set; }

        [XmlElement]
        public UnknownDataTypeXML UnknownDataType { get; set; }
        [XmlElement]
        public UnsupportedDataTypeXML UnsupportedDataType { get; set; }

        public ColumnXML()
        {

        }

        public ColumnXML(Column col)
        {
            this.ColumnName = col.ColumnName;
            this.ColumnDefault = col.ColumnDefault;
            this.IsNullable = col.IsNullable;

            this.IsPrimaryKey = col.IsPrimaryKey;
            this.IsforeignKey = col.IsforeignKey;

            if (col.Type is Domain.DataTypes.StringDataType)
            {
                var type = (Domain.DataTypes.StringDataType)col.Type;
                StringDataType = new StringDataTypeXML() { MaxLength = type.CharacterMaximumLength, OriginalName = type.OriginalName };
            }
            else if (col.Type is Domain.DataTypes.NumberDataType)
            {
                var type = (Domain.DataTypes.NumberDataType)col.Type;
                NumberDateType = new NumberDateTypeXML() { OriginalName = type.OriginalName, Max = type.Max, Min = type.Min, NUmberOfDecimalPLaces = type.NumberOfDecimalPLaces };
            }
            else if (col.Type is Domain.DataTypes.DateTimeDataType)
            {
                var type = (Domain.DataTypes.DateTimeDataType)col.Type;
                DateTimeDataType = new DateTimeDataTypeXML() { From = type.From, To = type.To, OriginalName = type.OriginalName };
            }
            else if (col.Type is Domain.DataTypes.TimeDataType)
            {
                var type = (Domain.DataTypes.TimeDataType)col.Type;
                TimeDataType = new TimeDataTypeXML() { From = type.From, To = type.To, OriginalName = type.OriginalName };
            }
            else if (col.Type is Domain.DataTypes.UnsupportedDataType)
            {
                var type = (Domain.DataTypes.UnsupportedDataType)col.Type;
                UnsupportedDataType = new UnsupportedDataTypeXML() { OriginalName = type.OriginalName };
            }
            else if (col.Type is Domain.DataTypes.UnknownDataType)
            {
                var type = (Domain.DataTypes.UnknownDataType)col.Type;
                UnknownDataType = new UnknownDataTypeXML() { OriginalName = type.OriginalName };
            }
        }
    }
}
