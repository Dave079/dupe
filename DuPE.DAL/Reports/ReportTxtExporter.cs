﻿using DuPE.Domain.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.DAL.Reports
{
    public class ReportTxtExporter : IReportExporter
    {
        public string GetFileFilterForSuffix()
        {
            return "Text file (*.txt)|*.txt";
        }
        public void SaveToFile(IList<ReportMessage> reports, string filePath)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
            {
                foreach (var line in reports)
                {
                    file.WriteLine(line.ToString());
                }
            }
        }
    }
}
