﻿using DuPE.Domain.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.DAL.Collections.XMLForSave
{
    public class CollectionXML
    {
        public List<string> Data { get; set; }
        public string Name { get; set; }

        public CollectionXML()
        {

        }

        public CollectionXML(Collection collection)
        {
            Data = new List<string>();
            if (collection.Data != null)
                foreach (var item in collection.Data)
                {
                    Data.Add(item);
                }
            Name = collection.Name;
        }
    }
}
