﻿using DuPE.Domain.Collections;
using System;
using System.IO;
using System.Text;

namespace DuPE.DAL.Collections.Parsers
{
    public class CollectionTxtFileParser : ICollectionParser
    {
        private Encoding w1250 = Encoding.GetEncoding("Windows-1250");

        private delegate bool DataChecker(ref string item);
        private DataChecker DataIsCorrect = null;

        public Collection GetData(CollectionForImport collectionForImport)
        {
            if (collectionForImport == null)
                throw new ArgumentNullException("collectionForImport");

            if (!File.Exists(collectionForImport.FilePath))
                throw new FileNotFoundException(string.Format(StringsResource.FileNotFound, collectionForImport.FilePath));

            SetDataChecker(collectionForImport.Type);

            Collection newCollection = new Collection();
            newCollection.Name = collectionForImport.Name;
            newCollection.Type = collectionForImport.Type;

            string result;
            using (StreamReader sr = new StreamReader(collectionForImport.FilePath))
            {
                while ((result = sr.ReadLine()) != null)
                {
                    if (DataIsCorrect(ref result))
                        newCollection.Data.Add(result.Trim());
                }
            }

            return newCollection;
        }

        private void SetDataChecker(Domain.RulesForMasking.RuleType ruleType)
        {
            switch (ruleType)
            {
                case DuPE.Domain.RulesForMasking.RuleType.String:
                    DataIsCorrect = StringrDataChecker;
                    break;
                case DuPE.Domain.RulesForMasking.RuleType.Number:
                    DataIsCorrect = NUmberDataChecker;
                    break;
                case DuPE.Domain.RulesForMasking.RuleType.DateTime:
                    DataIsCorrect = DateTimeDataChecker;
                    break;
                case DuPE.Domain.RulesForMasking.RuleType.Time:
                    DataIsCorrect = TimeDataChecker;
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private bool NUmberDataChecker(ref string result)
        {
            if (string.IsNullOrWhiteSpace(result))
                return false;
            if (double.TryParse(result, out double res))
            {
                return true;
            }
            else
                throw new FormatException(string.Format(StringsResource.ItemDoesNotHaveCorrectFormat, result));
        }

        private bool StringrDataChecker(ref string result)
        {
            if (string.IsNullOrWhiteSpace(result))
                return false;
            else
            {
                result = string.Format("{0}", result);
                return true;
            }
        }

        private bool DateTimeDataChecker(ref string result)
        {
            if (string.IsNullOrWhiteSpace(result))
                return false;

            DateTime res;
            if (DateTime.TryParse(result, out res))
            {
                result = string.Format("{0}", res.ToString());
                return true;
            }
            else
                throw new FormatException(string.Format(StringsResource.ItemDoesNotHaveCorrectFormat, result));
        }

        private bool TimeDataChecker(ref string result)
        {
            if (string.IsNullOrWhiteSpace(result))
                return false;

            TimeSpan res;
            if (TimeSpan.TryParse(result, out res))
            {
                result = string.Format("{0}", res.ToString());
                return true;
            }
            else
                throw new FormatException(string.Format(StringsResource.ItemDoesNotHaveCorrectFormat, result));
        }
    }
}
