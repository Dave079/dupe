﻿using DuPE.DAL.Collections.XMLForSave;
using DuPE.DAL.Tools;
using DuPE.Domain.Collections;
using DuPE.Domain.RulesForMasking;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DuPE.DAL.Collections.Repository
{
    public class CollectionRepository : ICollectionRepository
    {
        private const string CollectionExtension = "dupeColl";
        private const string SearchPatern = "*." + CollectionExtension;

        private readonly string PathOfData;
        public CollectionRepository(string dataStoragePath = @"Data\Collections")
        {
            if (!Directory.Exists(dataStoragePath))
            {
                Directory.CreateDirectory(dataStoragePath);
                //throw new ArgumentException(StringsResource.DirectoryDoesntExist);
            }

            PathOfData = dataStoragePath;
        }

        public bool IsExists(CollectionForImport collectionForImport)
        {
            if (collectionForImport == null)
                throw new ArgumentNullException();
            return File.Exists(Path.Combine(PathOfData, CreateCollectionName(collectionForImport.Name, collectionForImport.Type)));
        }

        public void Save(Collection collection)
        {
            if (collection == null)
                throw new ArgumentNullException();

            var path = Path.Combine(PathOfData, CreateCollectionName(collection.Name, collection.Type));
            FileHelper.CheckPath(PathOfData);
            FileHelper.SaveFileAsXml<CollectionXML>(new CollectionXML(collection), path);
        }

        private string CreateCollectionName(string name, RuleType type)
        {
            return string.Format("{0}_{1}.{2}", type.ToString(), name, CollectionExtension);
        }

        public IList<Collection> GetAllWithoutData()
        {
            var ListRes = new List<Collection>();
            if (Directory.Exists(PathOfData))
            {
                foreach (var item in Directory.GetFiles(PathOfData, SearchPatern))
                {
                    if (Path.GetFileName(item).StartsWith(RuleType.String.ToString()))
                    {
                        ListRes.Add(new Collection() { Name = GetName(item, RuleType.String), Type = RuleType.String });
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Number.ToString()))
                    {
                        ListRes.Add(new Collection() { Name = GetName(item, RuleType.Number), Type = RuleType.Number });

                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.DateTime.ToString()))
                    {
                        ListRes.Add(new Collection() { Name = GetName(item, RuleType.DateTime), Type = RuleType.DateTime });

                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Time.ToString()))
                    {
                        ListRes.Add(new Collection() { Name = GetName(item, RuleType.Time), Type = RuleType.Time });

                    }
                }
            }

            return ListRes;
        }

        private string GetName(string item, RuleType type)
        {
            return Path.GetFileNameWithoutExtension(item).Replace(type.ToString() + "_", "");
        }

        public IList<Collection> GetAllWithoutDataByType(RuleType type)
        {
            return GetAllWithoutData().Where(x => x.Type == type).ToList();
        }

        public Collection Get(string name, RuleType type)
        {
            Collection res = null;
            if (Directory.Exists(PathOfData))
            {
                foreach (var item in Directory.GetFiles(PathOfData, SearchPatern))
                {
                    var col = new Collection();

                    if (Path.GetFileName(item).StartsWith(RuleType.String.ToString()))
                    {
                        col.Type = RuleType.String;
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Number.ToString()))
                    {
                        col.Type = RuleType.Number;
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.DateTime.ToString()))
                    {
                        col.Type = RuleType.DateTime;
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Time.ToString()))
                    {
                        col.Type = RuleType.Time;
                    }

                    if (col.Type != type)
                        continue;

                    if (!GetName(item, type).Equals(name))
                        continue;

                    var data = FileHelper.LoadXmlFile<CollectionXML>(item);

                    col.Name = data.Name;
                    col.Data.AddRange(data.Data);
                    res = col;
                    break;
                }
            }

            return res;
        }

        public bool Delete(Collection collection)
        {
            if (collection == null)
                throw new ArgumentNullException();

            if (Directory.Exists(PathOfData))
            {
                foreach (var item in Directory.GetFiles(PathOfData, collection.Type.ToString() + "_" + collection.Name + "." + CollectionExtension))
                {
                    File.Delete(item);
                    return true;
                }
            }

            return false;
        }

        public IList<Collection> GetAll()
        {
            var ListRes = new List<Collection>();
            if (Directory.Exists(PathOfData))
            {
                Collection coll = null;

                foreach (var item in Directory.GetFiles(PathOfData, SearchPatern))
                {
                    if (Path.GetFileName(item).StartsWith(RuleType.String.ToString()))
                    {
                        coll = new Collection() { Name = GetName(item, RuleType.String), Type = RuleType.String };
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Number.ToString()))
                    {
                        coll = new Collection() { Name = GetName(item, RuleType.Number), Type = RuleType.Number };

                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.DateTime.ToString()))
                    {
                        coll = new Collection() { Name = GetName(item, RuleType.DateTime), Type = RuleType.DateTime };
                    }
                    else if (Path.GetFileName(item).StartsWith(RuleType.Time.ToString()))
                    {
                        coll = new Collection() { Name = GetName(item, RuleType.Time), Type = RuleType.Time };
                    }

                    if (coll != null)
                    {
                        var data = FileHelper.LoadXmlFile<CollectionXML>(item);
                        coll.Data.AddRange(data.Data);
                        ListRes.Add(coll);
                        coll = null;
                    }

                }
            }

            return ListRes;
        }
    }
}
