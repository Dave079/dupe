﻿SELECT 
    t.NAME AS TableName,
    s.Name AS SchemaName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS TotalSpaceMB,
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    CAST(ROUND(((SUM(a.used_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS UsedSpaceMB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB,
    CAST(ROUND(((SUM(a.total_pages) - SUM(a.used_pages)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSpaceMB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, s.Name, p.Rows
ORDER BY 
    t.Name



WITH 
ocols AS (
	SELECT o.object_id, c.column_id, o.name AS oname, c.name AS cname, 
		t.name AS tname, t.max_length, c.is_nullable,
	CASE 
		WHEN t.name IN ('char', 'binary') AND c.max_length >= 0 
			THEN c.max_length
		WHEN t.name IN ('varchar', 'varbinary', 'nvarchar') 
			AND c.max_length >= 0 THEN c.max_length + 2
		WHEN t.name IN ('nchar') THEN c.max_length / 2
		WHEN t.name IN ('bit') THEN 1
		WHEN t.name IN ('decimal', 'numeric', 'float', 'real') 
			THEN c.max_length
		WHEN t.name IN ('sql_variant', 'xml') THEN 0
		WHEN c.max_length = -1 THEN 0
		ELSE t.max_length
	END AS eff_length
	FROM sys.objects o
	INNER JOIN sys.columns c ON o.object_id = c.object_id
	INNER JOIN sys.types t ON c.user_type_id = t.user_type_id
	WHERE o.schema_id = 1
	AND o.type = 'U'
), 
ocolsum (object_id, column_count, is_nullable, eff_length) 
AS (
	SELECT object_id, count(column_id), is_nullable, sum(eff_length)
	FROM ocols
	GROUP BY object_id, is_nullable
),
Bytes AS (
	SELECT o.object_id, o.name, 
		ISNULL(ocolsum.column_count,0) + 
			ISNULL(ocolsumnull.column_count,0) AS NumCols,
		ISNULL(ocolsum.eff_length,0) AS FixedDataSize,
		ISNULL(ocolsumnull.column_count,0) AS NumVariableCols,
		ISNULL(ocolsumnull.eff_length,0) AS MaxVarSize,
		2 + ((ISNULL(ocolsum.column_count,0) + 
			ISNULL(ocolsumnull.column_count,0)  + 7) / 8 ) 
				AS NullBitmap,
		2 + (ISNULL(ocolsumnull.column_count,-1) * 2) + 
			ISNULL(ocolsumnull.eff_length,0) AS VarDataSize
	FROM sys.objects o
	LEFT OUTER JOIN ocolsum 
		ON o.object_id = ocolsum.object_id 
		AND ocolsum.is_nullable = 0
	LEFT OUTER JOIN ocolsum ocolsumnull 
		ON o.object_id = ocolsumnull.object_id 
		AND ocolsumnull.is_nullable = 1
	WHERE o.type = 'U'
)
SELECT name, NumCols, FixedDataSize, NumVariableCols, MaxVarSize, 
	NullBitmap, VarDataSize,
	FixedDataSize + VarDataSize + NullBitmap + 4 AS RowSize
FROM Bytes
ORDER BY name



go
create table ##tmpRowSize (TableName varchar(100),RowSizeDefinition int)
exec sp_msforeachtable 'INSERT INTO ##tmpRowSize Select ''?'' As TableName, SUM(C.Length) as Length from dbo.SysColumns C where C.id = object_id(''?'') '
select * from ##tmpRowSize order by RowSizeDefinition  desc
drop table ##tmpRowSize

select OBJECT_ID(customer)

Select name, SUM(C.Length) as Length from dbo.SysColumns C group by  name

select * from dbo.SysTables 
