﻿DROP TABLE customer;
CREATE TABLE customer
(
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    address character varying(200)  NOT NULL,
    phonenumber character varying(20) ,
    email character varying(254) ,
    ico character varying(20) ,
    note character varying(200) ,
    CONSTRAINT customer_pkey PRIMARY KEY (id)
)


DROP TABLE payment;

CREATE TABLE payment
(
    id integer NOT NULL,
    accountnumber character varying(45)  NOT NULL,
    variablesymbol integer,
    amount integer NOT NULL,
    CONSTRAINT payment_pkey PRIMARY KEY (id)
)


DROP TABLE product;

CREATE TABLE product
(
    id integer NOT NULL,
    ean integer,
    name character varying(45) NOT NULL,
    stock integer NOT NULL DEFAULT 0,
    price integer NOT NULL,
    note character varying(200) ,
    CONSTRAINT product_pkey PRIMARY KEY (id)
)

DROP TABLE productsonpurchaseorder;

CREATE TABLE productsonpurchaseorder
(
    id integer NOT NULL,
    quantity integer NOT NULL,
    discount integer NOT NULL DEFAULT 0,
    price integer NOT NULL,
    note character varying(200) ,
    purchaseorder_id integer NOT NULL,
    product_id integer NOT NULL
    --CONSTRAINT productsonpurchaseorder_pkey PRIMARY KEY (id),
    --CONSTRAINT fk_product_has_invoice_invoice1 FOREIGN KEY (purchaseorder_id)
    --    REFERENCES public.purchaseorder (id) MATCH SIMPLE
    --CONSTRAINT fk_product_has_invoice_product1 FOREIGN KEY (product_id)
    --    REFERENCES public.product (id) MATCH SIMPLE
)


DROP TABLE purchaseorder;

CREATE TABLE purchaseorder
(
    id integer NOT NULL,
    datetime datetime  NOT NULL,
    state character varying(15) NOT NULL,
    price integer NOT NULL,
    notefromcustomer character varying(200),
    notefromuser character varying(200) ,
    payment_id integer,
    user_id integer NOT NULL,
    customer_id integer NOT NULL
    --CONSTRAINT purchaseorder_pkey PRIMARY KEY (id),
    --CONSTRAINT fk_invoice_customer1 FOREIGN KEY (customer_id)
    --    REFERENCES public.customer (id) MATCH SIMPLE
    --    ON UPDATE NO ACTION
    --    ON DELETE NO ACTION,
    --CONSTRAINT fk_invoice_payment1 FOREIGN KEY (payment_id)
    --    REFERENCES public.payment (id) MATCH SIMPLE
    --    ON UPDATE NO ACTION
    --    ON DELETE NO ACTION,
    --CONSTRAINT fk_invoice_user1 FOREIGN KEY (user_id)
    --    REFERENCES public.users (id) MATCH SIMPLE
    --    ON UPDATE NO ACTION
    --    ON DELETE NO ACTION
)

DROP  TABLE users;


CREATE TABLE users
(
    id integer NOT NULL,
    name character varying(30)  NOT NULL,
    surname character varying(30)NOT NULL,
    lastlogin timestamp ,
    password character varying(20) NOT NULL,
    type character varying(10) NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)


select * from customer