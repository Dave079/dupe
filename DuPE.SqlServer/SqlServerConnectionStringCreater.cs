﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer
{
    public class SqlServerConnectionStringCreater
    {
        

        public static string Create(string server, string catalog, string user, string pass)
        {
            return string.Format("Server={0};Initial Catalog={1};User Id={2};Password={3}", server, catalog, user, pass);
        }
    }
}
