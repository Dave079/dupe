﻿using DuPE.Domain.Databases;
using DuPE.Domain.Logging;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer
{
    public class SqlDatabaseRepositoryCreater : IDatabaseRepositoryCreater
    {
        private ILogger logger;

        public SqlDatabaseRepositoryCreater(ILogger logger)
        {
            this.logger = logger;
        }

        public string Name => SqlServerConstants.DB_NAME;

        public IGeneralRepository GetGeneralRepository(Dictionary<string, object> arguments)
        {
            if (arguments.TryGetValue(SqlServerConstants.CONNECTION_STRING, out object connectionString)
                && arguments.TryGetValue(SqlServerConstants.SCHEMA, out object schema1))
                return new SqlServerRepository(logger, (string)connectionString, (string)schema1);
            else if (arguments.TryGetValue(SqlServerConstants.CATALOG, out object catalog)
                && arguments.TryGetValue(SqlServerConstants.SCHEMA, out object schema2)
                && arguments.TryGetValue(SqlServerConstants.PASS, out object pass)
                && arguments.TryGetValue(SqlServerConstants.SERVER, out object server)
                && arguments.TryGetValue(SqlServerConstants.USER, out object user))
                return new SqlServerRepository(logger, SqlServerConnectionStringCreater.Create((string)server, (string)catalog, (string)user, (string)pass), (string)schema2);
            else
                throw new Exception();
        }

        public IGeneralRepository GetGeneralRepository(string connectionString, string schema)
        {
            return new SqlServerRepository(logger, connectionString, schema);
        }
    }
}
