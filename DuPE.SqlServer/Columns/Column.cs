﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer.Columns
{
    public class SqlServerColumn
    {
        public string TableCatalog { get; set; }
        public string TableSchema { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public int OrdinalPosition { get; set; }
        public string ColumnDefault { get; set; }
        public string IsNullable { get; set; }
        public string DataType { get; set; }
        public int? CharacterMaximumLength { get; set; }
        public int? CharacterOctetLength { get; set; }
        public int? NumericPrecision { get; set; }
        public int? NumericPrecisionRadix { get; set; }
        public int? NumericScale { get; set; }
        public int? DatetimePrecision { get; set; }
        public string CharacterSetCatalog { get; set; }
        public string CharacterSetSchema { get; set; }
        public string CharacterSetName { get; set; }
        public string CollationCatalog { get; set; }
        public string CollationSchema { get; set; }
        public string CollationName { get; set; }
        public string DomainCatalog { get; set; }
        public string DomainSchema { get; set; }
        public string DomainName { get; set; }

        public bool IsPrimaryKey { get; set; }
        public bool IsforeignKey { get; set; }

        public override bool Equals(object obj)
        {
            SqlServerColumn eo = (SqlServerColumn)obj;
            return eo.TableCatalog == this.TableCatalog
                && eo.TableSchema == this.TableSchema
                && eo.TableName == this.TableName
                && eo.ColumnName == this.ColumnName;
        }

        public override int GetHashCode()
        {
            return TableCatalog.GetHashCode() ^ TableSchema.GetHashCode() ^ TableName.GetHashCode() ^ ColumnName.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}-{4}", TableCatalog, TableSchema, TableName, ColumnName, DataType);

        }

        public SqlServerColumn()
        {

        }

    }
}
