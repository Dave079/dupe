﻿using DuPE.Domain.Logging;
using DuPE.Domain.RuleProcesors;
using DuPE.Domain.Tables;
using DuPE.SqlServer.Database;
using DuPE.SqlServer.Database.Repository;
using System.Collections.Generic;

namespace DuPE.SqlServer
{
    public class SqlServerRepository : IGeneralRepository
    {
        private ILogger logger;
        private string connectionString;
        private string schema;

        //private TableRepository tableRepository;
        private BetterTableRepositry tableRepository;

        public SqlServerRepository(ILogger logger, string connectionString, string schema)
        {
            this.logger = logger;
            this.connectionString = connectionString;
            this.schema = schema;

          //  tableRepository = new TableRepository(new SessionFactoryBuilder(logger, connectionString));
            tableRepository = new BetterTableRepositry(new SessionFactoryBuilder(logger,connectionString), schema);
        }

        public string GetSchema()
        {
            return schema;
        }

        public IList<Table> GetAll()
        {
            return Converter.GetTables(tableRepository.GetAll());
        }

        public string GetConnectionString()
        {
            return connectionString;
        }

        public void Update(IList<IRuleProcesor> procesors)
        {
            tableRepository.Update(procesors);
        }
       
    }
}
