﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Tables;
using DuPE.SqlServer.Columns;
using DuPE.SqlServer.Tables;
using System;
using System.Collections.Generic;

namespace DuPE.SqlServer
{
    public class Converter
    {
        public static IList<Table> GetTables(IList<SqlServerTable> sqlServerTables)
        {
            var tables = new List<Table>();
            foreach (var sqlTable in sqlServerTables)
            {
                var table = CreateTable(sqlTable);
                table.Columns = CreateColumns(sqlTable.Columns);
                table.CheckPrimaryKey();
                tables.Add(table);
            }

            return tables;
        }
        private static Table CreateTable(SqlServerTable sqlTable)
        {
            var table = new Table()
            {
                TableCatalog = sqlTable.TableCatalog,
                TableName = sqlTable.TableName,
                TableSchema = sqlTable.TableSchema,
                TableType = sqlTable.TableType,
            };

            return table;
        }

        private static IList<Column> CreateColumns(IList<SqlServerColumn> sqlColumns)
        {
            var columns = new List<Column>();
            foreach (var sqlColumn in sqlColumns)
            {
                var column = new Column()
                {
                    ColumnName = sqlColumn.ColumnName,
                    ColumnDefault = sqlColumn.ColumnDefault,
                    IsNullable = sqlColumn.IsNullable.ToLower() == "yes",
                    IsforeignKey = sqlColumn.IsforeignKey,
                    TableCatalog = sqlColumn.TableCatalog,
                    TableName = sqlColumn.TableName,
                    TableSchema = sqlColumn.TableSchema,
                    Type = GetType(sqlColumn),
                    IsPrimaryKey = sqlColumn.IsPrimaryKey
                };

                columns.Add(column);
            }

            return columns;
        }

        private static DataType GetType(SqlServerColumn sqlColumn)
        {
            var type = sqlColumn.DataType.ToString();
            switch (type)
            {
                // case typeof(NumberDataType).ToString():
                //Exact Numerics

                case "bigint":
                case "bit":
                case "decimal":
                case "int":
                case "money":
                case "numeric":
                case "smallint":
                case "smallmoney":
                case "tinyint":
                //Approximate Numerics
                case "float":
                case "real":
                    return CreateNumberDataType(type);

                //Character Strings
                case "char":
                case "varchar":
                case "text":

                //Unicode Character Strings
                case "nchar":
                case "ntext":
                case "nvarchar":
                    return CreateStringDataType(type, sqlColumn);

                //Date and Time
                case "date":
                case "datetime2":
                case "datetime":

                case "datetimeoffset":
                case "smalldatetime":
                case "time":
                    return CreateDateTimeDataType(type);


                //Binary Strings
                case "binary":
                case "image":
                case "varbinary":

                //Other Data Types
                case "sql_variant":
                case "timestamp":
                case "uniqueidentifier":
                case "xml":
                    return new UnsupportedDataType(type);
                default:
                    return new UnknownDataType(type);

            }
        }

        private static DataType CreateDateTimeDataType(string type)
        {
            DateTime from;
            DateTime to;
            switch (type)
            {
                case "date":
                case "datetime2":
                case "datetime":
                case "datetimeoffset":
                case "smalldatetime":
                    from = DateTime.Now.AddMonths(-36);
                    to = DateTime.Now;
                    break;
                case "time":
                    return new TimeDataType(type, new TimeSpan(0), new TimeSpan(2, 2, 2));
                default:
                    return null;
            }
            return new DateTimeDataType(type, from, to);

        }

        private static DataType CreateStringDataType(string type, SqlServerColumn sqlColumn)
        {
            return new StringDataType(type, sqlColumn.CharacterMaximumLength);
        }

        private static DataType CreateNumberDataType(string type)
        {
            double min = 0;
            double max = 0;
            ushort numberOfDecimalPLaces = 0;

            switch (type)
            {
                case "bigint":
                    max = int.MaxValue;
                    break;
                case "bit":
                    max = 1;
                    break;
                case "decimal":
                    max = int.MaxValue;
                    numberOfDecimalPLaces = 2;
                    break;
                case "int":
                    max = Int32.MaxValue;
                    break;
                case "money":
                    max = 922337200;
                    numberOfDecimalPLaces = 2;

                    break;
                case "numeric":
                    max = Int32.MaxValue;

                    break;
                case "smallint":
                    max = Int16.MaxValue;
                    break;
                case "smallmoney":
                    max = 214748;
                    numberOfDecimalPLaces = 2;
                    break;
                case "tinyint":
                    max = byte.MaxValue;
                    break;
                case "float":
                    max = Int32.MaxValue;
                    numberOfDecimalPLaces = 2;
                    break;
                case "real":
                    max = Int32.MaxValue;
                    numberOfDecimalPLaces = 2;
                    break;
                default:
                    break;
            }

            return new NumberDataType(type, min, max, numberOfDecimalPLaces);
        }
    }
}
