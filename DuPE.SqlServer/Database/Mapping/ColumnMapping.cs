﻿using DuPE.SqlServer.Columns;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer.Database.Mapping
{
    public class ColumnMapping : ClassMap<SqlServerColumn>
    {
        public ColumnMapping()
        {
            this.Table("INFORMATION_SCHEMA.COLUMNS");
            this.CompositeId().KeyProperty(x => x.TableCatalog, "TABLE_CATALOG")
                              .KeyProperty(x => x.TableSchema, "TABLE_SCHEMA")
                              .KeyProperty(x => x.TableName, "TABLE_NAME")
                              .KeyProperty(x => x.ColumnName, "COLUMN_NAME");
            this.Map(x => x.OrdinalPosition).Column("ORDINAL_POSITION");
            this.Map(x => x.ColumnDefault).Column("COLUMN_DEFAULT");
            this.Map(x => x.IsNullable).Column("IS_NULLABLE");
            this.Map(x => x.DataType).Column("DATA_TYPE");
            this.Map(x => x.CharacterMaximumLength).Column("CHARACTER_MAXIMUM_LENGTH");
            this.Map(x => x.CharacterOctetLength).Column("CHARACTER_OCTET_LENGTH");
            this.Map(x => x.NumericPrecision).Column("NUMERIC_PRECISION");
            this.Map(x => x.NumericPrecisionRadix).Column("NUMERIC_PRECISION_RADIX");
            this.Map(x => x.NumericScale).Column("NUMERIC_SCALE");
            this.Map(x => x.DatetimePrecision).Column("DATETIME_PRECISION");
            this.Map(x => x.CharacterSetCatalog).Column("CHARACTER_SET_CATALOG");
            this.Map(x => x.CharacterSetSchema).Column("CHARACTER_SET_SCHEMA");
            this.Map(x => x.CharacterSetName).Column("CHARACTER_SET_NAME");
            this.Map(x => x.CollationCatalog).Column("COLLATION_CATALOG");
            this.Map(x => x.CollationSchema).Column("COLLATION_SCHEMA");
            this.Map(x => x.CollationName).Column("COLLATION_NAME");
            this.Map(x => x.DomainCatalog).Column("DOMAIN_CATALOG");
            this.Map(x => x.DomainSchema).Column("DOMAIN_SCHEMA");
            this.Map(x => x.DomainName).Column("DOMAIN_NAME");
        }
    }
}
