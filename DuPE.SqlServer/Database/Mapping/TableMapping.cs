﻿using DuPE.SqlServer.Tables;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer.Database.Mapping
{
    public class TableMapping : ClassMap<SqlServerTable>
    {
        public TableMapping()
        {
            this.Table("INFORMATION_SCHEMA.TABLES");
            this.CompositeId().KeyProperty(x => x.TableCatalog, "TABLE_CATALOG")
                              .KeyProperty(x => x.TableSchema, "TABLE_SCHEMA")
                              .KeyProperty(x => x.TableName, "TABLE_NAME");
            this.Map(x => x.TableType).Column("TABLE_TYPE");
            this.HasMany(x => x.Columns).AsBag().KeyColumns.Add("TABLE_CATALOG", "TABLE_SCHEMA", "TABLE_NAME");
        }
    }
}
