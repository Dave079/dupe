﻿using DuPE.Domain.Logging;
using DuPE.SqlServer.Database.Mapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace DuPE.SqlServer.Database
{
    public class SessionFactoryBuilder : ISessionFactoryBuilder
    {
        private readonly string connectionString;
        private static Configuration _config;
        protected ISessionFactory _factoryInstance;
        private ILogger logger;

        public SessionFactoryBuilder(ILogger logger, string connectionString)
        {
            this.connectionString = connectionString;
            this.logger = logger;
        }

        protected Configuration CreateConfiguration()
        {
            var config = Fluently.Configure();

            if (logger.IsDebugEnabled)
                config.Database(MsSqlConfiguration.MsSql2012.ConnectionString(connectionString).ShowSql);
            else
                config.Database(MsSqlConfiguration.MsSql2012.ConnectionString(connectionString));

            config.Mappings(m => m.FluentMappings

            .Add<ColumnMapping>().Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())
            .Add<TableMapping>().Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())
                );

            config.ExposeConfiguration(BuildSchema);
            _config = config.BuildConfiguration();

            return _config;
        }

        private void BuildSchema(Configuration config)
        {
            new SchemaUpdate(config).Execute(false, false);
        }

        public ISessionFactory GetFactory()
        {
            if (_factoryInstance == null)
            {
                var config = CreateConfiguration();
                _factoryInstance = config.BuildSessionFactory();
            }
            return _factoryInstance;
        }
    }
}
