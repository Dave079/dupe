﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.SqlServer.Database.Repository
{
    public class DefaultRepository<T>
    {
        protected readonly ISessionFactoryBuilder _sessionFB;


        public DefaultRepository(ISessionFactoryBuilder sessionFb)
        {
            _sessionFB = sessionFb;
        }

        public virtual IList<T> GetAll()
        {
            IList<T> list;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    list = session.CreateCriteria(typeof(T)).List<T>();

                    tx.Commit();
                }
            }
            return list;
        }

        public virtual IList<T> GetAll(ISession session)
        {
            IList<T> list;
            list = session.CreateCriteria(typeof(T)).List<T>();
            return list;
        }

        public void Save(T item)
        {
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    session.Save(item);

                    tx.Commit();
                }
            }
        }

        public virtual void Save(IList<T> items)
        {
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    foreach (var item in items)
                        session.Save(item);

                    tx.Commit();
                }
            }
        }

        public int Count()
        {
            int count;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    count = session.Query<T>().Count();

                    tx.Commit();
                }
            }
            return count;
        }

        public T FirstOrDefault(Expression<Func<T, bool>> query)
        {
            T item;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    item = session.Query<T>().FirstOrDefault(query);

                    tx.Commit();
                }
            }
            return item;
        }


    }

}
