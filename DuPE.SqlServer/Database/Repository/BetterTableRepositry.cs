﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RuleProcesors;
using DuPE.SqlServer.Tables;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;


namespace DuPE.SqlServer.Database.Repository
{
    public class BetterTableRepositry : DefaultRepository<SqlServerTable>
    {
        private readonly string defaultSchema;
        public BetterTableRepositry(ISessionFactoryBuilder sessionFb, string defaultSchema) : base(sessionFb)
        {
            this.defaultSchema = defaultSchema;
        }

        public override IList<SqlServerTable> GetAll()
        {
            IList<SqlServerTable> list;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    list = session.Query<SqlServerTable>().Fetch(x => x.Columns).Where(x => x.TableSchema == defaultSchema).ToList();
                    IList<object[]> objects = new List<object[]>();
                    foreach (var table in list)
                    {
                        SetKeys(table, objects, session);
                    }

                    foreach (var fk in objects)
                    {
                        var tt = list.FirstOrDefault(x => x.TableName.Equals(fk[6]));
                        if (tt != null)
                        {
                            var cc = tt.Columns.FirstOrDefault(x => x.ColumnName.Equals(fk[7]));
                            if (cc != null)
                                cc.IsforeignKey = true;
                        }
                    }

                    tx.Commit();
                }
            }
            return list;
        }

        private void SetKeys(SqlServerTable table, IList<object[]> objects, ISession session)
        {
            IList<object[]> pKeys = session.CreateSQLQuery(string.Format("EXEC sp_pkeys @table_name = N'{0}' ,@table_owner = N'{1}'", table.TableName, defaultSchema)).List<object[]>();
            for (int i = 0; i < pKeys.Count; i++)
            {
                var tt = table.Columns.FirstOrDefault(x => x.ColumnName.Equals(pKeys[i][3]));
                if (tt != null)
                    tt.IsPrimaryKey = true;
            }

            IList<object[]> fKeys = session.CreateSQLQuery(string.Format("EXEC sp_fkeys  @pktable_name = N'{0}' ,@pktable_owner = N'{1}'", table.TableName, defaultSchema)).List<object[]>();
            if (fKeys != null && fKeys.Count != 0)
                foreach (var fKey in fKeys)
                {
                    objects.Add(fKey);
                }
        }

        public void Update(IList<IRuleProcesor> procesors)
        {
            var groupsByTables = procesors.GroupBy(x => (x.GetCatalogName() + "." + x.GetSchemaName() + "." + x.GetTableName()));

            foreach (var tableGroup in groupsByTables)
            {
                UpdateTable(tableGroup);
            }
        }

        private void UpdateTable(IGrouping<string, IRuleProcesor> tableGroup)
        {
            List<object[]> values = null;
            List<object[]> editedValues = null;

            string catalog = tableGroup.FirstOrDefault().GetCatalogName();
            string schema = tableGroup.FirstOrDefault().GetSchemaName();
            string catalogAndSchema = string.Format("{0}.{1}", catalog, schema);
            string tempTableName;
            string sourceTableName = tableGroup.FirstOrDefault().GetTableName();

            string query = PrepareSQLQuery(tableGroup, catalogAndSchema, sourceTableName);

            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    values = session.CreateSQLQuery(query).List().Cast<object[]>().ToList();
                    editedValues = EditAndAddValues(values, tableGroup);
                    values = null;
                    tempTableName = CreateTempTabeName(session, tableGroup, catalog, schema, sourceTableName);
                    BulkInsert(session, tx, editedValues, tableGroup, catalogAndSchema, tempTableName);
                    MergeData(session, tableGroup, catalogAndSchema, tempTableName, sourceTableName);
                    DropTable(session, string.Format("{0}.{1}", catalogAndSchema, tempTableName));

                    tx.Commit();
                }
            }
        }

        private void DropTable(ISession session, string table)
        {
            session.CreateSQLQuery(string.Format("drop table {0};", table)).ExecuteUpdate();
        }
        private void MergeData(ISession session, IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tempTableName, string sourceTableName)
        {
            string query = string.Format("MERGE {0}.{1} AS TargetTable " +
                           "USING {0}.{2} AS SourceTable " +
                           "ON(TargetTable.{3} = SourceTable.{3}) " +
                           "WHEN MATCHED THEN " +
                           "UPDATE SET {4};",
                           catalogAndSchema,
                           sourceTableName,
                           tempTableName,
                           tableGroup.FirstOrDefault().GetPrimaryKeyName(),
                           string.Join(",", tableGroup.Select(x => string.Format("TargetTable.{0} = SourceTable.{0}", x.GetColumnName())))
                           );

            session.CreateSQLQuery(query).SetTimeout(0).ExecuteUpdate();
        }

        private string CreateTempTabeName(ISession session, IGrouping<string, IRuleProcesor> tableGroup, string catalog, string schema, string tableName)
        {
            string queryForTableName =
                "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE " +
                "TABLE_CATALOG = '" + catalog +
                "' and TABLE_SCHEMA = '" + schema +
                "' and TABLE_NAME = '{0}'";

            int version = 0;
            string tempTableName;

            while (true)
            {
                tempTableName = string.Format("{0}_Temp{1}", tableName, version++);

                if (session.CreateSQLQuery(string.Format(queryForTableName, tempTableName)).List().Cast<object>().ToList().Count == 0)
                {
                    session.CreateSQLQuery(
                        string.Format("SELECT {5},{4} INTO {0}.{1}.{3} FROM {0}.{1}.{2} WHERE 1 = 2",
                            catalog,
                            schema,
                            tableName,
                            tempTableName,
                            string.Join(",", tableGroup.Select(x => x.GetColumnName())),
                            tableGroup.FirstOrDefault().GetPrimaryKeyName()
                            )).ExecuteUpdate();
                    break;
                }
            }

            return tempTableName;
        }

        private void BulkInsert(ISession session, ITransaction tx, List<object[]> editedValues, IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tempTableName)
        {
            using (var cmd = new SqlCommand())
            {
                tx.Enlist(cmd);
                using (SqlBulkCopy bc = new SqlBulkCopy((SqlConnection)session.Connection, SqlBulkCopyOptions.Default, cmd.Transaction))
                {
                    bc.DestinationTableName = string.Format("{0}.{1}", catalogAndSchema, tempTableName);
                    Type type = null;
                    DataTable dt = new DataTable();
                    foreach (var ruleProcesor in tableGroup)
                    {
                        bc.ColumnMappings.Add(ruleProcesor.GetColumnName(), ruleProcesor.GetColumnName());

                        type = GetTypeForMapping(ruleProcesor.GetColumnDataType());
                        if (type != null)
                            dt.Columns.Add(new DataColumn(ruleProcesor.GetColumnName(), type));
                        else
                            dt.Columns.Add(new DataColumn(ruleProcesor.GetColumnName()));
                    }

                    bc.ColumnMappings.Add(tableGroup.FirstOrDefault().GetPrimaryKeyName(), tableGroup.FirstOrDefault().GetPrimaryKeyName());
                    type = GetTypeForMapping(tableGroup.FirstOrDefault().GetPrimaryKeyDataType());
                    if (type != null)
                        dt.Columns.Add(new DataColumn(tableGroup.FirstOrDefault().GetPrimaryKeyName(), type));
                    else
                        dt.Columns.Add(new DataColumn(tableGroup.FirstOrDefault().GetPrimaryKeyName()));

                    foreach (var item in editedValues)
                    {
                        DataRow row = dt.NewRow();
                        row.ItemArray = item;
                        dt.Rows.Add(row);
                    }
                    bc.WriteToServer(dt);
                }
            }
        }

        private Type GetTypeForMapping(DataType type)
        {
            if (type is TimeDataType)
            {
                return typeof(TimeSpan);
            }
            else if (type is DateTimeDataType)
            {
                return typeof(DateTime);
            }
            else
                return null;
        }

        private List<object[]> EditAndAddValues(List<object[]> values, IGrouping<string, IRuleProcesor> tableGroup)
        {
            var data = new List<object[]>();

            if (values == null || values.Count == 0)
                return data;

            int indexOfValueOnRow = 0;
            int keyPositionIndex = values[0].Length - 2;
            int CountOfValuesWithKey = tableGroup.Count() + 1;

            foreach (var row in values)
            {
                indexOfValueOnRow = 0;
                var key = row[keyPositionIndex];
                var dataRow = new object[CountOfValuesWithKey];

                for (int i = 0; i < tableGroup.Count(); i++)
                {
                    var ruleProcesor = tableGroup.ElementAt(i);
                    if (ruleProcesor.WithValue())
                    {
                        dataRow[i] = ruleProcesor.GetNextValue(key.ToString(), row[indexOfValueOnRow]);
                        indexOfValueOnRow++;
                    }
                    else
                    {
                        dataRow[i] = ruleProcesor.GetNextValue(key.ToString());
                    }
                }

                dataRow[tableGroup.Count()] = key;
                data.Add(dataRow);
            }

            return data;
        }

        private string PrepareSQLQuery(IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tableName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Select ");

            foreach (var ruleProcesor in tableGroup.Where(x => x.WithValue()))
            {
                sb.Append(ruleProcesor.GetColumnName());
                sb.Append(",");
            }

            sb.Append(tableGroup.FirstOrDefault().GetPrimaryKeyName());
            sb.Append(",''");
            sb.Append(" from ");
            sb.Append(catalogAndSchema);
            sb.Append(".");
            sb.Append(tableName);

            return sb.ToString();
        }
    }
}
