﻿using DuPE.Domain.Databases;
using DuPE.Domain.Logging;
using DuPE.Domain.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL
{
    public class PostgreSQLDatabaseRepositoryCreater : IDatabaseRepositoryCreater
    {
        private ILogger logger;

        public PostgreSQLDatabaseRepositoryCreater(ILogger logger)
        {
            this.logger = logger;
        }

        public string Name => PostgreSQLConstants.DB_NAME;

        public IGeneralRepository GetGeneralRepository(Dictionary<string, object> arguments)
        {
            if (arguments.TryGetValue(PostgreSQLConstants.CONNECTION_STRING, out object connectionString)
                && arguments.TryGetValue(PostgreSQLConstants.SCHEMA, out object schema1))
                return new PostgreSQLRepository(logger, (string)connectionString, (string)schema1);
            else if (arguments.TryGetValue(PostgreSQLConstants.CATALOG, out object catalog)
                && arguments.TryGetValue(PostgreSQLConstants.PASS, out object pass)
                && arguments.TryGetValue(PostgreSQLConstants.SERVER, out object server)
                && arguments.TryGetValue(PostgreSQLConstants.USER, out object user)
                && arguments.TryGetValue(PostgreSQLConstants.SCHEMA, out object schema2))
                return new PostgreSQLRepository(logger, PostgreSQLConnectionStringCreater.Create((string)server, (string)catalog, (string)user, (string)pass), (string)schema2);
            else
                throw new Exception();
        }

        public IGeneralRepository GetGeneralRepository(string connectionString, string schema)
        {
            return new PostgreSQLRepository(logger, connectionString, schema);

        }
    }
}
