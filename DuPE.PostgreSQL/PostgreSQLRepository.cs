﻿using DuPE.Domain.Logging;
using DuPE.Domain.RuleProcesors;
using DuPE.Domain.Tables;
using DuPE.PostgreSQL.Database;
using DuPE.PostgreSQL.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL
{
    public class PostgreSQLRepository : IGeneralRepository
    {
        private ILogger logger;
        private string connectionString;
        private string schema;

        //private TableRepository tableRepository;
        private BetterTableRepository tableRepository;

        public PostgreSQLRepository(ILogger logger, string connectionString, string schema)
        {
            this.logger = logger;
            this.connectionString = connectionString; ;
            this.schema = schema;

            //tableRepository = new TableRepository(new SessionFactoryBuilder(logger, connectionString), schema);
            tableRepository = new BetterTableRepository(new SessionFactoryBuilder(logger, connectionString), schema);
        }

        public string GetConnectionString()
        {
            return connectionString;
        }

        public string GetSchema()
        {
            return schema;
        }

        public IList<Table> GetAll()
        {
            return Converter.GetTables(tableRepository.GetAll());
        }

        public void Update(IList<IRuleProcesor> procesors)
        {
            tableRepository.Update(procesors);
        }

    }
}
