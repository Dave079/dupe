﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL
{
    public class PostgreSQLConstants
    {
        public const string SERVER = "Server";
        public const string CATALOG = "Catalog";
        public const string USER = "User";
        public const string PASS = "Password";
        public const string CONNECTION_STRING = "ConnectionString";
        public const string SCHEMA = "Schema";

        public const string DB_NAME = "PostgreSQL";
    }
}
