﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL
{
    public class PostgreSQLConnectionStringCreater
    {
        public static string Create(string server, string catalog, string user, string pass)
        {
            //@"Server = 10.0.0.18; Port = 5432; Database = postgres; User Id = postgres; Password = postgres; CommandTimeout = 0;";
            return string.Format("Server={0};Database={1};User Id={2};Password={3}", server, catalog, user, pass);
        }
    }
}
