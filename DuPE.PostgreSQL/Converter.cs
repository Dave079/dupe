﻿using DuPE.Domain.Columns;
using DuPE.Domain.DataTypes;
using DuPE.Domain.Tables;
using DuPE.PostgreSQL.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL
{
    public class Converter
    {
        public static IList<Table> GetTables(IList<PostgreSQLTable> sqlServerTables)
        {
            var tables = new List<Table>();
            foreach (var sqlTable in sqlServerTables)
            {
                var table = CreateTable(sqlTable);
                table.Columns = CreateColumns(sqlTable.Columns);
                table.CheckPrimaryKey();
                tables.Add(table);
            }

            return tables;
        }
        private static Table CreateTable(PostgreSQLTable sqlTable)
        {
            var table = new Table()
            {
                TableCatalog = sqlTable.TableCatalog,
                TableName = sqlTable.TableName,
                TableSchema = sqlTable.TableSchema,
                TableType = sqlTable.TableType,
            };

            return table;
        }

        private static IList<Column> CreateColumns(IList<PostgreSQLColumn> sqlColumns)
        {
            var columns = new List<Column>();
            foreach (var sqlColumn in sqlColumns)
            {
                var column = new Column()
                {
                    ColumnName = sqlColumn.column_name,
                    ColumnDefault = sqlColumn.column_default,
                    IsNullable = sqlColumn.is_nullable.ToLower() == "yes",
                    TableCatalog = sqlColumn.table_catalog,
                    TableName = sqlColumn.table_name,
                    TableSchema = sqlColumn.table_schema,
                    Type = GetType(sqlColumn),
                    IsPrimaryKey = sqlColumn.IsPrimaryKey,
                    IsforeignKey = sqlColumn.IsforeignKey
                };
                columns.Add(column);
            }

            return columns;
        }

        private static DataType GetType(PostgreSQLColumn sqlColumn)
        {
            var type = sqlColumn.data_type.ToString();
            switch (type)
            {
                case "smallint":
                case "integer":
                case "bigint":
                case "numeric":
                case "real":
                case "double precision":
                    return CreateNumberDataType(type);

                //Date and Time
                case "timestamp without time zone":
                case "timestamp with time zone":
                case "timestamp":
                case "date":
                case "time without time zone":
                    //case "time with time zone":
                    return CreateDateTimeDataType(type);

                //Character Strings
                case "character varying":
                case "character":
                case "text":
                    return CreateStringDataType(type, sqlColumn);

                //Other
                case "bigserial":
                case "bit":
                case "bit varying":
                case "boolean":
                case "box":
                case "bytea":
                case "cidr":
                case "circle":
                case "inet":
                case "interval":
                case "json":
                case "jsonb":
                case "line":
                case "lseg":
                case "macaddr":
                case "money":
                case "path":
                case "pg_lsn":
                case "point":
                case "polygon":
                case "smallserial":
                case "serial":
                case "time with time zone":
                case "tsquery":
                case "tsvector":
                case "txid_snapshot":
                case "uuid":
                case "xml":
                    return new UnsupportedDataType(type);

                default:
                    return new UnknownDataType(type);
            }
        }

        private static DataType CreateDateTimeDataType(string type)
        {
            DateTime from;
            DateTime to;
            switch (type)
            {
                case "timestamp without time zone":
                case "timestamp with time zone":
                case "timestamp":
                case "date":

                    from = DateTime.Now.AddMonths(-36);
                    to = DateTime.Now;
                    break;
                case "time without time zone":
                case "time with time zone":
                    return new TimeDataType(type, new TimeSpan(0), new TimeSpan(23, 59, 59));
                default:
                    return new UnknownDataType(type);
            }
            return new DateTimeDataType(type, from, to);

        }

        private static DataType CreateStringDataType(string type, PostgreSQLColumn sqlColumn)
        {
            if (sqlColumn.character_maximum_length == null)
                return new StringDataType(type, null);
            else
                return new StringDataType(type, int.Parse(sqlColumn.character_maximum_length));
        }

        private static DataType CreateNumberDataType(string type)
        {
            double min = 0;
            double max = 0;
            uint numberOfDecimalPLaces = 0;

            switch (type)
            {
                case "smallint":
                    min = -32768;
                    max = 32767;
                    break;
                case "integer":
                    min = -2147483648;
                    max = +2147483647;
                    break;
                case "bigint":
                    min = -9223372036854775808;
                    max = 9223372036854775807;
                    break;
                case "numeric":
                    max = Int32.MaxValue;
                    numberOfDecimalPLaces = 16000;
                    break;
                case "real":
                    max = Int32.MaxValue;
                    numberOfDecimalPLaces = 6;
                    break;
                case "double precision":
                    max = Int64.MaxValue;
                    numberOfDecimalPLaces = 15;
                    break;
                default:
                    throw new Exception();
            }

            return new NumberDataType(type, min, max, numberOfDecimalPLaces);
        }
    }
}
