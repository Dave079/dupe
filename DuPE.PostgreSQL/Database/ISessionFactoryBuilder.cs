﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database
{
    public interface ISessionFactoryBuilder
    {
        ISessionFactory GetFactory();
    }
}
