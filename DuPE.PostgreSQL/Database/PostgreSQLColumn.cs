﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database
{
    public class PostgreSQLColumn
    {
        public string table_catalog { get; set; }
        public string table_schema { get; set; }
        public string table_name { get; set; }
        public string column_name { get; set; }
        public string ordinal_position { get; set; }
        public string column_default { get; set; }
        public string is_nullable { get; set; }
        public string data_type { get; set; }
        public string character_maximum_length { get; set; }
        public string character_octet_length { get; set; }
        public string numeric_precision { get; set; }
        public string numeric_precision_radix { get; set; }
        public string numeric_scale { get; set; }
        public string datetime_precision { get; set; }
        public string interval_type { get; set; }
        public string interval_precision { get; set; }
        public string character_set_catalog { get; set; }
        public string character_set_schema { get; set; }
        public string character_set_name { get; set; }
        public string collation_catalog { get; set; }
        public string collation_schema { get; set; }
        public string collation_name { get; set; }
        public string domain_catalog { get; set; }
        public string domain_schema { get; set; }
        public string domain_name { get; set; }
        public string udt_catalog { get; set; }
        public string udt_schema { get; set; }
        public string udt_name { get; set; }
        public string scope_catalog { get; set; }
        public string scope_schema { get; set; }
        public string scope_name { get; set; }
        public string maximum_cardinality { get; set; }
        public string dtd_identifier { get; set; }
        public string is_self_referencing { get; set; }
        public string is_identity { get; set; }
        public string identity_generation { get; set; }
        public string identity_start { get; set; }
        public string identity_increment { get; set; }
        public string identity_maximum { get; set; }
        public string identity_minimum { get; set; }
        public string identity_cycle { get; set; }
        public string is_generated { get; set; }
        public string generation_expression { get; set; }
        public string is_updatable { get; set; }


        public bool IsPrimaryKey { get; set; }
        public bool IsforeignKey { get; set; }

        public override bool Equals(object obj)
        {
            PostgreSQLColumn eo = (PostgreSQLColumn)obj;
            return eo.table_catalog == this.table_catalog
                && eo.table_schema == this.table_schema
                && eo.table_name == this.table_name
                && eo.column_name == this.column_name;
        }

        public override int GetHashCode()
        {
            return table_catalog.GetHashCode() ^ table_schema.GetHashCode() ^ table_name.GetHashCode() ^ column_name.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}-{4}", table_catalog, table_schema, table_name, column_name, data_type);

        }

        public PostgreSQLColumn()
        {

        }

    }
}
