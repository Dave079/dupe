﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database.Mapping
{
    public class TableMapping : ClassMap<PostgreSQLTable>
    {
        public TableMapping()
        {
            this.Table("information_schema.tables");
            this.CompositeId().KeyProperty(x => x.TableCatalog, "table_catalog")
                              .KeyProperty(x => x.TableSchema, "table_schema")
                              .KeyProperty(x => x.TableName, "table_name");
            this.Map(x => x.TableType).Column("table_type");
            this.Map(x => x.SelfReferencingColumnName).Column("self_referencing_column_name");
            this.Map(x => x.ReferenceGeneration).Column("reference_generation");
            this.Map(x => x.UseDefinedTypeCatalog).Column("user_defined_type_catalog");
            this.Map(x => x.UserDefinedTypeSchema).Column("user_defined_type_schema");
            this.Map(x => x.UserDefinedTypeName).Column("user_defined_type_name");
            this.Map(x => x.IsInsertable_into).Column("is_insertable_into");
            this.Map(x => x.IsTyped).Column("is_typed");

            this.HasMany(x => x.Columns).AsBag().KeyColumns.Add("table_catalog", "table_schema", "table_name");
        }
    }
}
