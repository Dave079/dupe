﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database.Mapping
{
    public class ColumnMapping : ClassMap<PostgreSQLColumn>
    {
        public ColumnMapping()
        {
            this.Table("information_schema.columns");
            this.CompositeId().KeyProperty(x => x.table_catalog, "table_catalog")
                               .KeyProperty(x => x.table_schema, "table_schema")
                               .KeyProperty(x => x.table_name, "table_name")
                               .KeyProperty(x => x.column_name, "COLUMN_NAME");
            this.Map(x => x.table_catalog);
            this.Map(x => x.table_schema);
            this.Map(x => x.table_name);
            this.Map(x => x.column_name);
            this.Map(x => x.ordinal_position);
            this.Map(x => x.column_default);
            this.Map(x => x.is_nullable);
            this.Map(x => x.data_type);
            this.Map(x => x.character_maximum_length);
            this.Map(x => x.character_octet_length);
            this.Map(x => x.numeric_precision);
            this.Map(x => x.numeric_precision_radix);
            this.Map(x => x.numeric_scale);
            this.Map(x => x.datetime_precision);
            this.Map(x => x.interval_type);
            this.Map(x => x.interval_precision);
            this.Map(x => x.character_set_catalog);
            this.Map(x => x.character_set_schema);
            this.Map(x => x.character_set_name);
            this.Map(x => x.collation_catalog);
            this.Map(x => x.collation_schema);
            this.Map(x => x.collation_name);
            this.Map(x => x.domain_catalog);
            this.Map(x => x.domain_schema);
            this.Map(x => x.domain_name);
            this.Map(x => x.udt_catalog);
            this.Map(x => x.udt_schema);
            this.Map(x => x.udt_name);
            this.Map(x => x.scope_catalog);
            this.Map(x => x.scope_schema);
            this.Map(x => x.scope_name);
            this.Map(x => x.maximum_cardinality);
            this.Map(x => x.dtd_identifier);
            this.Map(x => x.is_self_referencing);
            this.Map(x => x.is_identity);
            this.Map(x => x.identity_generation);
            this.Map(x => x.identity_start);
            this.Map(x => x.identity_increment);
            this.Map(x => x.identity_maximum);
            this.Map(x => x.identity_minimum);
            this.Map(x => x.identity_cycle);
            this.Map(x => x.is_generated);
            this.Map(x => x.generation_expression);
            this.Map(x => x.is_updatable);
        }
    }
}
