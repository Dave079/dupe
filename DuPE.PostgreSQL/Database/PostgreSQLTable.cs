﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database
{
    public class PostgreSQLTable
    {
        public string TableCatalog { get; set; }
        public string TableSchema { get; set; }
        public string TableName { get; set; }
        public string TableType { get; set; }
        public string SelfReferencingColumnName { get; set; }
        public string ReferenceGeneration { get; set; }
        public string UseDefinedTypeCatalog { get; set; }
        public string UserDefinedTypeSchema { get; set; }
        public string UserDefinedTypeName { get; set; }
        public string IsInsertable_into { get; set; }
        public string IsTyped { get; set; }

        public IList<PostgreSQLColumn> Columns { get; set; }

        public PostgreSQLTable()
        {

        }

        public override bool Equals(object obj)
        {
            PostgreSQLTable eo = (PostgreSQLTable)obj;
            return eo.TableCatalog == this.TableCatalog
                && eo.TableSchema == this.TableSchema
                && eo.TableName == this.TableName;
        }

        public override int GetHashCode()
        {
            return TableCatalog.GetHashCode() ^ TableSchema.GetHashCode() ^ TableName.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}", TableCatalog, TableSchema, TableName, TableType);
        }
    }
}
