﻿using DuPE.Domain.Logging;
using DuPE.PostgreSQL.Database.Mapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database
{
    public class SessionFactoryBuilder : ISessionFactoryBuilder
    {
        private readonly string connectionString;
        private static Configuration _config;
        private ILogger logger;

        public SessionFactoryBuilder(ILogger logger, string connectionStrin)
        {
            this.connectionString = connectionStrin;
            this.logger = logger;
        }
        protected ISessionFactory _factoryInstance;

        public ISessionFactory GetFactory()
        {
            if (_factoryInstance == null)
            {
                var config = createConfiguration();
                _factoryInstance = config.BuildSessionFactory();
            }
            return _factoryInstance;
        }

        public ISession session { get; set; }

        protected Configuration createConfiguration()
        {
            var config = Fluently.Configure();

            if (logger.IsDebugEnabled)
                config.Database(PostgreSQLConfiguration.PostgreSQL82.Raw("hbm2ddl.keywords", "none").ConnectionString(connectionString).ShowSql);
            else
                config.Database(PostgreSQLConfiguration.PostgreSQL82.Raw("hbm2ddl.keywords", "none").ConnectionString(connectionString));

            config.Mappings(m => m.FluentMappings

            .Add<ColumnMapping>().Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())
            .Add<TableMapping>().Conventions.Add(FluentNHibernate.Conventions.Helpers.DefaultLazy.Never())
                );

            config.ExposeConfiguration(BuildSchema);
            _config = config.BuildConfiguration();

            return _config;
        }

        private void BuildSchema(Configuration config)
        {
            new SchemaUpdate(config).Execute(false, false);
        }
     
    }
}
