﻿using DuPE.Domain.DataTypes;
using DuPE.Domain.RuleProcesors;
using NHibernate;
using NHibernate.Linq;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuPE.PostgreSQL.Database.Repository
{
    public class BetterTableRepository : DefaultRepository<PostgreSQLTable>
    {
        private readonly string defaultSchema;

        public BetterTableRepository(ISessionFactoryBuilder sessionFb, string defaultSchema) : base(sessionFb)
        {
            this.defaultSchema = defaultSchema;
        }

        public override IList<PostgreSQLTable> GetAll()
        {
            IList<PostgreSQLTable> list;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    list = session.Query<PostgreSQLTable>().Fetch(x => x.Columns).Where(x => x.TableSchema == defaultSchema).ToList();
                    SetKeys(list, session);
                    tx.Commit();
                }
            }
            return list;
        }

        private void SetKeys(IList<PostgreSQLTable> tables, ISession session)
        {
            var data = session.CreateSQLQuery(
                string.Format("SELECT c.table_name,c.column_name, t.constraint_type " +
                "FROM information_schema.key_column_usage AS c, information_schema.table_constraints AS t " +
                "WHERE t.constraint_name = c.constraint_name " +
                "and t.constraint_schema = c.constraint_schema " +
                "and t.table_schema = '{0}'", defaultSchema)).List<object[]>().ToDictionary(x => x[0].ToString() + " " + x[1].ToString());

            foreach (var table in tables)
            {
                foreach (var column in table.Columns)
                {
                    var key = column.table_name + " " + column.column_name;
                    if (data.ContainsKey(key))
                    {
                        if (((string)data[key][2]).ToLower().Contains("primary"))
                            column.IsPrimaryKey = true;
                        else
                            column.IsforeignKey = true;
                    }
                }
            }
        }

        public void Update(IList<IRuleProcesor> procesors)
        {
            var groupsByTables = procesors.GroupBy(x => (x.GetCatalogName() + "." + x.GetSchemaName() + "." + x.GetTableName()));

            foreach (var tableGroup in groupsByTables)
            {
                UpdateTable(tableGroup);
            }
        }

        private void UpdateTable(IGrouping<string, IRuleProcesor> tableGroup)
        {
            List<object[]> values = null;
            List<object[]> editedValues = null;

            string catalog = tableGroup.FirstOrDefault().GetCatalogName();
            string schema = tableGroup.FirstOrDefault().GetSchemaName();
            string catalogAndSchema = string.Format("{0}.{1}", catalog, schema);
            string tempTableName;
            string sourceTableName = tableGroup.FirstOrDefault().GetTableName();

            string query = PrepareSQLQuery(tableGroup, catalogAndSchema, sourceTableName);

            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    values = session.CreateSQLQuery(query).List().Cast<object[]>().ToList();

                    editedValues = EditAndAddValues(values, tableGroup);
                    values = null;
                    tempTableName = CreateTempTabeName(session, tableGroup, catalog, schema, sourceTableName);
                    BulkInsert(session, editedValues, tableGroup, catalogAndSchema, tempTableName);
                    MergeData(session, tableGroup, catalogAndSchema, tempTableName, sourceTableName);
                    DropTable(session, string.Format("{0}.{1}", catalogAndSchema, tempTableName));

                    tx.Commit();
                }
            }
        }

        private void DropTable(ISession session, string table)
        {
            session.CreateSQLQuery(string.Format("drop table {0};", table)).ExecuteUpdate();
        }
        private void MergeData(ISession session, IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tempTableName, string sourceTableName)
        {
            string query = string.Format("UPDATE {0}.{1} " +
                           "SET {4} " +
                           "from  {0}.{2} " +
                           "where {0}.{2}.{3} = {0}.{1}.{3}",
                           catalogAndSchema,
                           sourceTableName,
                           tempTableName,
                           tableGroup.FirstOrDefault().GetPrimaryKeyName(),
                           string.Join(",", tableGroup.Select(x => string.Format(" {3} =  {0}.{2}.{3}", catalogAndSchema,sourceTableName,tempTableName, x.GetColumnName())))
                           );

            //string query = string.Format("INSERT INTO {0}.{1}({3},{5}) " +
            //              "select {3},{5} from {0}.{2} " +
            //              "ON CONFLICT({3}) " +
            //              "DO UPDATE SET {4};",
            //              catalogAndSchema,
            //              sourceTableName,
            //              tempTableName,
            //              tableGroup.FirstOrDefault().GetPrimaryKeyName(),
            //              string.Join(",", tableGroup.Select(x => string.Format("{0} = EXCLUDED.{0}", x.GetColumnName()))),
            //              string.Join(",", tableGroup.Select(x => x.GetColumnName()))
            //              );

            session.CreateSQLQuery(query).SetTimeout(0).ExecuteUpdate();
        }

        private string CreateTempTabeName(ISession session, IGrouping<string, IRuleProcesor> tableGroup, string catalog, string schema, string tableName)
        {
            string queryForTableName =
                "SELECT * FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE " +
                "TABLE_CATALOG = '" + catalog +
                "' and TABLE_SCHEMA = '" + schema +
                "' and TABLE_NAME = '{0}'";

            int version = 0;
            string tempTableName;

            while (true)
            {
                tempTableName = string.Format("{0}_temp{1}", tableName, version++);

                if (session.CreateSQLQuery(string.Format(queryForTableName, tempTableName)).List().Cast<object>().ToList().Count == 0)
                {
                    session.CreateSQLQuery(string.Format("SELECT {5},{4} INTO {0}.{1}.{3} FROM {0}.{1}.{2} WHERE 1 = 2",
                        catalog,
                        schema,
                        tableName,
                        tempTableName,
                        string.Join(",", tableGroup.Select(x => x.GetColumnName())),
                        tableGroup.FirstOrDefault().GetPrimaryKeyName()
                        )).ExecuteUpdate();
                    break;
                }
            }

            return tempTableName;
        }

        private void BulkInsert(ISession session, List<object[]> editedValues, IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tempTableName)
        {
            string command = string.Format("COPY {0}.{1} ({2},{3}) FROM STDIN (FORMAT BINARY)",
                catalogAndSchema, tempTableName,
                string.Join(",", tableGroup.Select(x => x.GetColumnName())),
                tableGroup.FirstOrDefault().GetPrimaryKeyName()
                );

            var types = new List<NpgsqlTypes.NpgsqlDbType>();
            foreach (var rule in tableGroup)
                types.Add(GetTypeForPostgre(rule.GetColumnDataType()));
            types.Add(GetTypeForPostgre(tableGroup.FirstOrDefault().GetPrimaryKeyDataType()));

            var connection = (NpgsqlConnection)session.Connection;
            using (var writer = connection.BeginBinaryImport(command))
                foreach (var row in editedValues)
                {
                    int index = 0;
                    writer.StartRow();
                    foreach (var att in row)
                    {
                        if (att == null)
                            writer.WriteNull();
                        else
                            writer.Write(att, types[index]);
                        index++;
                    }
                }
        }

        private NpgsqlDbType GetTypeForPostgre(DataType dataType)
        {
            switch (dataType.OriginalName)
            {
                //Exact Numerics
                case "smallint":
                    return NpgsqlDbType.Smallint;
                case "integer":
                    return NpgsqlDbType.Integer;
                case "bigint":
                    return NpgsqlDbType.Bigint;
                case "numeric":
                    return NpgsqlDbType.Numeric;
                case "real":
                    return NpgsqlDbType.Real;
                case "double precision":
                    return NpgsqlDbType.Double;

                //Date and Time
                case "timestamp without time zone":
                    return NpgsqlDbType.Timestamp;
                case "timestamp with time zone":
                    return NpgsqlDbType.TimestampTZ;
                case "timestamp":
                    return NpgsqlDbType.Timestamp;
                case "date":
                    return NpgsqlDbType.Date;
                case "time without time zone":
                    return NpgsqlDbType.Time;
                case "time with time zone":
                    return NpgsqlDbType.TimeTZ;

                //Character Strings
                case "character varying":
                    return NpgsqlDbType.Varchar;
                case "character":
                    return NpgsqlDbType.Char;
                case "text":
                    return NpgsqlDbType.Text;
                default:
                    throw new Exception();
            }
        }

        private List<object[]> EditAndAddValues(List<object[]> values, IGrouping<string, IRuleProcesor> tableGroup)
        {
            var data = new List<object[]>();

            if (values == null || values.Count == 0)
                return data;

            int indexOfValueOnRow = 0;
            int keyPositionIndex = values[0].Length - 2;
            int CountOfValuesWithKey = tableGroup.Count() + 1;

            foreach (var row in values)
            {
                indexOfValueOnRow = 0;
                var key = row[keyPositionIndex];
                var dataRow = new object[CountOfValuesWithKey];

                for (int i = 0; i < tableGroup.Count(); i++)
                {
                    var ruleProcesor = tableGroup.ElementAt(i);
                    if (ruleProcesor.WithValue())
                    {
                        dataRow[i] = ruleProcesor.GetNextValue(key.ToString(), row[indexOfValueOnRow]);
                        indexOfValueOnRow++;
                    }
                    else
                    {
                        dataRow[i] = ruleProcesor.GetNextValue(key.ToString());
                    }
                }

                dataRow[tableGroup.Count()] = key;
                data.Add(dataRow);
            }

            return data;
        }

        private string PrepareSQLQuery(IGrouping<string, IRuleProcesor> tableGroup, string catalogAndSchema, string tableName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Select ");

            foreach (var ruleProcesor in tableGroup.Where(x => x.WithValue()))
            {
                sb.Append(ruleProcesor.GetColumnName());
                sb.Append(",");
            }

            sb.Append(tableGroup.FirstOrDefault().GetPrimaryKeyName());
            sb.Append(",''");
            sb.Append(" from ");
            sb.Append(catalogAndSchema);
            sb.Append(".");
            sb.Append(tableName);

            return sb.ToString();
        }
    }
}
