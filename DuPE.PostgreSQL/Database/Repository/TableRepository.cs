﻿using DuPE.Domain.RuleProcesors;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DuPE.PostgreSQL.Database.Repository
{
    public class TableRepository : DefaultRepository<PostgreSQLTable>
    {
        private readonly string defaultSchema;

        public TableRepository(ISessionFactoryBuilder sessionFb, string defaultSchema) : base(sessionFb)
        {
            this.defaultSchema = defaultSchema;
        }

        public override IList<PostgreSQLTable> GetAll()
        {

            IList<PostgreSQLTable> list;
            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    list = session.Query<PostgreSQLTable>().Fetch(x => x.Columns).Where(x => x.TableSchema == defaultSchema).ToList();
                    SetKeys(list, session);
                    tx.Commit();
                }
            }
            return list;
        }

        private void SetKeys(IList<PostgreSQLTable> tables, ISession session)
        {
            var data = session.CreateSQLQuery(string.Format("SELECT c.table_name,c.column_name, t.constraint_type FROM information_schema.key_column_usage AS c LEFT JOIN information_schema.table_constraints AS t ON t.constraint_name = c.constraint_name WHERE t.table_schema = '{0}'", defaultSchema)).List<object[]>().ToDictionary(x => x[0].ToString() + " " + x[1].ToString());

            foreach (var table in tables)
            {
                foreach (var column in table.Columns)
                {
                    var key = column.table_name + " " + column.column_name;
                    if (data.ContainsKey(key))
                    {
                        if (((string)data[key][2]).ToLower().Contains("primary"))
                            column.IsPrimaryKey = true;
                        else
                            column.IsforeignKey = true;
                    }
                }
            }
        }

        public void Update(IList<IRuleProcesor> procesors)
        {
            List<IRuleProcesor> procesorsWithValue = procesors.Where(x => x.WithValue()).ToList();
            List<IRuleProcesor> procesorWitoutValue = procesors.Where(x => !x.WithValue()).ToList();

            ProcessWithValue(procesorsWithValue);
            ProcessWithoutValue(procesorWitoutValue);
        }

        private void ProcessWithValue(IList<IRuleProcesor> procesors)
        {
            foreach (var procesor in procesors)
            {
                using (var session = _sessionFB.GetFactory().OpenSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        var pKeys = session.CreateSQLQuery(string.Format("Select {0},{4} from {1}.{2}.{3}", procesor.GetPrimaryKeyName(), procesor.GetCatalogName(), procesor.GetSchemaName(), procesor.GetTableName(), procesor.GetColumnName())).List().Cast<object[]>().ToList();

                        for (int i = 0; i < pKeys.Count; i++)
                        {
                            session.CreateSQLQuery(
                                string.Format(
                                    "UPDATE {0}.{1}.{2} SET {3}=:value WHERE {4}=:key",
                                        procesor.GetCatalogName(),
                                        procesor.GetSchemaName(),
                                        procesor.GetTableName(),
                                        procesor.GetColumnName(),
                                        procesor.GetPrimaryKeyName()
                                        )
                                )
                                .SetParameter("value", procesor.GetNextValue(pKeys[i][0].ToString(), pKeys[i][1]))
                                .SetParameter("key", pKeys[i][0])
                                .ExecuteUpdate();
                        }
                        tx.Commit();
                    }
                }
            }
        }

        private void ProcessWithoutValue(IList<IRuleProcesor> procesors)
        {
            List<Thread> threads = new List<Thread>();
            var groups = procesors.GroupBy(x => (x.GetCatalogName() + "." + x.GetSchemaName() + "." + x.GetTableName()));
            StringBuilder sb = new StringBuilder();

            foreach (var group in groups)
            {
                using (var session = _sessionFB.GetFactory().OpenSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        var pKeys = session.CreateSQLQuery(string.Format("Select {0} from {1}", group.First().GetPrimaryKeyName(), group.Key)).List();

                        int countforthread = pKeys.Count / 4;
                        int sum = 0;
                        int sumP = 0;

                        sb.Clear();
                        sb.AppendFormat("UPDATE {0} SET ", group.Key);

                        foreach (var att in group)
                            sb.AppendFormat("{0}=:{0},", att.GetColumnName());

                        sb = sb.Remove(sb.Length - 1, 1);
                        sb.AppendFormat(" where {0}=:{0}", group.FirstOrDefault().GetPrimaryKeyName());

                        IList<ParamaForThread> ParamasForThread = new List<ParamaForThread>();
                        if (pKeys.Count > 5000)
                        {
                            ParamasForThread = new List<ParamaForThread>() { new ParamaForThread() { query = sb.ToString(), group = group, pKeys = new List<object>() } };
                            foreach (var item in pKeys)
                            {
                                sum++;
                                ParamasForThread[sumP].pKeys.Add(item);

                                if (sum % countforthread == 0 && sumP != 3)
                                {
                                    sumP++;
                                    ParamasForThread.Add(new ParamaForThread() { query = sb.ToString(), group = group, pKeys = new List<object>() });
                                }
                            }

                        }

                        if (ParamasForThread.Count == 0)
                        {
                            ParamasForThread.Add(new ParamaForThread() { query = sb.ToString(), group = group, pKeys = pKeys });
                        }

                        foreach (var param in ParamasForThread)
                        {
                            ThreadStart processTaskThread = delegate { UpdateWithoutValue(param); };
                            var thread = new Thread(processTaskThread);

                            threads.Add(thread);
                            thread.Start();
                        }
                    }
                }

                foreach (var th in threads)
                {
                    th.Join();
                }
            }
        }

        private void UpdateWithoutValue(ParamaForThread param)
        {
            var group = param.group;
            var listrules = new List<IRuleProcesor>();

            foreach (var item in group)
            {
                listrules.Add(item.GetCopy());
            }

            using (var session = _sessionFB.GetFactory().OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    foreach (var item in param.pKeys)
                    {
                        var query = session.CreateSQLQuery(param.query);
                        foreach (var att in group)
                            query.SetParameter(att.GetColumnName(), att.GetNextValue(item.ToString()));

                        query.SetParameter(group.FirstOrDefault().GetPrimaryKeyName(), item);
                        query.ExecuteUpdate();
                    }
                    tx.Commit();
                }
            }
        }



        public class ParamaForThread
        {
            public IList pKeys { get; set; }
            public IGrouping<string, IRuleProcesor> group { get; set; }
            public string query { get; set; }
            public ParamaForThread()
            {
            }
        }

    }
}
